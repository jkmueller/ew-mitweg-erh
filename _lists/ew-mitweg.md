---
title: "Mitweg mit Eugen Rosenstock-Huessy"
menu: Mitweg
row: 5
used-categories: mitweg
---
Eckart Wilkens hat seine Beiträge zu Eugen Rosenstock-Huessy aus den Jahren 1973 bis 1990, seinen Mitweg, zwischen Dezember 2002 und April 2003 in fünf Bänden zusammengefaßt.

[Band 1: Dienst auf dem Planeten]( {{ '/ew-mitweg-1' | relative_url }} )

[Band 2: Joseph Wittig, Helmuth James von Moltke, Eugen Rosenstock-Huessy mit Heinrich Heine am Kölner Dom]( {{ '/ew-mitweg-2' | relative_url }} )

[Band 3: Arbeitsgemeinschaft, Andragogik]( {{ '/ew-mitweg-3' | relative_url }} )

[Band 4: Zurück ins Wagnis der Sprache]( {{ '/ew-mitweg-4' | relative_url }} )

[Band 5: Im Lehrhaus der Zukunft: „Erlebthaben des innereren Gesichts”]( {{ '/ew-mitweg-5' | relative_url }} )
