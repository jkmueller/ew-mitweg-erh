---
title: "Eugen Rosenstock-Huessy: Peace Corps (1966)"
category: lectures
created: 1966
published: 2023-09-17
---
### NOTE

This is the transcription of three lectures during a visit in a camp of the Peace Corps, transcribed from the tapes by Frances Huessy, with the following changes and additions:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.

The three lectures are extraordinary because they constitute a form of peace after a harsh, breathtaking quarrel preceding the second lecture. So that the whole is what it speaks about.

The quote of the greeting of peace Phil. 4, 7 transcends even all what is said and understood – or misunderstood.

*Cologne, March 19, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH Peace Corps (1966)]( {{ 'assets/downloads/wilkens_erh_Peace_Corps.pdf' | relative_url }})

- [The transcript of „ERH Peace Corps (1966)” as PDF](https://www.erhfund.org/wp-content/uploads/655.pdf)
- [The audio recordings of „ERH Peace Corps (1966)”](https://www.erhfund.org/lecturealbum/volume-30-peace-corps-1966/)
