---
title: "Eugen Rosenstock-Huessy: Historiography (1959)"
category: lectures
created: 1959
published: 2023-09-23
---
### NOTE OF THE EDITOR

In the year 1959 Eugen Rosenstock-Huessy gave 13 lectures in Los Angeles: Historiography. They are minutely transcribed by Frances Huessy.

I

Here I offer an edited sample. I did the following operations.

A

FINDING THE TEXT WHICH IS FINALLY MEANT FOR THE LISTENER TO BE KEPT IN MIND

1) Elimination of oral empty phrases as: you see, obviously, so to speak, of course.  \
2) Elimination of false beginnings of sentences which are corrected by the speaker. \
3) Elimination of words, that come first in German, then in English.

B

DIVIDING THE TEXT

4) forming paragraphs: after a slight change within the field in which speaker and listener go together, \
5) forming subdivisions: after a mental change of aspect of the cross of reality: from 1 forward, 2 inward, 3 backward to 4 outward, \
6) forming higher divisions in the same manner: I forward, II inward, III backward, IV outward, \
7) forming chapters, after four times four aspects within a chapter are paced off, one lecture usually containing three parts, the first and second complete (four chapters), the third one shorter or missing. So you have about forty times change of aspect.

The reader may keep in mind:
- I and 1 = appeal to the listener as a person thrown in to a new future
- II and 2 = appeal to the listener as a feeling and participating person
- III and 3 = appeal to the listener to hear something about the history of which he is invited to be a part
- IV and 4 = appeal to the listener to sum up a theme and take home a message.

So the edited text tries to give you a help to change the aspects in time (not to be taken by surprise too much which tends to make you desperate) as one of the students said:

*Because when I realize how hard it is to explain these things when we just start with ourselves up here, all alone without any notes, because as we're reading our notes and seeing all the things that you said, and it seems like we understand it, until when I meet someone who doesn't even know you, and I want to say that I met an instructor that I think has made some difference in my life, and he asks the question, "Why?" and I try to explain –*

Kill him.

*I just say, "You have to meet him."*

Well, that's very good of you.

C

HEADINGS

8) to chapters taken from the text – this is of course arbitrary, result of selecting what is important considering the whole lecture, \
9) to the parts taken from the text, directing your attention what will come out as the main point. \
10) to the lectures taken from the text, giving the outline of the whole process,

D

INDICES

11) Contents – this makes it possible to have an overview of the whole series, with the headings for lectures, parts and chapters, \
12) Names (the Roman ciphers indicating the lectures, then parts, then chapter). Many names of course, several should be more precisely given – the ease in their appearing during the lectures is astonishing, no pressure, apt to flow into the speech as Americans, I surmise, expect. \
13) Put it down – is often the imperative, or: take it down. Here are the sentences which Rosenstock-Huessy himself (or me!) wants to be kept in memory. It is quite a collection of aphorisms, emphatic enough. \
14) The story of... – I have listed in alphabetical order the beginning of short and long stories showing how great the part of oral history here is. Especially interesting of course all the stories beginning with “I” ‘(listed as: The story of Rosenstock-Huessy ...). There are personal experiences and historical details which appeal to the listener as traject – to pass them on.

II

The whole series reveals tragically how speech is constituted out of misunderstanding – creating the future. And it is really a pity that Rosenstock-Huessy couldn ́t follow his first vision of continuing with his new findings about reading the bible: he had to change to Thucydides, Plutarch, Polybius – and even there he met the shortag of learning and education in his listeners. And notwithstanding: he continued to call to listening and courage to enter history, in his sense: to accept the unique moments of unique life.

III

The shaking, shocking decision: this is my last full lecture to American students: \
*I shall never teach again American students, I can assure you. This is my last term in my life.*

after one of the students rejected listening to John Brown ́s Body as imperative to his own heart and asked:

(Is this his realism, or naturalism, or --.)

*Oh be ashamed, in this context to ask for stupidities.*

(Or is that romanticism ...?)

*I could shoot you! Breaking up our understanding at this moment with these abstractions.*

**came true – almost:** you find in the chronological bibliography after 1959
- Liberal Arts College 1960 (26 pp.)
- What Future for the Professions? 1960 (93 pp.)
- Grammatical Method 1962 (51 pp.)
- In Defence of the Grammatical Method 1962 (87 pp.)
- The Bionomics of Language 1962 (149 pp.)
- The Cross of Reality 1965 (10 pp.)
- Economy of Times 1965 (79 pp.)
- Talk with Franciscans 1965 (36 pp.)
- Lingo of Linguistics 1966 (63 pp.)
- Cruciform Character of History 1967 (65 pp.)
- **Universal History 1967** (396 pp.)
- Fashions of Atheism 1968 (20 pp.)
- The University 1968 (8 pp.)

**that is** – the twenty lectures Universal History of 1967 made good what he had promised eight years earlier.

IV

It is really astonishing that Eugen Rosenstock-Huessy didn ́t squint at the listeners and readers through tape recording: he spoke really to the present persons.

And this is why these lectures speak again as if we were these persons.

*Cologne, August 8, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH Historiography (1959)]( {{ 'assets/downloads/wilkens_erh_Historiography_1959.pdf' | relative_url }})

- [The transcript of „ERH Historiography (1959)” as PDF](https://www.erhfund.org/wp-content/uploads/645.pdf)
- [The audio recordings of „ERH Historiography (1959)”](https://www.erhfund.org/lecturealbum/volume-20-historiography-1959/)
