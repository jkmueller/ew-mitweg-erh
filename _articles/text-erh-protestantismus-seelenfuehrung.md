---
title: "Eugen Rosenstock-Huessy: Protestantismus und Seelenführung (1929)"
category: bearbeitung
created: 1929
published: 2023-09-11
---
in: in: Kairos-Jahrbuch 2, Protestantismus als Kritik und Gestaltung, Darmstadt 1929, hrsg. von Paul Tillich

### NOTIZ I

*Im Deutschen Pfarrerblatt 1964 Nr. 17 erschienen Abschnitte (Kapitel 2 und Teile aus Kapitel 5) aus dem Beitrag in dem von Paul Tillich herausgegebenen Buch: Protestantismus als Kritik und Gestaltung. Zweites Buch des Kairos-Kreises, Otto Reichl Verlag, 1929 (41 Seiten) mit folgender Vorbemerkung:*

*Auf die unverminderte Aktualität des unter o. a. Titel vor Jahrzehnten veröffentlichten umfänglichen Beitrags machte ein Leser aufmerksam. Wenn wir zwei ausgewählte Abschnitte gerade jetzt veröffentlichen, so deshalb, weil die Gedanken, denen man hier begegnet, in einem unverkennbaren Zusammenhang mit dem Thema des Deutschen Pfarrertags stehen. RSch.*

*(Das Thema des Deutschen Pfarrertages am 9. September 1964 in Münster lautete: „Sterbende und lebende Kirche jenseits von Oder und Neiße“ – „Die Dogmatik zwischen Exegese und Verkündigung“ – „Der römische Katholizismus der Gegenwart als Spannungsfeld und die evangelische Aufgabe“.)*

Ich kannte Rosenstock-Huessys Schrift in der verkürzten Form, wie sie im Pfarrerblatt steht. Die Kenntnis des ganzen Stücks nun bringt mich zu folgenden Bemerkungen.

Die Pfarrerblatt-Vorbemerkung – gezeichnet: RSch. – offenbart in erschütternder Weise, wie die Stimme Eugen Rosenstock-Huessys, gerade indem sie scheinbar wieder gehört wird, im Verstummen bleibt.

1) *„vor Jahrzehnten“* – diese Angabe verlegt Rosenstock-Huessy in das längst Vergangene, verschweigt Hitler-, Kriegs- und Nachkriegszeit, ganz im Adenauer- Ton, statt etwa zu sagen: vier Jahre vor Hitler;

2) *„umfänglich“* – bleibt ohne Angabe, wie die cruciverte Gliederung des Ganzen den Sinn überhaupt erst erschließt und die Luft atmen läßt, die jenseits von Hitler zu hören war, und dann: keine Angabe, wie man denn nun an die ganze Schrift herankommen könnte;

3) *„ein Leser“* - der Leser des Pfarrerblatts, doch wohl ein Pfarrer, wird nicht namentlich genannt, die Gedächtnisbrücke bleibt namenlos - viel zu riskant, weil die Hitler-Luft immer noch die Herzen erstickt, den Namen preiszugeben, wer weiß, was der dann auszuhalten hätte;

4) *„Thema des Pfarrertages“* - die Entschuldigung, daß die gewählten Abschnitte zum Thema des Pfarrertages 1964 passen, ist eine Ohrfeige, indem die Autorität derer, die das Thema des Pfarrertages festgesetzt haben, ganz ungebrochen ist: sie sagen, was wichtig ist, Rosenstock-Huessys Beitrag ist willkommene Vor- oder Nachspeise.

5) *„unverkennbarer Zusammenhang“* - Sterbende und lebende Kirche jenseits von Oder und Neiße – ich nehme an: östlich von Oder und Neiße – als wäre die Kirche diesseits der Oder und Neiße so lebendig wie nie! Die Dogmatik zwischen Exegese und Verkündigung

– darf der Pfarrer nun seine Philosophie treiben oder nicht? – um es mit Worten der Schrift von Rosenstock-Huessy zu sagen. *Der römische Katholizismus der Gegenwart als Spannungsfeld und die evangelische Aufgabe* – um dazu beizutragen, wäre der Abdruck der Kapitel 2 *Die Seele des protestantischen Pfarrers* und 3 *Die Sorge für das katholische Beichtkind* erschließend gewesen. Von der Aufgabe der Erwachsenenbildung kein Wort, das Pfarrerblatt tut so, als hätten sie, die evangelischen Pfarrer, die neu ausgesprochene Aufgabe der Zeitgenossenschaft Jesu allein zu bewältigen.

6) *„Eine Seelenführung gibt es im Protestantismus nicht“* – dieser erste Satz im Pfarrerblatt bekommt eine Schärfe und Unklarheit zugleich, indem die ausdrückliche Vorbemerkung zu dem Wort „Seelenführung“ (das mir jedenfalls nicht vorgekommen ist, verschüttet im Holocaust, eine Übersetzung vielleicht von *Psychagogik*, die heute für Kinder- und Jugendpsychotherapie steht, hervorgegangen aus der Psychoanalyse durch Sigmund Freuds Tochter Anna Freud) nicht einmal gestreift wird.

Stellt man zur Orientierung das Kreuz der Wirklichkeit (das ja ausdrücklich genannt wird) dem Verstehen zur Seite:

*präjektiv: Kapitel 1 Die Religion des Staatsmanns* – die Zukunftskraft \
*subjektiv: Kapitel 2 Die Seele des protestantischen Pfarrers* – die lyrische Stimme innen \
*trajektiv: Kapitel 3 Die Sorge für das katholische Beichtkind* – das Weiten der Zeiten im Erzählen \
*objektiv: Kapitel 4 Der bleibende Wert der bisherigen Formen* – das Bleiben bei den Tatsachen

als Voraussetzung für das, was zaghaft, verschämt, nach dem richtigen Zeitpunkt suchend, Gehör finden will:

*präjektiv und subjektiv: Kapitel 5 Die Zeitgenossenschaft Jesu* \
*trajektiv und objektiv: Kapitel 6 Beseelung und Seelenführung*

mit dem – fast schon verzweifelten Ausruf am Schluß: *Werden sie es tun?* – nämlich die Kirchenleute? – dann ist zu erkennen, was alles weggelassen wurde: der Schritt aus der Theologie in die Soziologie.

Und das Pfarrerblatt hätte einsetzen müssen mit dem Bekenntnis: wir haben es nicht geschafft, die Bekennende Kirche war in ihrer über Hitler- und Kriegszeit hinausreichenden Wirkung nicht stark genug, in den 19 (neunzehn!) Jahren nach Kriegsende die Verödung der Kirchengemeinden aufzuhalten.

Diese Veröffentlichung – übrigens ja auch ohne Hinweis auf die aktuelle Neuerscheinung Rosenstock-Huessys: *„Die Sprache des Menschengeschlechts“* 1963/64 – ist ein Beispiel für das Zum-Verstummen-bringen, indem man so tut, als brächte man die Stimme – als „Wiedergutmachung“ – wieder hervor.

### NOTIZ II

Abschrift des Textes aus dem Kairos-Jahrbuch von 1929, mit einer Gliederung, die den Gang der Gedanken, also vor allem den Aspektwechsel, wie er zu der lebendigen Rede im Kreuz der Wirklichkeit gehört, merklich macht. Die Kapiteleinteilung ist ebenfalls hinzugefügt, ein Kapitel ist vollendet, wenn das Besprochene in viermal vier Richtungen durchgeführt ist. Zu erwarten sind also, bei 16 Kapiteln, 16 mal 16 Worte = 256 Sprach.-Wendungen (da die Kapitel am Ende eines Abschnittes nicht vollständig sein müssen, sind es etwas weniger).

Dies ist also das Maß der Aufmerksamkeit, das diese Schrift erheischt.

Die Kapitelüberschriften erleichtern die Orientierung, sind aus dem Text geschöpft.

***Das Maß der Aufmerksamkeit*** ist der präjektive Aspekt in dem zwischen Sprecher und Leser stattfindenden Vorgang: es setzt das Maß bis zum Aufhören, also bis zu der Umkehr des Wortes in dem Leser, wenn er das Gehörte zu der einen imperativischen Gestalt bei sich fassen soll.

***Das Maß der Innigkeit*** ist abzulesen an dem Gebrauch der Namen, die als bekannt vorausgesetzt werden. Da ist nun auffällig, daß fast alle Namen die Nachnamen sind, ohne Vornahmen und natürlich auch ohne Angabe der Lebensdaten: die Genannten bilden eben die Gefährtenschaft, über die Zeitläufe erhaben. Für den Leser nach 1945 ist es wahrscheinlich nötig, diese Gefährtenschaft durch weitere Mitteilungen lebendig zu machen.

***Das Maß der Vertrautheit*** ist daran abzulesen, wie der Sprecher sich in Erzählungen offenbart – diese wecken die Verbundenheit: ach, Du bist auch ein Mensch wie ich, wir haben die Geschichte betreten, Du von dieser Seite her, ich vielleicht von einer anderen. Diese Geschichten sind hier durch Färbung hervorgehoben.

***Das Maß der Dringlichkeit*** des Gesagten ist an der Schärfe zu erfahren, mit der einige Sätze sich merklich machen und über den Zusammenhang im Gedächtnis weiter schweben mögen. Deshalb habe ich einige solcher Sätze durch Fett-kursiv- Druck hervorgehoben und in einem eigenen Register alphabetisch (also selbständige außerhalb des Zusammenhangs) zusammengestellt.

Die Vollständigkeit dieser vier Maße verbürgt auch, daß dieser eine Text für eine ganze Existenz einstehen kann – „und wenn Du von mir nur dieses eine erfahren hast, so steht es schon für mein ganzes Leben ein“.

Mit Erschütterung habe ich beim Abschreiben gefühlt, wie ausgerechnet das Wort „Seelenführung“ durch Hitlers Führer-Kult zunichte gemacht wurde – so daß es schwer ist, durch die dadurch angerichtete Finsternis das helle Licht zu erkennen, das in der Frömmigkeit leuchtet, wie sie in dem Satz aufscheint: Seelenführung, die sich vor Gott verantworten lassen soll, darf natürlich Gott nicht ins Handwerk pfuschen.

*Köln, 29., Januar 2017 \
Eckart Wilkens*

[Eckarts Bearbeitung]({{ 'assets/downloads/wilkens_erh_Protestantismus_und_Seelenfuehrung_1929.pdf' | relative_url }})

[Das Orginal von „Protestantismus und Seelenführung” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/225.pdf)
