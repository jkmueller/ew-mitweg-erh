---
title: "Eugen Rosenstock-Huessy: Man Must Teach (1959)"
category: lectures
created: 1959
published: 2023-09-14
---

### NOTE OF THE EDITOR

This is the edited transcription of one lecture April 1959.

1

The transcription was made by Frances Huessy – my work are the following changes:

1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.

2. Additions:
paragraphs, \
chapters with titles scooped from the text, \
Roman numbers for the four parts of a chapter, \
Arabian numbers for the four parts of the parts of a chapter, \
titles for the stories – which are marked by color – which communicate either a personal or historical event, \
sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
indices of contents, names, stories, sentences.

2

In this lecture is touched the following encounter with one of Rosenstock-Huessy ́s colleagues in the “Hohenrodter Bund”, Wilhelm Flitner (1889-1990). After he gave a lecture in the Volkshochschule Köln I asked him to say something to his experiences with Rosenstock-Huessy, and he said: “Er wollte immer Nachfolger haben.” (He always wanted to have successors.) I was disturbed, I understood that he didn ́t give freedom enough to his listeners – which means that Flitner didn ́t distinct between Lehrer and Führer (teacher and leader) – his word meant: Rosenstock-Huessy wanted to be a leader, he wasn ́t a teacher (at least this was what I heard).

And now here we have this tremendous confession:

*I have buried two generations of my own students in two world wars -- and so I know what it means as a teacher to survive the people one had thought that would take one's own place.*

Nachfolger (successor) meant this! Successor in time! His successor in Jena was Adolf Reichwein, in 1925, one of those mentioned by Rosenstock-Huessy. And Wilhelm Flitner stayed as professor in Nazi-Germany all the time, remaining in the space of contemporaries and surviving Rosenstock-Huessy by seventeen years!

I am grateful to have found this interpretation of “successors” and understand myself that I didn ́t answer Wilhelm Flitner in any way.

*Cologne, May 9, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH Man Must Teach 1959]( {{ 'assets/downloads/wilkens_erh_Man_must_teach_1959.pdf' | relative_url }})

- [The transcript of „ERH Man Must Teach 1959” as PDF](https://www.erhfund.org/wp-content/uploads/646.pdf)
- [The audio recordings of „ERH Man Must Teach 1959”](https://www.erhfund.org/lecturealbum/volume-21-man-must-teach-1959/)
