---
title: "Eugen Rosenstock-Huessy: Talk with Franciscans (1965)"
category: lectures
created: 1965
published: 2023-09-22
---
### NOTE

This is the transcription of two lectures, with the following changes and additions:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
  indices of contents, names, stories, sentences.

This talk with Franciscans happens to be in heaven, in the fullness of times – the Franciscans representing the presence of all times before God. And look, how eternal truth touches our daily life, our daily dangers!

*Cologne, March 27, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH The Cross of Reality (1965)]( {{ 'assets/downloads/wilkens_erh_Talk_with_Franciscans_1965.pdf' | relative_url }})

- [The transcript of „ERH The Cross of Reality (1965)” as PDF](https://www.erhfund.org/wp-content/uploads/652.pdf)
- [The audio recordings of „ERH The Cross of Reality (1965)”](https://www.erhfund.org/lecturealbum/volume-27-talk-with-franciscans-1965/)
