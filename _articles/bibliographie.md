---
title: Die Bibliographie Eugen Rosenstock-Huessys
menu: Bibliographie
category: hinfuehrung2
order: 120
---

## Ergänzungen seit 2007

Die beste Übersicht bietet noch immer:
* *A Guide to the Works of Eugen Rosenstock-Huessy* Lise van der Molen, 1997, Argo books
[Interactive Bibliography auf ERHFund.org](https://www.erhfund.org/bibliography/)

Neu herausgegeben sind:
* [The Fruit of our Lips, The Transformation of God's Word into the Speech of Mankind](https://wipfandstock.com/9781725291546/the-fruit-of-our-lips/), edited by Raymond Huessy, 2021, Wipf and Stock
* [Cartas sobre judaísmo y cristianismo](http://www.sigueme.es/libros/cartas-sobre-judaismo-y-cristianismo.html), Edicion Roberto Navarrete Alonso, 2017, Ediciones Sígueme, S.A,
* [In the Cross of Reality: The Hegemony of Spaces](https://www.routledge.com/In-the-Cross-of-Reality-The-Hegemony-of-Spaces/Rosenstock-Huessy/p/book/9781412865074#), 2017, Taylor & Francis Inc
* [Practical Knowledge of the Soul: With a New Introduction](https://wipfandstock.com/9781498282109/practical-knowledge-of-the-soul/    ), 2015, Argo Books
* [The Multiformity of Man](https://wipfandstock.com/9781620324448/the-multiformity-of-man/), 2015, Wipf and Stock
* [Des Christen Zukunft oder: Wir überholen die Moderne](https://www.rosenstock-huessy.com/ew-des-christen-zukunft/), hg. Eckart Wilkens, 2015, Agenda
* [De taal van de ziel](https://www.skandalon.nl/shop/theologie-cultuur/371-de-taal-van-de-ziel.html), vertaald door Henk van Olst en Otto Kroesen, 2014, Skandalon
* [Out of Revolution: Autobiography of Western Man](https://wipfandstock.com/9781620324431/out-of-revolution/), 2013, Wipf and Stock
* [The Christian Future: Or the Modern Mind Outrun](https://wipfandstock.com/9781620324509/the-christian-future/), 2013, Wipf and Stock
* [The Origin of Speech](https://wipfandstock.com/9781620324479/the-origin-of-speech/), 2013, Wipf and Stock
* [Speech and Reality](https://wipfandstock.com/9781620324493/speech-and-reality/), 2013, Wipf and Stock
* [Planetary Service - A Way Into The Third Millennium](https://wipfandstock.com/9781620324486/planetary-service/), 2013, Wipf and Stock
* [I am an Impure Thinker](https://wipfandstock.com/9781620324455/i-am-an-impure-thinker/), 2013, Wipf and Stock
* [Life Lines - Quotations from the Work of Eugen Rosenstock-Huessy](https://wipfandstock.com/9781620324547/life-lines/), 2013, Wipf and Stock
* [Die Kopernikanische Wende in der Sprachphilosophie](https://www.herder.de/philosophie-ethik-shop/die-kopernikanische-wende-in-der-sprachphilosophie-taschenbuch/c-27/p-4662/), hg. Stephan Grätzel, 2012, Karl Alber
* [Im Kreuz der Wirklichkeit: Eine nach-goethische Soziologie](http://www.talheimer.de/gesamtverzeichnis.html?page=shop.product_details&product_id=105), 3 Bände, hg. Michael Gormann-Thelen, Lise van der Molen, Ruth Mautner, 2009, Talheimer


## Ausgewählte Werke

Werke von Eugen Rosenstock-Huessy, die im Buchhandel, antiquarisch oder in Bibliotheken relativ leicht erreichbar sind:

Informations-Stand 18. Jan. 2005, Prof. Dr. Jürgen Frese, ergänzt von Drs Wilmy Verhage April 2007 mit dank an Drs Lise van der Molen:
* Herzogsgewalt und Friedensschutz, 1910: M & H Marcus, Breslau; Nachdruck 1969: Scientia-Verla, Aalen
* Ostfalens Rechtsliteratur unter Friedrich II., 1912, Verlag Hermann Böhlaus Nachfolger, Weimar
* Königshaus und Stämme, 1914, Felix-Meiner-Verlag, Leipzig; Nachdruck 1965 Scientia-Verlag, Aalen
* Angewandte Seelenkunde [1916] 1924, Röther-Verlag GmbH, Darmstadt
     * Practical Knowledge of the Soul, 1988
     * Toegepaste Zielkunde, 1982
* Briefwechsel mit Franz Rosenzweig [1916] 1935, Schocken-Verlag, Berlin
   * Judaism despite Christianity, 1969, Un. Of Alabama Press; 1971, Schocken Books, New York
* Daimler Werkszeitung, 1919-1920 Stuttgart-Untertürkheim: Daimler-Motoren-Gesellschaft
* Die Hochzeit des Kriegs und der Revolution, 1920, Patmos-Verlag, Würzburg
* Die Tochter, 1920, Mössingen-Talheim, Talheimer-Verlag; Nachdruck 1988 ebd.
* Werkstattaussiedlung, 1922, Julius-Springer-Verlag, Berlin; Neuauflage 1997, Brendow-Verlag, Moers
* Die Kreatur. Eine Zeitschrift, 1926-30, Verlag Lambert-Schneider, Berlin; Neuauflage 1969, Nendeln Lichtenstein, Kraus-Reprint
* Das Alter der Kirche, 3 Bde, 1927-1928, Verlag Lambert Schneider, Berlin; Neuauflage 1998, Agenda-Verlag, Münster
* Die Europäischen Revolutionen 1931, Eugen-Diederichs-Verlag, Jena; Neuauflagen 1951 und 1960 ebd.
* The Multiformity of Man, 1936, Beachhead, Norwich Vt (USA)
  * Der Unbezahlbare Mensch, 1955, Käthe-Vogt-Verlag, Berlin; Neuauflage 1964 Herder-Verlag, Freiburg/Basel/Wien
* Out of Revolution. Autobiography of Western Man, 1938, William Morrow & Co., New York; Neuauflagen 1966 und 1969, Argo Books, Norwich Vt; 1993, Berg Publishers, Providence
   * De grote revoluties, 2003, Uitgeverij Skandalon, Vught NL
* The Origin of Speech [1941-1945] 1981, Argo Books, Norwich VT; auf holländisch:
   * Het Wonder van de Taal, 2003, Uitgeverij Skandalon, Vught
* The Christian Future, 1946, Charles Scribner's Sons, New York; Neuauflage 1966, Harper & Row, New York
   * Toekomst – Het Christelijk Levensgeheim, 1993 Aalsmeer: Dabar/Boekmakerij Luyten
   * Des Christen Zukunft, oder wir überholen die Moderne, 1956, Chr.-Kaiser-Verlag, München; Neuauflage 1985, Brendow-Verlag, Moers
* Der Atem des Geistes, 1951, Verlag der Frankfurter Hefte, Frankfurt/Main; Neuauflage 1991, Brendow-Verlag, Moers, und Amandus-Verlag, Wien
* Heilkraft und Wahrheit, 1952, Evangelisches Verlagwerk GmbH, Stuttgart; Neuauflage 1991, Brendow-Verlag, Moers, und Amandus-Verlag, Wien
* Soziologie, 2 Bd., 1956-1958, W.-Kohlhammer-Verlag, Stuttgart/Berlin/Köln/Mainz
* Frankreich – Deutschland. Mythos oder Anrede? 1957, Käthe-Vogt-Verlag, Berlin
* (Zurück in) Das Wagnis der Sprache, 1957 Berlin: Kathe Vogt Verlag; Neuauflage 1997, Verlag Die blaue Eule, Essen
* Das Geheimnis der Universität, 1958, W.-Kohlhammer-Verlag, Stuttgart
* Die Gesetze der Christlichen Zeitrechnung, 1958, Münster, Agenda Verlag GmbH; Neuauflage 2002 ebd.
* Die Sprache des Menschengeschlechts, 2 Bde., 1963 und 1964, Verlag Lambert Schneider, Heidelberg
* Fruit of Lips, 1978, The Pickwick Press, Pittsburgh (USA)
   * De vrucht der lippen, 1981 Baarn, Uitgeverij Ten Have bv
* Dienst auf dem Planeten, 1965, W.-Kohlhammer-Verlag, Stuttgart/Berlin/Köln/Mainz
* Planetary Service, 1978
   * Dienen op de planeet, 1988
* Magna Carta Latina [1937], 1967 und 1975, The Pickwick Press, Pittsburgh
* Ja und Nein, Autobiographische Fragmente, 1968, Verlag Lambert-Schneider, Heidelberg
* I am an Impure Thinker, 1970, Argo Books, Norwich VT
  * Ik ben een onzuivere denker, 1996, Dabar-Luyten, Aalsmeer NL
* Speech and Reality, 1969, Argo Books, Norwich VT
   * Spraak en Werkelijkheid, 1978, Vereniging Rosenstock-Huessy Huis, Haarlem
* Friedensbedingungen der planetarischen Gesellschaft, Neuauflage 2001, Agenda-Verlag, Münster
* Unterwegs zur planetarischen Solidarität, 2006; Sammeledition von Der unbezahlbare Mensch, Dienst auf dem Planeten, Ja und Nein, Agenda-Verlag, Münster
* The collected Works of Eugen Rosenstock Huessy on DVD Argo Books via Amazon.com.

[zum Seitenbeginn](#top)
