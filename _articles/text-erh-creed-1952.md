---
title: "Eugen Rosenstock-Huessy: Creed (1952)"
category: bearbeitung
created: 1952
published: 2024-02-18
---
Eugen Rosenstock-Huessy

### Creed

(The Power of Pentecostal Sects\
Jehova ́s Witnesses \
Armageddon

three not written chapters)

### Note of the Editor

According to Lise van der Molen written about 1952 (war in Korea 25.6.1950-27.7.1953)

p. III is missing (?)

Mistakes in his typescript:\
Victorinus instead of *Victorianus*\
someone death instead of *some one death*

This is one deep-rooted handwritten explication of what we have to expect and to do after the list of mischievous events:
- *as physical death*,
- *as separation*,
- *as exile*,
- *as prison of war in Korea*,
- *as concentration camps*,
- *as emigration*.

I added the segmentation of the text, the headlines to the parts I-VI of the text, the index of names and list of sentences.

*Cologne, June 15, 2018 \
Eckart Wilkens*

[Eckart's Edition]( {{ 'assets/downloads/wilkens_erh_Creed.pdf' | relative_url }})


[The original of „Creed (1952)” as PDF-Scan](https://www.erhfund.org/wp-content/uploads/449.pdf)
