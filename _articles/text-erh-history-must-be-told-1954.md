---
title: "Eugen Rosenstock-Huessy: History must be told (1954)"
category: lectures
created: 1954
published: 2023-09-15
---


- [Eckart's Edition: ERH History must be told]( {{ 'assets/downloads/wilkens_erh_History_must_be_told_1954.pdf' | relative_url }})

- [The transcript of „ERH History must be told” as PDF](https://www.erhfund.org/wp-content/uploads/636.pdf)
- [The audio recordings of „ERH History must be told”](https://www.erhfund.org/lecturealbum/volume-11-history-must-be-told-draft-1954/)
