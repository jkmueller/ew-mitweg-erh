---
title: "Eugen Rosenstock-Huessy: The Artist and His Community (1939)"
category: bearbeitung
created: 1939
published: 2024-02-24
---
Eugen Rosenstock-Huessy

### THE ARTIST AND HIS COMMUNITY

***Commencement exercises\
at the Stuart School of the Creative Arts, Boston, Massachusetts***

June 1, 1939


### NOTE OF THE EDITOR

Transcribed from the typescript. Articulated by chapters (with titles scooped from the text), paragraphs etc. according to the change of tone, which realizes here (and elsewhere) what the author says: *I gave up music one day when I felt that the musical element ought to fuse with my intellectual life still more intimately, that my thoughts had better become musical.*

The twenty parts articulate the great joy, the whole speech being a praise of his own childhood and youth (I played on the piano in Four Wells using scores inscribed with the name of his mother Paula Rosenstock).

Each chapter paces the four fields of the cross of reality, each field again containing four steps – sixteen steps per chapter.

The reader (listener) is taken to

the future as listener\
the inspiration of being involved\
the listening to what happened and is worthwhile to be listened to just now \
the point of fact which opens the progress to the next stage

The fifth chapter is the first of the second quartet of chapters – re-opening the understanding of what was heard.

This beautiful speech is indeed the lily in Rosenstock-Huessy ́s oeuvre.

*Cologne, Decemeber 31, 2016\
Eckart Wilkens*

[Eckart's Edition]( {{ 'assets/downloads/wilkens_erh_The_Artist_and_his_community_1939.pdf' | relative_url }})

Die deutsche Übersetzung: [Der Künstler und die Gemeinde]( {{ 'text-erh-the-artist-and-his-community-dt' | relative_url }})



[The original of „The Artist and His Community” as PDF-Scan](https://www.erhfund.org/wp-content/uploads/333.pdf)
