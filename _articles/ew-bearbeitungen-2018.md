---
title: Bearbeitungen
category: methodik
created: 2018-12
---


In dem Briefwechsel zwischen Ko Vos und Freya von Moltke, den Marlouk von Lien Leenman fürs Archiv übergeben hat, steht zu lesen, daß Ko Vos während der Entstehung der erneuten Herausgabe der Soziologie von 1956/58 die Einsicht gekommen ist, daß die Zeit der Bücher vorbei ist. Oder vielmehr: daß das Lesen von der ersten Stelle an die zweite (oder dritte?) gerückt ist, das heißt vom Präjektivum, was es für die zu Rat und Tat Ge- und Berufenen seit der Reformation gewesen ist, zum Subjektivum, zum Feld, auf dem die Schätze erst ins Innere wandern müssen, ehe sie trajektiv, das heißt fruchtbar werden. Und die Editionsgeschichte mit dem Desaster, das die Eugen Rosenstock-Huessy Gesellschaft mit dem Talheimer Verlag erlebt hat, ist vielleicht Beleg für diese Erkenntnis.

So kann denn die Verwunderung, daß Eugen Rosenstock-Huessy bei so vielen Vorlesungen, die dank der Initiative von Russ Keep auf Tonband aufgenommen wurden, es dabei beließ, daß sie ihren Weg auch weiter zum Ohr, wie Ricarda Huch gesagt hat: dem Tor zum Herzen finden würden. Die Transkriptionen von Frances Huessy sind ja vordringlich dazu verfaßt worden, daß die Hörer beim Hören Hilfe hätten.

Ich bin nun dazu gelangt, etliche dieser Vorlesungen ähnlich wie die Soziologie und andere Werke Rosenstock-Huessys nach dem Gehör zu gliedern und dabei auch Editionsarbeit zu leisten, indem Floskeln der mündlichen Rede weggelassen werden und dem Leser das flüssige Verfolgen ermöglicht wird, das ein Hörer, mögen auch Störungen welcher Art auch immer, wie von selber aufbringt, weil das Gedächtnis doch streng wählt zwischen dem vom Sinn des Ganzen her Notwendigen und dem Beiwerk, das diesen Sinn, damit wir Zeit gewinnen, umgibt.

Und da kommt nun eine andere Art des Lesens heraus, die sich dem alten Gebrauch von Schrift annähert, nämlich daß sie verlautet wird. Ich habe immer so gelesen, daß ich innerlich die Worte verlautet habe – aber das ist doch längst nicht mehr Usus.

Meine Übersetzung des Gehörseindrucks mitsamt dem Fassen des Sinns fordert den Leser nun auf, die Geschwindigkeit, die das Sehen erlaubt, zu verlangsamen und also das Tor zum Herzen zu öffnen, die Hörkraft, die nämlich zwischen dem entscheidenden Augenblick und der Ewigkeit eine Brücke schlägt, von der es in einem seiner Gedichte heißt: Augenblicke, Ewigkeiten zueinander zu bekennen ist des Wortewesens Sinn.

Gerade bin ich mit der Bearbeitung der 25 Vorlesungen Universal History 1954 beschäftigt und möchte alle, die mögen, darauf anspitzen, die Zeit aufzubringen, das in Ansprache, Ausruf, Erzählung und Feststellung ausgebreitete Wissen der Soziologie von 1956/58 in aktueller Form nachzulesen.

aus: [ERHG Mitgliederbrief 2018-12](https://www.rosenstock-huessy.com/erhg-2018-12-mitgliederbrief/)
