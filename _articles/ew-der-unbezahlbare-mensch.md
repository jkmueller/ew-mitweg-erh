---
title: "Neuherausgabe von Eugen Rosenstock-Huessy: Der unbezahlbare Mensch"
created: 2022
category: bearbeitung
published: 2023-09-11
---

[![Der Unbezahlbare Mensch]({{ 'assets/images/erh-der-unbezahlbare-mensch.jpg' | relative_url }}){:.img-right.img-large}]({{ '/ew-der-unbezahlbare-mensch' | relative_url }})
<!--more-->
### Eugen Rosenstock-Huessy <br/> Der Unbezahlbare Mensch
#### Mit dem ersten Teil: Die Bruchteile des Menschen
***neu übersetzt und gegliedert***
#### Mit dem zweiten Teil: Die Wirtschaft im Großen und Ganzen <br/> und dem dritten Teil: Wem gehört der Betrieb?
***neu gegliedert***
#### von Eckart Wilkens

„Rosenstock-Huessy is a powerful and original mind. What is most important in his work is the understanding of the relevance of traditional values to a civilization still undergoing revolutionary transformations; and his contribution will gain rather than lose significance in the future.”
>*Lewis Mumford*

„Was die eigentliche Aktualität der Intuitionen Rosenstock-Huessys ausmacht, ist die Erkenntnis, dass das „soziale Problem“ auch eine geistig-kulturelle Dimension hat, die von dem nur szientistischen und materialistischen Geist nicht gesehen wird. Im Begleittext zur Neuauflage seines Buches „Der Unbezahlbare Mensch“ hat er 1962 die Fruchtbarkeit des „lebendigen Sprechens“ mit über 100 Ingenieuren unseres Hauses über die Gesetze des technischen Fortschritts erwähnt.“
>*Edzard Reuter*

„Du aber hast den Weg gebahnt, auf dem wir Kirche die verlorenen claves wiederfinden werden. Christliche Kirche ist genau insoweit Christi Kirche, als von ihr wahrhaft und wirklich Vergebung in den Raum dieser Welt ausströmt.“
>*Hans Ehrenberg*

„This book on the power of times in the life of industrial man shows clearly how we poor humans constantly have to live suspended. Between past and future, our inner world, and the world that surrounds us. We are not the one-dimensional creatures that conventional social science and the technocratic ideology have assumed us to be. It is obvious to all of us that contemporary man, under the pressure of an industrialized world, has been getting himself into a corner. We are convinced that Rosenstock-Huessy’s insights can help us to get out.”
>*Freya von Moltke and Clinton C. Gardner*

„Rosenstock-Huessy hat dem „Cogito ergo sum“ des Descartes sein Motto „Respondeo etsi mutabor“ entgegengestellt, um damit den Weg vom „ich-einsamen Denken der neuzeitlichen Philosophie zur gelebten Sprachvernunft“ im Chaos des Weltkriegsjahrhunderts zu bahnen: „Ich antworte, wenn ich mich auch wandeln lassen muss“. In der Person des Descartes entschloss sich die Menschheit, gewiss des göttlichen Segens, das dunkle Chaos der Natur in Gegenstände verstandesmäßiger Beherrschung umzuformen. Aber: ‚Insofern das Menschengeschlecht heute auf Grund gemeinsamer Anstrengung entscheiden muss, wie die Wahrheit im Sozialleben darzustellen ist, hat die cartesianische Formel nichts auszusagen.‘“
>*Karl-Ernst Nipkow*

Zur Bedeutung von ***Der Unbezahlbare Mensch*** siehe
- [Georg Müller: Der Mensch im Kreuz der Wirklichkeit](https://www.rosenstock-huessy.com/gm-mensch-kreuz-der-wirklichkeit/)
- [Otto Kroesen: Der Freiwillige ist unbezahlbar](https://www.rosenstock-huessy.com/ok-freiwillige-unbezahlbar/).

[Eugen Rosenstock-Huessy: Der Unbezahlbare Mensch, agenda, 2022](https://agenda-verlag.de/produkt/eugen-rosenstock-huessy-der-unbezahlbare-mensch/)
