---
title: "Eugen Rosenstock-Huessy: Economy of Times (1965)"
category: lectures
created: 1965
published: 2023-09-22
---
### NOTE OF THE EDITOR

1

In his letter to Georg Müller from October 18th 1965 Rosenstock-Huessy wrote the plan of the lectures in Santa Barbara:
- The Salvation of Economics
- 4 Lectures to be given at the University of California in Santa Barbara in November and December 1965
- I. The old Economy of Salvation (bis 1758)
- II. The Two Defectors: Adam Smith and Karl Marx
- III. Gold, Goods, Governments or, The Massacre
- IV. The Report on Measures by John Quincy Adams (1819) and its promise for the Planet.

You can see how the lectures were planned and present long before the first word was spoken.

2

Here is the transcription of what was spoken as transcribed by Frances Huessy, with the following changes and additions:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.

3

All these additions try to give to the reader what the listener gets by change of tone and rhythm, the inner order of speech which – according to Rosenstock-Huessys teaching of the cross of reality – has a progress through the four stations or modi:
- I and 1, title: opening a new time between speaker and listener,
- II and 2, names: finding a common ground between speaker and listener,
- III and 3, stories: commemorating a common detail of history which invigorates the common trust between speaker and listener,
- IV and 4, sentences: stating a truth which is true even outside the occasion of this speech.

4

It is rather astonishing how clear the text becomes according to this mode of giving, so to speak, a score of the speech, which otherwise – and Eugen Rosenstock-Huessy wrote on paper as though the shortage of paper for writing during the First World War continued throughout his life – seems to be too multicolored and changing most boldly and swiftly.

5

As you can see in the letters to Georg Müller: the initial preparation of the speech is firm and at least in a deciding detail new, here the study of Jonathan Edwards book:

1)

*Zu dem Zweck lese ich gerade Jonathan Edward ́s Economy of Salvation und Adam Smith ́s Economy of money. Es wäre schön, wenn mir dies noch gelänge: die Ökonomen nach den zwei Jahrhunderten Kapitalismus und Sozialismus, heimzuholen in die Heilsökonomie!*

To this purpose I am reading Jonathan Edward ́s Economy of Salvation and Adam Smith ́s Economy of money. It would be fine, if I could achieve this: to bring home the Economists into the Economoy of Salvation after two hundred years of Capitalism and Socialism.

2)

*Der größte amerikanische Theologe Jonathan Edwards (+1756!) hat seine „Ökonomie des Heils“ darauf gebaut, daß die göttliche Geschichte aus drei Teilen bestehe: 6000 Jahre, Incarnation, 3000 Jahre. Das Wunder Christi aber sei, in ein Menschenleben unsere ganze Geschichte so zu verdichten, daß sie unserem kurzfristigen Blick sichtbar zu werden vermochte!*

The greatest American theologian Jonathan Edwards (+1756!) build his “Economy of Salvation” on the fact, that the divine history exists in three parts: 6000 years, incarnation, 3000 years. The miracle of Christ being, to condense the whole history into a singular human life, so that it could become visible to our short-dated sight!

3)

*Das liegt mir gerade nahe, weil ich hier \
die Ökonomie des Heils und das Heil der Ökonomie \
doziere in sechs Vorlesungen. Ökonomie im Neuen Testament ist immer Heilsökonomie. Moderne Ökonomie ist nun das volle Gegenteil, nämlich hausloses Wirtschaften, nach der Zerstörung des Oikos!*

*Bisher habe ich Freude an der nicht zahlreichen Hörerschar. Die besten sind Franziskanermönche. sie haben die Ehre, daß Santa Barbara 1786 als Franziskanermission entstanden ist!*

*Es wird Dich interessieren, daß ich versuchen werde, das Kreuz der Wirklichkeit als ein in jeden von uns hineinverlegtes Haus zu kennzeichnen, mit seinen mindestens 2 Generationen und Innen- und Aussenwänden! Nachdem den Haussegen, den Feudalismus die feindlichen Brüder Kapitalismus und Sozialismus zerstört haben, muß jeder Einzelne statt eines Individuums ein Kreuzträger werden!*

*Es ist waghalsig, was ich da übernommen oder unternommen habe. Aber es wird auf Band aufgenommen und wenn es wider mein eigenes Erwarten gelingen sollte, wäre es ein wirklicher Schritt in ein weder kommunistisch noch kapitalistisch zersetztes Denken.*

This suggests to me immediately, because I am teaching here the Economy of Salvation and the Salvation of the Economy in six lectures. Economy of the New Testament is always Economoy of Salvation. Modern Economy is the complete counterpart, operating economically without any house, after destroying the Oikos!

Until now I rejoice in the not numerable audience. The best are Franciscan monks, they have the honor, that Santa Barbara originated 1785 as mission of the Franciscans!

You may be interested to hear, that I shall try to signify the cross of reality as a house which is put into everybody with its at least 2 generations and its walls of Inward and Outward! After the hostile brothers of Capitalism and Socialism have destroyed the house blessing, the feudalism, every single man has to be a cross-bearer instead of being an individual!

It is bold, what I have taken over and undertaken. But it will be saved on tape, und if it should succeed against my own expectation, it would be a real step into a thinking not corroded by communistic nor capitalistic thinking.

6
Encouragement and consolation are the tenor of the whole speech: I assure you, for
any person who still has something he loves, there's always plenty of time.

*March 8, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH Economy of Times (1965)]( {{ 'assets/downloads/wilkens_erh_Economy_of_Times.pdf' | relative_url }})

- [The transcript of „ERH Economy of Times (1965)” as PDF](https://www.erhfund.org/wp-content/uploads/651.pdf)
- [The audio recordings of „ERH Economy of Times (1965)”](https://www.erhfund.org/lecturealbum/volume-26-economy-of-times-1965/)
