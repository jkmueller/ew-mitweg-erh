---
title: "Eugen Rosenstock-Huessy: Fashions of Atheism (1968)"
category: lectures
created: 1968
published: 2023-09-16
---
NOTE

This is the transcript of a lecture in Eugen Rosenstock-Huessy ́s eightieth year – with the following changes and additions:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.

According to the possibilities of one single lecture there is this one main theme: Eugen Rosenstock-Huessy ́s suffering for all his lifetime, that so many and so many theologians kill God speaking about him as though He cannot listen to it. So that the “atheism” is done in speech most surprisingly by all, who do not listen themselves to what they are saying – listening understood as standing under the word.

So the speech transports what is – in Eugen Rosenstock-Huessy ́s understanding – the soul of his whole life, namely that which is – as rhythm, as one word which is proved by the whole life-time – the essence, stronger than death.
It is touching to be with Freya von Moltke who is asked to advise the speaker together with this fifteen to twenty people.

*Cologne, March 9, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH Fashions of Atheism 1968]( {{ 'assets/downloads/wilkens_erh_Fashions_of_Atheism_1968.pdf' | relative_url }})

- [The transcript of „ERH Fashions of Atheism 1968” as PDF](https://www.erhfund.org/wp-content/uploads/658.pdf)
- [The audio recordings of „ERH Fashions of Atheism 1968”](https://www.erhfund.org/lecturealbum/volume-33-fashions-of-atheism-1968/)
