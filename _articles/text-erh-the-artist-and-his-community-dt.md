---
title: "Eugen Rosenstock-Huessy: Künstler und Gemeinde (1939)"
category: bearbeitung
created: 1939
published: 2024-02-24
---
Eugen Rosenstock-Huessy

### Künstler und Gemeinde

COMMENCEMENT ÜBUNGEN\
an der Stuart School der schönen Künste\
Boston, Massachusetts

1. Juni 1939

### Nachwort des Übersetzers

Eugen Rosenstock-Huessys Schriften sind nicht so leicht zu lesen, wenn man sie
nicht hört. Er hat wirklich dem Auftrag Folge geleistet: *laß, was in der Musik für sich zu
leisten ist, mit dem Klavierspiel – und tu diese Qualität in die Darlegung, ja in das Verfassen Deiner Gedanken.* So wechselt von Satz zu Satz die Beleuchtung. Und die Absätze sind oft schon für sich kompakte Kompositionen.

Aber nicht aus dem Kalkül heraus, sondern aus dem Vertrauen auf jenes Medium
zwischen Aktiv und Passiv, von dem er spricht. Was gesprochen wird, wirkt auf den
Sprecher als Hörer zurück und erzeugt das, was noch kommt, mit. Am Anfang ist
dieser Anteil des Hörens gewaltig, gegen Ende summiert er sich und wird immer
zarter.

Dabei gibt es aber doch den Kompositionsplan von Anfang an.

Anrede (Prolog)

Präludium I\
Die schon biographisch faßbare Verbindung zwischen dem Kind Eugen Rosenstock-
Huessy in Berlin und Boston.

Präludium II\
Die Geschichte von der Mona Lisa 1913 in Florenz.

Präludium III\
Die Anknüpfung an das sichtbar dargestellte Problem Künstler und Publikum,
Künstler und Gemeinde in der New Yorker Weltausstellung.

Präludium IV\
Das Gedicht von dem alten Mann, der alle Filme gesehen hat. (Im Englischen ist der
Satz I have seen all the pictures leichter auf die gesamte bildende Kunst zu wenden.)

Fuge I\
Die Widmungen der Gebrüder Grimm an Bettina von Arnim als Zeugnis für die
Entdeckung: der Künstler, den Sie bewundern, bewundert selber das All.
Der Unterschied zu den Kennern, mit Zitaten von William Blake und George Eliot.
Gottes Schöpfung als das Künstler und Gemeinde verbindende Erleben.

Fuge II\
G. K. Chesterton und Richard Cabot\
Die Geschichte von Mr. Mc Cabe\
Das Thema: der ganze Kosmos ist die gespannte und geheime Feierlichkeit
wie die Vorbereitung zum großen Tanz\
Was heißt den Nächsten lieben wie sich selbst?\
Der wahre Schatz

Fuge III\
Neue Aufgabenstellung\
Der Apostel Paulus ist nicht Stoff\
Wie ist die Beziehung des Künstlers zum Stoff?\
Die Wörter\
Die Dichtung als Beispiel dafür, warum es Künstler geben muß, die die
Ursprünglichkeit eines elementaren Tuns wiederherstellen

Fuge IV\
Kleidung und Sprache\
Architektur\
Musik\
Der Künstler als Arzt

Postludium I\
Zwischen Barbar und Dekadent\
Die geborenen Anwälte des kostbaren Materials

Postludium II\
Was ist Kreativität?\
wand (englisch Zauberstab) und Wandlung\
Shakespeare\
Beethoven\
Leonardo\
Amy Lowell\
Emily Dickinson

Postludium III\
Der einsame Künstler

Postludium IV\
Was das praktisch bedeutet

Epilog\
Abbot Thayer am Berg Monadnock

Bei dieser Aufstellung erkennt man, wie der Sprecher zwölfmal ansetzt und immer
den Bezug zu dem Mittelpunkt behält.

In der Übersetzung habe ich die kompakten Absätze gegliedert, um den Wechsel der
Stimme deutlich werden zu lassen. Die originalen Absätze sind mit einem
Schrägstrich gekennzeichnet.

*19. März 2001\
Eckart Wilkens*

[Eckarts Übersetzung]( {{ 'assets/downloads/wilkens_erh_artist_Kuenstler_und_Gemeinde.pdf' | relative_url }})

Das amerikanische Orginal: [The Artist and His Community]( {{ 'text-erh-the-artist-and-his-community' | relative_url }})



[Das Original von „The Artist and His Community” as PDF-Scan](https://www.erhfund.org/wp-content/uploads/333.pdf)
