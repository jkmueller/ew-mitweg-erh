---
title: "Mitweg Band 1: Dienst auf dem Planeten"
category: mitweg
created: 2002-12
published: 2023-09-11
---
### INHALT

1.
DIENST AUF DEM PLANETEN \
ZUM TODE EUGEN ROSENSTOCK-HUESSYS (1888-1973) \
Rede am Freitag, dem 16. März 1973 im Forum (III) der Volkshochschule Köln

2.
LIES UND NIMM! \
Eine Art Gebrauchsanweisung für den Umgang mit den Schriften Eugen Rosenstock-Huessys (1888-1973) \
Geschrieben im Juni 1974

3.
FREIWILLIGE ARBEITSLAGER, ENTWICKLUNGSHILFE, BILDUNGSURLAUB – \
STICHWÖRTER AUSSERHALB DER ABENDVOLKSHOCHSCHULE \
Geschrieben am 16. Juli 1974, gehalten am 5. November 1974

4.
SPRACHBARRIEREN – DIE RICHTIGE FRAGE \
Dienstag, 18. Februar 1975, 20 Uhr VHS-Forum, DM 1,-

5.
JUDENTUM UND CHRISTENTUM IN DER KORRESPONDENZ \
AUS DEM DRITTEN JAHRE DES (ERSTEN) WELTKRIEGES ZWISCHEN FRANZ ROSENZWEIG (1886-1929) UND EUGEN ROSENSTOCK-HUESSY (1888-1973) \
Gehalten am 15. September 1975

6.
NAMEN, WÖRTER, DINGE – ÖFFENTLICHKEIT, \
WIE SIE DER ARBEITSPLAN DER VOLKSHOCHSCHULE KÖLN 1946-1975 SPIEGELT 1708 \
Dienstag, 16. März 1976, 20 Uhr VHS-Forum, DM 1,-


[Eckart Wilkens: Mitweg Band 1: Dienst auf dem Planeten]({{ 'assets/downloads/wilkens_mitweg_erh_1_dienst_auf_dem_planeten.pdf' | relative_url }})
