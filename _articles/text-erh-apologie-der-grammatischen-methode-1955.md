---
title: "Eugen Rosenstock-Huessy: Apologie der Grammatischen Methode (1935/1955)"
category: bearbeitung
created: 1955
published: 2025-02-18
---

### NOTIZ DES ÜBERSETZERS

#### 1
Kardinal Newman schrieb 1865 die *Apologia pro vita sua*, um den Übertritt aus der
anglikanischen und die römisch-katholische Kirche zu erzählen und zu rechtfertigen.
Weil ich meine, daß Rosenstock-Huessy, als er mit 45 Jahren (Newman´s Konversion
war im Oktober 1845, also im 45. Lebensjahr) nach Amerika eingewandert war,
entblößt von der seit dem Zusammenbruch am 9. November 1918 empfangenen
Aufgabe, Friedenserfahrung zu schaffen, die erst das Gehör für sein Lebenswerk
schaffen könnte, an dieses Ereignis im Lebensalter Henry Newman´s und die Schrift,
die zwanzig Jahre später erschien, 1865, anknüpfen wollte, habe ich *In Defense of*
übersetzt mit dem lateinisch-griechischen Wort *Apologie*. (Die zwanzig Jahre
zwischen 1845 und 1865 kehren sogar wieder, indem Rosenstock-Huessy seine
Schrift zwanzig Jahre später, nämlich 1955, noch einmal vornahm und ergänzt hat.)

#### 2
Merkwürdig genug, daß dieser Eckstein seiner Biographie: wie erkläre ich mich den
Theologen und Akademikern in Amerika, ehe ich mit der Lehrtätigkeit im Sinne der
grammatischen Methode am Dartmouth College beginne – nicht in das Sprachbuch
*Die Sprache des Menschengeschlechts, eine leibhaftige Grammatik in zwei Teilen von
1963/64* gelangt ist. Erstmals veröffentlicht 35 Jahre nach der ersten Niederschrift in
der Trias der ersten Bücher des Verlages Argo Books, die Clinton Gardner trug, in
*Speech and Reality*, 1970, S. 9-44 als Chapter 1. (Aus diesem Buch kamen ins
Sprachbuch:
- *Chapter 2: Articulated Speech 1937* – Vom Artikulieren
- *Chapter 6: The Listener´s Tract 1944* – Hörern und Sprecher – Aufhören und Lossagen
- *Chapter 4: Grammar as Social Science 1945* – In Beziehung Treten Teil 1
- *Chapter 5: How Language Estabilishes Relations 1945* – In Beziehung Treten Teil 2
- *Chapter 3: The Uni-versity of Logic, Language and Literature 1935* – Die Einsinnigkeit von
Logik, Linguistik und Literatur
- *Chapter 7: The Individual´s Right to Speak 1945* – Des Individuums Recht auf Sprache

mithin alle sechs Kapitel (für das Sprachbuch übersetzt und bearbeitet) – nur das
erste nicht. Gab es noch keine Übersetzung?)

Im Vorwort zu *Speech and Reality* schreibt Clinton Gardner: *The essays in this book do
not follow one another in any logical scheme except that the first chapter is the most all-
embracing statement of the theme.* Die Aufsätze in diesem Buch folgen keinem
begründbaren Plan, außer daß das erste Kapitel das Thema am umfassendsten
darstellt.

#### 3
Herausgegeben von Rudolf Hermeier erschien die Apologie von 1935 unter dem Titel:
*In Verteidigung der grammatischen Methode* (1939/1963? – also falschen Daten) S. 287-
326 in: *Friedensbedingungen einer Weltwirtschaft. Aus dem Englischen übersetzt von Anna
Siemsen; teilweise vom Autor durchgesehen, doch offensichtlich in nicht-druckfertiger Weise,
deshalb nochmals vom Herausgeber überarbeitet.*

In dem Band 14 der *Arnoldshainer Schriften zur Interdisziplinären Ökonomie* mit
insgesamt 16 Beiträgen ist die *Verteidigung der grammatischen Methode* der einzige aus
dem Englischen übersetzte, der einzige aus den Jahren 1933 bis 1950, als Eugen
Rosenstock-Huessy sein Wirken in Deutschland persönlich redend und
veröffentlichend wieder aufnahm. Und wie seltsam der Umgang mit diesen 17
Jahren ist, zeigt der Passus in dem Lebenslauf, den Rudolf Hermeier beifügt.

Dieser ist in vier Abschnitten vorgetragen:
1. *Als Christ aufgewacht*
2. *Als Gewandelter 1918 heimgekehrt und fortan gelebt*
3. *Als Teilnehmer am Mahle des HERRN zum Exodus bereit*
4. *Als Argonaut den Weg ins 3. Jahrtausend gewiesen*

Und der Abschnitt 3 lautet:\
*1960 sagte Rosenstock in Heidelberg: Das Abendmahl „ist ein Haushaltsvorgang, in dem sich
die Menschen mit Kräften ausrüsten lassen von einer bekannten Vergangenheit, einer
geliebten und vertrauten Vergangenheit für eine den Tod bewältigende Zukunft“. In der
Bereitschaft, zur rechten Stunde die geliebte Heimat zu verlassen, kann also ein Kriterium für
die wirksame Teilnahme am Mahle des HERRN gesehen werden. Rosenstock sprach aus einer
bewährten Einsicht heraus. Er hatte 1919 ein „Lügenkaisertum“ vorausgesagt und dann
nach Kräften versucht, sein Kommen zu verhindern – insbesondere hatte er auch scharf den
ab Mitte der zwanziger Jahre immer stärker ins Kraut schießenden Führerkult bekämpft. Die
sogenannte „Machtergreifung“ 1933 empfand er als „Besiegter“ und als Freigesprochener.
Sein Antrag auf vorübergehende Schließung der juristischen Fakultät in Breslau fand nicht
die notwendige Unterstützung. Er entschloß sich zur Auswanderung nach Amerika.*

Mir kommt es so vor, als wäre in der Weigerung den Namen vollständig zu nennen:
*Rosenstock-Huessy* die Kälte zu spüren, mit der der tatsächlich erzwungene Bruch in
dem Lebensatem 1933 und die tatsächlich durch neuen Aufbruch in Amerika
bezeugte Kraft übergangen ist, zugedeckt mit einem *Wort-Credo* (Goethe, *Kampagne
in Frankreich*, 30. August 1792), das in die Formen landeskirchlicher
Abendmahlspraktik zurückweist, um dann selber – als frommer Mit-Christ - gut
dazustehen (Rudolf Hermeier lebte vom 1. Dezember 1929 bis zum 4. August 2009,
war also im 16. Lebensjahr, als der Hitler-Wahn zusammenbrach).

Und diese Kälte wirkt auch in der von Anna Siemsen verfaßten und wie immer
bearbeiteten Fassung der *Apologie der grammatischen Methode*.

#### 4
Die vorliegende Übersetzung versucht, das Unerhörte des Aufatmens nach der
Zerstörung der deutschen Sprache durch Hitler, das Finden einer neuen Sprache zu
neuem Aufbruch sowohl in die Vergangenheit wie in die Zukunft getreu
wiederzugeben, so daß dieses Bekenntnis in der Lebensmitte seinen Rang bekommt,
daß es sowohl nach rückwärts wie nach vorwärts, nach innen wie nach außen
balancierend spricht. Das heißt – um mit dem Aufsatz zu reden – Frieden schließt.

*Nach rückwärts*: Rosenstock-Huessy weckt Verständnis dafür, warum die Jugend in
Deutschland die alten Lehren nicht hören konnte, er gibt dafür die Zeitspanne 1914
bis 1923 an, es mangelte ihr an der Erfahrung des Friedens, ohne die kein Gehör für
Sozialwissenschaft zu erwarten ist.

*Nach vorwärts*: Rosenstock-Huessy ruft alle Intellektuellen auf dem Planeten Erde
dazu auf, die grammatische Methode als Schwerpunkt zukünftigen Wirkens zu
üben.

*Nach innen*: Rosenstock-Huessy spricht zu den wirklichen Kollegen und Studenten
am Dartmouth College, wo er von 1935 bis 1957/58 gewirkt hat.

*Nach außen*: Rosenstock-Huessy fordert die ganze Welt der abendländischen
Hochschule heraus, indem er zum Friedensschluß zwischen Scholastik, Akademik und
Argonautik (so nennt er das Wirken nach der grammatischen Methode) aufruft – und
ohne solchen Friedensschluß zwischen den Fakultäten müsse die Wirksamkeit der
Hochschulbildung im ganzen verfallen, weil das Vertrauen der Studenten
verlorenginge, daß dort etwas Wichtiges zu lernen wäre.

#### 5
Die vier genannten Aspekte durchdringen die ganze Darstellung. Um sie deutlich zu
machen, habe ich den Text der sechs darstellenden Teile in Kapitel mit einer von mir
hinzugefügten Überschrift, Abschnitte (mit römischen Ziffern I-IV), Absätze (mit
arabischen Ziffern 1-4 und Absätze) gegliedert. Dadurch wird der Leser
„aufhaltsam“, bekommt Zeit, die Wendungen der Aspekte mitzuvollziehen.

Ergänzend dazu:

Inhaltsverzeichnis (für den Leser, der vorher nichts gelesen hat, der präjektive Teil des
Zur-Kenntnis-nehmens),

Namensverzeichnis (das zeigt, welche Namen bei den Lesern als bekannt
vorausgesetzt werden, also *Zusammengehörigkeit* bezeugen),

das Notieren der Geschichten, die von Erlebtem erzählen, wofür der Sprecher bürgen
kann, zeigt den *trajektiven* Teil der Rede an, der auf das Erlebte zurückweist, die
Merksätze – ähnlich wie die Thesen im siebten Teil – stellt die Sätze heraus, die auch
über das Verfolgen der Darstellung hinaus, also gewissermaßen außen,
Aufmerksamkeit beanspruchen dürfen.

Die grammatische Methode gliedert – nach meiner Darstellung – das Entstehen der
Rede schon im Augenblick des Niederschreibens (in der Vorstellung, es wären
wirkliche Menschen zu einem wirklichen Zeitpunkt da, denen die Darstellung gilt).

#### 6
Englische und deutsche Satzmelodie sind verschieden, und deshalb wirkt es nur
bleiern, wenn die englischen Sätze fast modulationslos übersetzt werden, indem der
englische Satz Wort für Wort abgetrottet wird. Deshalb habe ich die volle
Beweglichkeit innerhalb der Sätze in Anspruch genommen.

Ähnliches gilt für die Wortwahl: der Schreiber setzt – in diesem Stück –
Lateinkenntnis voraus, richtet sich also an Leser mit solcher Vorbildung. Und die
englische Sprache besteht in solchem Maße aus Wörtern lateinischer Herkunft, daß
das Konzise lateinischer Formulierung auch bei englischen Texten zu gespannter
Aufmerksamkeit auffordert. Bei der Übersetzung ist daher – so meine ich – darauf zu
achten, daß sich die Sätze nicht überstürzen. Wechsel von Konstruktionsformen
(Aktiv/Passiv – Partizipialkonstruktion/ Nebensatz), Einfügen von vermittelnden
Konjunktionen oder Teilnahme heischenden Wörtchen wie *wohl, ebenso, ja* usw.
erleichtern hoffentlich die Lektüre.

#### 7
Über eine Schwelle freilich muß der Leser gelangen, die das ganze Stück
zusammenhält, nämlich das Bekenntnis, daß es bei allen drei Formen der
abendländischen Hochschule darum geht, der *Allgegenwart Gottes* zu begegnen (S.
70). Eben auf drei ganz verschiedene Weisen.

#### 8
Der Übersetzer ist im Jahre 1942 geboren, teilt also mit den Jahrgängen, die die Jahre
1914 bis 1923 als prägend erlebt haben, den Mangel an Friedenserfahrung.

In mehr als fünfzig Jahren Lehrtätigkeit in der Andragogik (1968-2000 an der
Volkshochschule Köln, seither in privat weitergeführten Kursen zu Musik, Literatur
und Bildender Kunst, seit 2005 als Vorstandsmitglied in der Eugen Rosenstock-
Huessy Gesellschaft) habe ich die grammatische Methode bestätigt gefunden: und
wie die bis 1914 vorausgesetzten Friedenserfahrungen in Kirchengemeinde,
Konzertsaal, Universität, Familie, Beruf usw. gefährdet sind und nur dadurch das
Gehör herbeikommt, daß das Lehren ohne Prüfungsziel geübt wird.


*Köln, 29. August 2019\
Eckart Wilkens*

[Eckart's Übersetzung]( {{ 'assets/downloads/wilkens_erh_Apologie_der_grammatischen_Methode.pdf' | relative_url }})


[Das Orginal „In Defense of the Grammatical Method (1939 and 1955)” ist Teil des PDF-Scan](https://www.erhfund.org/wp-content/uploads/589.pdf)
