---
title: "Eugen Rosenstock-Huessy: The Cruciform Charakter of History (1967)"
category: lectures
created: 1967
published: 2023-09-17
---
### NOTE OF THE EDITOR

Transcription of the text as it was written after the tape by Frances Huessy, with the following changes and additions:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.

I hope that one can read the change of direction which is characteristic for this cruciform of speech – which in itself is a proof of what is said.

The titles show you the expectation which is aroused, \
the names introduce you into the whole heaven of names which incorporate you into the promise to Abraham and his seed, \
the stories make you sure that a common history is meant and spoken of, \
the sentences (marked by me) give the essence of what is said.

The consciousness of being old and having the opportunity to speak again of what is the essence of his existence for the whole lifetime from 1888 to 1973 makes the speech the more serious.

*Cologne, March 15, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH The Cruciform Charakter of History]( {{ 'assets/downloads/wilkens_erh_The_cruciform_character_of_history.pdf' | relative_url }})

- [The transcript of „ERH The Cruciform Charakter of History” as PDF](https://www.erhfund.org/wp-content/uploads/656.pdf)
- [The audio recordings of „ERH The Cruciform Charakter of History”](https://www.erhfund.org/lecturealbum/volume-31-cruciform-character-of-history-1967/)
