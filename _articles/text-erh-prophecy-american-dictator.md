---
title: "Eckart's making current of Rosenstock-Huessy's Prophecy of an American Dictator (1954)"
category: vertiefung
created: 1954
published: 2023-04-27
---
in his *Nineteenth Lecture: The Gospel and the Fourth Century* of the *Universal History lecture series* in 1954 *Rosenstock-Huessy* made the following statements:
<hr>
...

*If you abolish this Creed, gentlemen, you are back to pharaoh.*

*So please laugh at your own stupidity, and erase from your vocabulary this sneer with which you say, "Oh I'm not dogmatic." Poor people who are not dogmatic, because they cannot affix any epoch to their thinking.*

*In 325, freedom came into the world of politics, because every human being today can say to the powers that be, "Don't you remember that the righteous one has been crucified by you, by the powers that be? You are not proven so far as being just. Even though you were elected by the people, you may be a bloody tyrant." \\
That's very serious gentlemen, it's nothing to laugh about.*

*But you learn nothing from history. Hitler is nothing to laugh about. He killed, after all, six million people in gas chambers. So it's natural that he should say, "I ..." he's Christ, the only basis on which he could extend his power. And the Creed was forgotten.*

*Well, we aren't so very far from this, gentlemen.* ***Fifty years from now, this country will have a dictator, and then the Creed, which you now have spit at and make ridiculous, would come very handy. You with your undogmatic Christianity and your ethical culture, and all this spineless, beliefs and opinions and what-not.***

*This was paid in blood, gentlemen. Athanasius was saved by the monks, from 325 to 371, this patriarch of Alexandria - he was archbishop of Alexandria - had to live off and on in the desert with the monks, because he was doing evil in the eyes of the emperors. The emperors didn't like it.*

*The successors of Constantine wanted to get away from the Nicene Creed again, because it stood in the way of their own authority. So from 325, gentlemen, to 371, the Athanasian Creed was in abeyance. He was only allowed to return in 375 to his seat and for four more years, the good man was allowed to act as archbishop of Alexandria.\\
When he said good-bye to the monks in the desert, who had sheltered him and protected him, they said, "Don't forget us, Father Athanasius, when you now return to the empire and become the archbishop again." And he said, with the words of the 147th Psalm, I think it is, "If I thine forget, O Jerusalem, my right hand should dry up ..."*

....
<hr>

Eckart added a heading which was not taken out of the text, as he normally did, but signalled a fulfilment of Rosenstock-Huessy's prediction:

**THE PROPHECY OF DONALD TRUMP**

*Well, we aren't so very far from this, gentlemen. Fifty years from now, this country will have a dictator, and then the Creed, which you now have spit at and make ridiculous, would come very handy. You with your undogmatic Christianity and your ethical culture, and all this spineless, beliefs and opinions and what-not.*

<hr>
[Eckart's Edition]( {{ 'text-erh-universal-history-1954' | relative_url }} )
