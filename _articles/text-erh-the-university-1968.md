---
title: "Eugen Rosenstock-Huessy: The University (1968)"
category: lectures
created: 1968
published: 2023-09-16
---
### NOTE

This is the transcription of a single lecture, with the following changes and additions:

1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.

One of the main themes of his life: the university – the perseverance with which he strove to renew the universal history – to a new form of assisting the next Council – ecumenical or what not.

And this true to what he experienced together with Franz Rosenzweig and Benno Jacob – the listening to Genesis 1, 1.

*Cologne, March 9, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH FThe University 1968]( {{ 'assets/downloads/wilkens_erh_The_University_1968.pdf' | relative_url }})

- [The transcript of „ERH Fashions of Atheism 1968” as PDF](https://www.erhfund.org/wp-content/uploads/659.pdf)
- [The audio recordings of „ERH Fashions of Atheism 1968”](https://www.erhfund.org/lecturealbum/volume-34-the-university-1968/)
