---
title: "Vorstellung Rosenstock-Huessys: Teil 2 – Lehre"
category: hinfuehrung
order: 101
---

Eugen Rosenstock-Huessy lehrt das Kreuz der Wirklichkeit: Alles Erlebte muss sich in vier Aggregatzuständen bewährt haben, ehe es als Teil der Wirklichkeit angesprochen werden kann: als Verheißung, als Wunsch, als Erfahrung, als Faktum.

Die Verheißung weist auf etwas hin, was noch nicht da ist, aberzum Beispiel in der Form des Imperativs: Tu das!doch schon. Etwas existiert nur zwischen zweien, Sprecher und Hörer. Das ist der Aggregatzustand DU.

Der Wunsch entsteht in dem Angesprochenen, er oder sie möchte in mächtig quellender Weise dem Imperativ entsprechen, sehnt sich danach, möglichst viele Miterlebende dafür zu finden: Freude, schöner Götterfunken. In diesem Aggregatzustand ICH blüht das Leben.

Die Erzählung ist nur möglich, wenn Sprecher wie Hörer etwas gemeinsam erlebt haben, das nun in der Erzählung eines Einzelnen Wort wird. Die Verbindung der vielen ruft das Wort in dem einen hervor. Dennoch ist es der Ausdruck des Aggregatzustandes WIR. Wer es hört, wer es liest, findet für das Erlebte darin eine gedächtnisstiftende Gestalt. Andere können in das Erleben der Gruppe wohl eintreten, bedürfen dazu aber der Einfühlsamkeit und der Bereitschaft, gelebtes Leben als Vorbild weiterzutragen.

Fakt ist das, was unabhängig von dem Erleben als Totes existiert, was durch die Wiederholung des Geschehens keine Veränderung leidet. Das ist der Aggregatzustand ES, in dem sich alles Untersuchte befindet, von dem anzunehmen ist, dass es durch die Untersuchung selber keine Veränderung erfährt. So ist es heißt es davon, es ist beweisbar.

Diese vier Erfahrungsweisen scharen uns in verschiedener Weise zusammen. Eugen Rosenstock-Huessy hat gesehen, dass wir sie als zwei Zeiten und zwei Räume erkennen können. Die zwei Zeiten sind: das Noch-nicht-aber-schon-da echter Zukunft und das Gestaltgewordene, das sich Traditionen und Institutionen geschaffen hat, also alle gebahnten Wege umfasst, die es einem ins Leben tretenden Menschenkind ermöglicht, nicht alles von vorne anzufangen.

Die zwei Räume sind: der Innenraum geistig Zusammengehöriger, die durch ein Vorhaben, eine Idee, eine Beschäftigung unabhängig von der Verschiedenheit nach Datum und Ort miteinander verbunden sind, und der Außenraum, der mit Geräten, die die Arbeit der Sinne fortsetzen ins Unendliche hinein, zu messen ist und Ergebnisse liefert, die von dem Zeitpunkt der Messung unabhängig bleiben.

Der allgemein gängigen Vorstellung, es gäbe Zeit und Raum, den Raum sogar in drei, vier oder fünf Dimensionen, wird so ein Modell entgegengestellt, das es zwar sprachlich schon immer gegeben hat, zum Beispiel in den Verbformen des Imperativs, des Optativs, des Narrativs und des Indikativs, das aber doch ein radikales Umdenken erfordert.

Arthur Schopenhauer sprach von Wille und Vorstellung als Repräsentanten des Innenraums und des Außenraums. Friedrich Nietzsche sprach von Übermensch und zerrüttetem Europa, damit die zeitlichen Pole immerhin andeutend. Eugen Rosenstock-Huessy fügte diese Konzepte zu einem Leben in der Vierzahl.

Dabei entdeckte er, dass eine Vierzahl den Menschen nach dem 19. Jahrhundert begegnet, wobei aber jede unverbunden ganze Gefolgschaft verlangt:

* Sigmund Freud durchbrach alle Scham, um falsche Zukunft zu Grabe zu tragen,

* Friedrich Nietzsche widerrief alle Verheißung, um die echten Wünsche der Kreatur Mensch wieder hörbar zu machen,

* Charles Darwin setzte seine Idee von einer katastrophenlosen, allmählichen Evolution dem geschichtswunden kirchlichen Gewissen entgegen,

* Karl Marx entzauberte die Arbeitskraft als Motor allen Geschehens.

Diese Vier lebten auf den Weltkrieg zu, in dem ihre Prophezeiung von dem Ende jede auf ihre Weisevollzogen wurde. Schamlosigkeit, Verheißungslosigkeit, Vertrauensverlust in die jahrtausendealte Tradition, Trotz gegen jede Form der Gegenseitigkeit im Klassenkampf das ist das Ergebnis für alle die, die in dem Weltkrieg, dessen rückwärtsgerichteter Teil die Russische Revolution als Teilgeschehen ist, nichts als die Zerstörung sehen können.

So ist die Stunde für die Entdeckung des Kreuzes der Wirklichkeit in zwei Zeiten und zwei Räumen der Erste Weltkrieg gewesen, bei täglich drohender Vernichtung im Felde, im Jahre 1916/1917.

Einer der ersten Freunde, der sie mitgeteilt wurde, ist Franz Rosenzweig gewesen.

Die Bibliographie der Werke Eugen Rosenstock-Huessys ist umfangreich. In zahllosen Büchern, Aufsätzen, Reden hat er seine Lehre niedergelegt. Und doch sind es vier Werke, in denen die Stationen des Wirklichwerdens seiner Lehre zu sehen sind.

## Die vier Stationen der Lehre

1. Station: das Verkünden

    Die erste Station ist die des Verkündens, wie es ganz neu gesagt wird. Dafür steht das Buch „Die Hochzeit des Kriegs und der Revolution”, Patmos-Verlag, Würzburg 1920. Es zeigt den tränenschweren Auszug aus der gewohnten Welt des 19. Jahrhunderts. Die letzten Kapitel haben prophetischen Charakter, besonders auch das Kapitel Die Tochter, in welchem angesagt wird, dass künftig die Begeisterung nicht nur und vielleicht nicht mehr von dem Sohn erlebt wird, sondern von der Tochter, der Tochter Gottes, die sich, wie vorher der Sohn, in eigener Inspiration von Gottes Geist her verstehen lernt, unabhängig von dem irdischen Vater (und Gatten).

2. Station: Die Europäischen Revolutionen

    Die zweite Station ist das Buch zu den Europäischen Revolutionen, das in zweierlei, ja dreierlei Gestalt vorliegt, nämlich in der Ausgabe auf deutsch von 1931, in der Ausgabe auf englisch von 1938 und in der erneuerten deutschen Ausgabe von 1951, den Zweiten Weltkrieg sozusagen durchziehend und überwindend. Entgegen der Romantik und der Utopie (in welchen er das Heraufdämmern des Konzepts von Innenraum und echter Zukunft erblickte) fordert er auf, beherzt die nahe Geschichte zu vergegenwärtigen, nämlich das Zweite Jahrtausend nach Christus (statt der Antiken und naturwissenschaftlich wie man zum Beispiel in jeder Zahnarztpraxis lesen kann in immer weiterer Verflüchtigung bis in die Milliarden Jahre der Sternexistenzen), und das Element der echten Zukunft, eben die Geschichtsbrüche, die einen neuen Menschentypus hervorgebracht haben, als unerlässliches Erbe anzuerkennen.
    Das deutsche Buch von 1931 ist auf hergebrachte Weise konzipiert: zuerst die Theorie, dann die chronologische Erzählung. Das amerikanische Buch von 1938 dreht diese Reihenfolge um, erzählt erst die Geschichte der Russischen Revolution, dann weiter rückwärts die der Französischen, der englischen, der deutschen Revolution (die man als solche anzusprechen nicht gewohnt ist), schließlich dann doch chronologisch die Geschichte der Umbrüche während des Mittelalters, in denen sich auf der Ebene der geistlichen Herrschaft schon abgespielt hat, was die neueren Revolutionen in säkularer Weise hervorgebracht haben. Eingeschoben sind die Geschichte Österreich-Ungarns und der Vereinigten Staaten als Verheißungen auf eine multikulturelle Gesellschaft nach den Weltkriegen.
    Die zweite deutsche Ausgabe bleibt bei dem einmal gefassten Aufbau, enthält aber viele Ergänzungen teils aus dem amerikanischen Buch, teils auch wieder ganz neu.

    In Form der Familienmitglieder sieht Rosenstock-Huessy die Menschentypen in den einzelnen Nationen betont und hervorgebildet: das Mütterliche in der Kirche (Italien), das Väterliche in der Lutherischen Verfassung aus Landesfürst und Räten, das Männliche in der Verfassung der englischen Seefahrer, die eine unabhängige Zuflucht auf dem Lande daheim brauchen, das Frauliche in der französischen Lebensart, die sich den Salon als Vermittlungsstelle alles politischen und kulturellen Geschehens schuf. Der Verlorene Sohn wird in der Russischen Revolution erkennbar; im Weltkrieg aber, wie schon gesagt, die Tochter.
    Das Buch appelliert an den Leser, in der „Autobiographie des abendländischen Menschen” sein eigenes Leben, die eigene Erfahrung zu erkennen und von daher zu ergänzen und zum Leben zu bringen.
    Diese Darstellungsform ist die der Andragogik, das heißt die Rede von Erwachsenen miteinander, nachdem sie Fachleute geworden sind, um des Nächsten willen aber, des Laien in sich selber und dem Hörer, diese Fachlichkeit überwinden. Die Fachlichkeit äußert sich in System, Register, Fakten, Fußnoten usw. Das Überwinden aber in dem Ton und dem quasi-mündlichen Duktus des Erzählens. Die Spanne zwischen System und Monographie zeigt sich bei jedem einzelnen Kapitel.
    Wer also die Rede des Fachmanns erwartet, wird verwirrt, wer an dem Überwinden teilnimmt, fühlt sich angesprochen und zur Ergänzung aufgerufen.

3. Station: Die Soziologie in zwei Bänden

    Die dritte Station des Wirklich-Werdens der Lehre Eugen Rosenstock-Huessys ist die Soziologie in zwei Bänden, erschienen in Stuttgart 1956 und 1958. Nach vierzig Jahren die Frucht des Lebens und der Lehre.
    Der erste Band, mit dem ins 19. Jahrhundert und auf das Europa der Naturwissenschaften weisenden Titel „Die Übermacht der Räume” erschien schon 1924 unter dem Titel „Die Kräfte der Gemeinschaft”. Da findet man aber auch in dem andragogischen Stil -, was man von der Gesellschaft der Menschheit, wie sie jetzt gerade zusammenlebt, alles Wünschenswerte: eine systematische Darstellung der wirkenden Kräfte im Kreuz der Wirklichkeit. Besonders wichtig ist die Einsicht in das, was Rosenstock-Huessy das Reflexivum nennt, die Tatsache, das sich alle Kräfte in Ernst und Spiel (eben Reflexivum) auswirken möchten und dass wir ohne die Möglichkeit des Spielens (z.B. in Liturgie, Kunst, Reisen, Sport) nicht genügend Geistesgegenwart hätten, mit den ernsten Notlagen fertigzuwerden.
    Der zweite Band ist eine Universalgeschichte, aber eben nicht in dem Ton des „Es war einmal”, sondern „Es geht Dich an”. Unendliche Forschungsarbeit ist in dieses Werk eingeflossen, das uns die vier vorchristlichen Antiken in Stamm, Reich, Israel und Griechenland vorstellt und die drei nachchristlichen Jahrtausende mit den drei Singularen Ein Gott, Eine Welt, Ein Menschengeschlecht präsentiert und verheißt. Der Inhalt ist nicht kühl dargeboten, sondern so, dass der Leser beständig das Gefühl hat: es brennt eine ansteckende Begeisterung, die aber auch abverlangt, eigene Lebenszeit für die Integration so vieler neuer Sichtweisen und Gegenstände herzugeben.

4. Station: Die Sprache des Menschengeschlechts

    Die vierte Station endlich stellt das Buch auch in zwei Bänden dar: „Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen”, erschienen in Heidelberg 1963 und 1964. Der Leser dieser Darstellung erkennt schon, dass das Kreuz der Wirklichkeit die Gestalt dieses Werkes ganz bestimmt hat. Es geht nicht um die Sprachwissenschaften, die einzelne Sprachen und Sprachstrukturen untersuchen, katalogisieren, systematisieren es geht um die Frage: Was tun wir Menschen eigentlich, wenn wir sprechen? Und tun wir es alle gemeinsam und von demselben Grunde aus? Gehört das Hören dazu? Wir wird gesprochen, wie wird gehört? Und das nicht nur gegenwärtig, sondern mit genauem Verfolgen dessen, was in der „Soziologie in zwei Bänden” schon durchgeführt ist: wann sich die Sprache gewandelt hat.
    Krönung dieses letzten großen Werkes ist das Kapitel Die Frucht der Lippen, in welchem dargestellt wird, wie die vier Evangelien des Matthäus, Markus, Lukas und Johannes die Vierfalt der Kreuzeswirklichkeit offenbaren, weil jeder zu einem anderen Zeitpunkt und zu anderen Adressaten sprach.

## Jesus – die Mitte der Zeiten

Damit ist ein weiterer Stein des Anstoßes angesprochen: dass Eugen Rosenstock-Huessy die christliche Zeitrechnung zum Sine-qua-Non erhebt. Selbst die Atheisten sagt er müssen anerkennen, dass Jesus die Mitte der Zeiten ist, weil erst nach ihm die bisher getrennt gebliebenen schöpferischen Ströme der Menschwerdung in der Geschichte einander geöffnet wurden und zu den drei Singularen: Ein Gott, Eine Welt, Ein Menschengeschlecht zusammenkommen können. Das mag eine harte Nuss sein, aber dem steht entgegen, dass es im Dritten Jahrtausend nach Christus um nichts weniger geht als um die gegenseitige Durchdringung aller vier: der Zeiten und Räume.

Und dass der Singular durchdringt durch den Wirrwarr des Plurals allenthalben, ist nur dort möglich, wo der Tod überwunden wird. Der Tod des Kaiserreichs war es bei der „Hochzeit des Kriegs und der Revolution”, der Tod Europas bei dem „Revolutionenbuch”, der Tod der abendländischen Geschichte bei der „Soziologie”, der Tod der Sprache in den KZs Hitlers im „Sprachbuch”.

Für diese Todesüberwindung hat er ins Licht vier Todesüberwinder aus der Zeit vor Jesus gehoben: Abraham für Israel, Buddha für die Stämme, Laotse für die antiken Reiche, Jesus für die griechische Leibbesessenheit. In Jesus münden alle diese Kräfte zusammen denn alle vier, Abraham, Buddha, Laotse und Jesus, bilden, wie es auch alle Mystiker unermüdlich sagen, den Innenraum der Heilsgeschichte.

Neben diesen vier Säulen gibt es für jede eine ganze Schar von begleitenden Satelliten in Buch- und Aufsatzform. Was davon antiquarisch oder in Bibliotheken verhältnismäßig leicht erreichbar ist, zeigt folgende Liste:

(Informations-Stand 18. Jan. 2005, Prof. Dr. Jürgen Frese, ergänzt von Drs Wilmy Verhage, 2007)
* Herzogsgewalt und Friedensschutz, 1910
* Ostfalens Rechtsliteratur unter Friedrich II., 1912
* Königshaus und Stämme, 1913
* Angewandte Seelenkunde [1916] 1924
  * Practical Knowledge of the Soul, 1988
  * Toegepaste Zielkunde, 1982
* Briefwechsel mit Franz Rosenzweig [1916] 1935
  * Judaism despite Christianity, 1969, 1971
* Daimler Werkszeitung, 1919-1920
* Die Hochzeit des Kriegs und der Revolution, 1920
* Die Tochter, 1920, 1988
* Werkstattaussiedlung, 1922
* Die Kreatur. Eine Zeitschrift, 1926-30
* Das Alter der Kirche, 3 Bde, 1927-1928, 1998
* Die Europäischen Revolutionen 1931, 1951, 1960
* The Multiformity of Man, 1936
  * Der Unbezahlbare Mensch, 1955
* Out of Revolution. Autobiography of Western Man, 1938, 1966
  * De grote revoluties, 2003
* The Origin of Speech [1941-1945] 1981
  * Het Wonder van de Taal, 2003
* The Christian Future, 1946, dt 1955
  * Toekomst – Het Christelijk Levensgeheim,1993
  * Des Christen Zukunft, oder wir überholen die Moderne, 1956
* Der Atem des Geistes, 1951
* Heilkraft und Wahrheit, 1952
* Soziologie, 2 Bd., 1956-1958
* Frankreich – Deutschland, 1957
* Das Geheimnis der Universität, 1958
* Die Gesetze der Christlichen Zeitrechnung, 1958, 2002
* Friedensbedingungen der planetarischen Gesellschaft, 1959, 2001
* (Zurück in) Das Wagnis der Sprache, 1957, 1997
* Die Sprache des Menschengeschlechts, 2 Bde., 1963, 1964
* Die Frucht der Lippen, 1964
  * Fruit of Lips, 1978
  * De vrucht der lippen, 1981
* Dienst auf dem Planeten, 1965
  * Planetary Service, 1978
  * Dienen op de planeet, 1988
* Magna Carta Latina [1937], 1967, 1974
* Ja und Nein, Autobiographische Fragmente, 1968
* I am an Impure Thinker, 1970
  * Ik ben een onzuivere denker, 1996
* Speech and Reality, 1969
  * Spraak en Werkelijkheid, 1978
* Unterwegs zur planetarischen Solidarität, Sammeledition von: Der unbezahlbare Mensch, Dienst auf dem Planeten, Ja und Nein, 2006

Eckart Wilkens\
Köln, 24. April 2007

[zum Seitenbeginn](#top)
