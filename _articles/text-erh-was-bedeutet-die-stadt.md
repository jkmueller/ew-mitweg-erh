---
title: "Eugen Rosenstock-Huessy: Was bedeutet die Stadt für das christliche Leben? (1949)"
category: bearbeitung
created: 1949
published: 2023-02-24
---
*Über die künftige Beziehung zwischen der Stadt des Menschen und der Stadt Gottes*

in: Zeitwende, 21. Jahrgang, Oktober 1949, Heft 4, p.241-251

in der Abschrift gegliedert, mit Inhalt, Namensregister, Liste der Geschichten und Merksätzen und einer erklärenden Notiz\
von Eckart Wilkens

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Was_bedeutet_die_Stadt_fuer_das_christliche_Leben_1949.pdf' | relative_url }})

[Das Orginal von „Was bedeutet die Stadt für das christliche Leben? (1949)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/425.pdf)

The English original text: [Die Grosstadt](https://www.rosenstock-huessy.com/text-erh-die-grossstadt/) was published in: Die Sprache des Menschengeschlechts, Bd1, p. 221-237
