---
title: "Eugen Rosenstock-Huessy: Letters to Cynthia (1943)"
category: bearbeitung
created: 1943
---
Letters to Cynthia\
including an autobiographical sketch on his baptism

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Letters_to_Cynthia_final.pdf' | relative_url }})

[Das Orginal von „First Cycle of Letters to Cynthia (Harris)(1943)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/378.pdf)
