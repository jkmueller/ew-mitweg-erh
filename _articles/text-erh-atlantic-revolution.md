---
title: "Eugen Rosenstock-Huessy: The Atlantic Revolution (1940)"
category: bearbeitung
created: 1940
published: 2022-04-05
---
#### by the author of „Out of Revolution“

unpublished, geschrieben am 13. Juni 1940

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_The_Atlantic_Revolution.pdf' | relative_url }})

Die deutsche Übersetzung: [Die Atlantische Revolution]( {{ 'text-erh-atlantic-revolution-dt' | relative_url }})

#### The Atlantic Revolution enthält einige extremen Aussagen:
* “Obviously, the Atlantic Revolution has little to learn from Russia or from Central Europe.”
* “The Atlantic Revolution concerns the old powers of the Atlantic: Holland and Belgium, France and Spain, the British Empire and the U. S.”
* “But there is nothing inevitable in the stupidity of the French and English democracies between September 1929 and May 1940. It was not inevitable but just inexcusable.”
* “The Atlantic Revolution forces the U. S., to discount the Allies as sovereign powers.”

#### Hier einige Ereignisse zum historischer Kontext von The Atlantic Revolution:
* Deutschland\
  * [Erklärung der Reichsregierung zu militärischen Aktivitäten am 1.9.1939](https://www.1000dokumente.de/index.html?c=dokument_de&dokument=0209_pol&object=translation&st=&l=de)
    * deutsche Minderheiten in Danzig und dem Korridor werden in der qualvollsten Weise mißhandelt
    * friedliche Vermittlungsversuche beantworten die Polen durch Mobilmachungen und verstärktem Terror
    * die polnische Regierung befindet sich in Abhängigkeit von einer nunmehr entfesselten wilden Soldateska
    * die neutralen Staaten haben ihre Neutralität versichert und wir haben sie ihnen schon vorher garantiert
    * Rußland und Deutschland arbeiten zusammen und ein [Pakt](#hintergrunddokument-hitler-stalin-pakt-1939) wurde abgeschloßen, der Gewalt ausschließt
    * wer mit Gift kämpft, wird mit Giftgas bekämpft.

  * Polen
    * 1\. September 1939: Einmarsch Deutschlands
    * 6\. Oktober  1939: Sieg Deutschlands
  * Niederlande
    * 10\. Mai 1940: Schlacht um die Niederlande
    * 14\. Mai 1940: Kapitulation
    * 17\. Mai 1940: Aufgabe des Wiederstandes der Festung Holland
  * Belgien
    * 10\./11\. Mai 1940: Einnahme des belgischen Forts Eben-Emael
    * 28\. Mai 1940: Kapitulation
  * Frankreich
    * 3\. September 1939: Kriegserklärung an Deutschland, danach Sitzkrieg
    * 10\. Mai 1940: Beginn der Offensive
    * 25\. Juni 1940: Waffenstillstand
  * Dänemark
    * 9\. April 1940: Angriff und Kapitulation
  * Norwegen
    * 9\. April 1940: Angriff auf Narvik
    * 10\. Juni 1940: Kapitualtion
* Italien
  * 10\. Juni 1940: Kriegserklärung an Frankreich und Großbritannien
* Großbritannien
  * 3\. September 1939: Kriegserklärung an Deutschland, danach Sitzkrieg
* Rußland
  * Polen
    * 17\. September 1939: Einmarsch in (Ost)Polen
  * Finnland
    * 30\. November 1939: Beginn des Winterkrieges
    * Begründung: Gebietsforderungen in der Karelischen Landenge wegen unabdingbarer Sicherheitsinteressen für die Stadt Leningrad
    * 13\. März 1940 Friedensvertrag von Moskau

#### Hintergrunddokument: Carl Schmitt 1939

Carl Schmitt: ***Völkerrechtliche Großraumordnung mit Interventionsverbot für raumfremde Mächte***\
			*Ein Beitrag zum Reichsbegriff im Völkerrecht, 1939 (4: 1941)*

##### Inhaltsverzeichnis:
- Vorbemerkung
- Allgemeines
- I. Beispiele unechter oder überholter Raumprinzipien
- II. Die Monroedoktrin als der Präzedenzfall eines völkerrechtlichen Großraumprinzips
- III. Der Grundsatz der Sicherheit der Verkehrswege des britischen Weltreiches
- IV. Minderheiten- und Volksgruppenrecht im mittel- und osteuropäischen Großraum
- V. Der Reichsbegriff im Völkerrecht
- VI. Reich und Raum
- VII. Der Raumbegriff in der Rechtswissenschaft

##### Zitate:
- “Das Völkerrecht ist als jus gentium, als ein Recht der Völker, zunächst eine personal, d. h. von der Volks- und Staatsangehörigkeit her bestimmte, konkrete Ordnung. Das dem Volksbegriff zugeordnete völkerrechtliche Ordnungsprinzip ist das Selbstbestimmungsrecht der Völker. Es ist als Grundsatz heute anerkannt.
Jede Ordnung seßhafter, mit- und nebeneinander lebender, gegenseitig sich achtender Völker ist aber nicht nur personal bestimmt, sondern zugleich eine territorial konkrete Raumordnung.”
- “Dabei halte ich es für notwendig, über die abstrakten, im Allgemeinbegriff "Staat" liegenden Gebietsvorstellungen hinaus, den Begriff des konkreten Großraums und den ihm zugeordneten Begriff eines völkerrechtlichen Großraumprinzips in die Völkerrechtswissenschaft einzuführen.”
- “Großraum ist ein aus einer umfassenden gegenwärtigen Entwicklungstendenz entstehender Bereich menschlicher Planung, Organisation und Aktivität. Großraum ist für uns vor allem ein zusammenhängender Leistungsraum”
- “Die 1823 verkündete amerikanische "Monroe Doctrine" ist in der neueren Geschichte des Völkerrechts das erste und das bisher erfolgreichste Beispiel eines völkerrechtlichen Großraumprinzips.”
- “Die Monroedoktrin wurde um die Jahrhundertwende aus einer defensiven Abwehr der Interventionen raumfremder Mächte zu einem aggressiven, imperialistisch gedeuteten Ausdehnungsgrundsatz, um dann seit 1934 diesen imperialistischen Charakter wenigstens offiziell wieder einzuschränken. Sie wurde aus einem Grundsatz der Nichtintervention und der Ablehnung fremder Einmischungen zu einer Rechtfertigung imperialistischer Interventionen der Vereinigten Staaten in andere amerikanische Staaten.”
- “Für uns ist entscheidend, daß die ursprüngliche Monroelehre von 1823 die erste Erklärung in der Geschichte des modernen Völkerrechts ist, die von einem Großraum spricht und für ihn den Grundsatz der Nichtintervention raumfremder Mächte aufstellt. Sie bezieht sich ausdrücklich auf die "westliche Halbkugel" der Erde. Wenn Talleyrand oder Gentz oder die Regierungen der Heiligen Allianz von "Europa" sprechen, so meinen sie mehr ein staatliches Machtverhältnissystem. Die amerikanische Erklärung von 1823 aber denkt in einem modernen Sinne raumhaft planetarisch.”
- “Der liberale Freiheitsgedanke der westlichen Demokratie ist heute geschichtlich überholt. Er dient jetzt seinerseits dazu, einen bloßen status quo rechtlich zu sanktionieren und einem Weltbesitz die Heiligkeit des Rechts, die Weihe der Legalität und der Legitimität zu geben. Die westlichen Demokratien sind heute in der Lage der damaligen europäischen Mächte der Heiligen Allianz. Aus einem monarchistisch-dynastischen ist ein liberaldemokratisch-kapitalistisches Legitimitätsprinzip geworden. Schon der Weltkrieg 1914 bis 1918 war ein Interventionskrieg dieser liberaldemokratischen Legitimität. Damals konnte er sich allerdings noch als Krieg gegen reaktionäre, mit der monarchistischen Heiligen Allianz verwandte Mächte ausgeben, während die liberaldemokratische Heilige Allianz der westlichen Mächte heute offen auf der Seite der Vergangenheit und der Heiligkeit des status quo steht und neue politische Ideen sowohl wie neue, wachsende Völker zu unterdrücken sucht.”
- “Die Rechtfertigung eines kapitalistischen Imperialismus, zu der Präsident Theodore Roosevelt um die Wende des 19. zum 20. Jahrhundert die Monroedoktrin benutzte, ist ein besonderer Abschnitt in der Geschichte dieser Doktrin. Es ist mit Recht als ein Selbstwiderspruch und als das auffälligste Beispiel des Sinnwandels eines solchen Grundsatzes empfunden worden, daß ein ursprünglich defensiver, Interventionen raumfremder Mächte abwehrender Raumgedanke zur Grundlage einer "Dollar Diplomacy" gemacht werden konnte. In allen geschichtlichen Darstellungen der Monroedoktrin tritt diese imperialistisch-kapitalistische Umdeutung des ursprünglichen Sinnes als eine tiefe Sinnänderung hervor. Daneben aber haben wir eine andere, vielleicht noch tiefere und für unsere Betrachtung völkerrechtlicher Großraumprinzipien jedenfalls noch aufschlußreichere Art der Veränderung und des Sinnwandels zu beachten, nämlich die der Umdeutung der Monrodehre aus einem konkreten, geographisch und geschichtlich bestimmten Großraumgedanken in ein allgemeines, universalistisch gedachtes Weltprinzip, das für die ganze Erde gelten soll und "Ubiquität" beansprucht. Diese Umdeutung hängt allerdings mit der Fälschung in ein universalistisch-imperialistisches Expansionsprinzip eng zusammen. Sie ist für uns von besonderem Interesse, weil sie den Punkt sichtbar macht, an welchem die Politik der Vereinigten Staaten von Amerika ihr kontinentales Raumprinzip verläßt und sich mit dem Universalismus des britischen Weltreiches verbindet.”
- “Der Präsident W. Wilson hat in seiner Botschaft an den Kongreß vom 22. Januar 1917 vorgeschlagen, daß alle Völker der Welt die Lehre des Präsidenten Monroe als eine "Weltdoktrin" annehmen sollten, mit dem Sinn des freien Selbstbestimmungsrechts der Völker für große und kleine Völker in gleicher Weise. Man hat sogar den Artikel 10 der Genfer Völkerbundsatzung bereits als einen Ausdruck und Anwendungsfall dieser Weltmonroedoktrin ausgegeben. Das sind typische und kennzeichnende Sinnveränderungen. Ihre Methode besteht darin, einen konkreten, räumlich bestimmten Ordnungsgedanken in Universalistische "Welt"-Ideen aufzulösen und dadurch den gesunden Kern eines völkerrechtlichen Großraumprinzips der Nichtintervention in eine imperialistische, unter humanitären Vorwänden in alles sich einmischende, sozusagen pan-interventionistische Weltideologie zu verwandeln.”
- “Universalistische, weltumfassende Allgemeinbegriffe sind im Völkerrecht die typischen Waffen des Interventionismus. Auf ihre Verbindung und Verquickung mit konkreten, geschichtlichen und politischen Situationen und Interessen ist daher stets zu achten. Ein wichtiger Fall solcher Verquickung wird uns noch im Minderheitenrecht (unter IV) begegnen. Hier soll zunächst eine mit der Monroedoktrin oft in Parallele gesetzte "Doktrin" behandelt werden: die der "Sicherheit der Verkehrswege des britischen Weltreiches". Sie ist das Gegenbild dessen, was die ursprüngliche Monrodehre war.”
- “Freilich ist dieses System mit seinem Minderheitenschutz heute geschichtlich überholt. Aber die völkerrechtliche Denkweise, die sich in ihm dokumentiert, und eine Welt dazugehöriger völkerrechtlicher Prinzipien und Begriffsbildungen wirken immer noch weiter und sind keineswegs verschwunden. Sie werden von den Mächten der westlichen Demokratie weitergetragen und sind ein Teil der geistigen und moralischen Rüstung zu einem neuen, totalen Weltkrieg, zu einem "gerechten Kriege" großen Stils”
- “In der politischen und sozialen Wirklichkeit verbergen sich hinter dem inhaltlosen Wort "Minderheit" derartig offensichtlich verschiedene und widersprechende Sachverhalte - Grenzbereinigungsfragen, Fragen der kulturellen und völkischen Autonomie, das durchaus besonders geartete und mit keiner dieser anderen Fragen vergleichbare Judenproblem -, daß ich in diesem Zusammenhang nur daran zu erinnern brauche.”
- “Im Vordergrund steht der allgemeine liberal-individualistische Gedanke, daß dem einzelnen Individuum, das zufällig einer "Minderheit" angehört, Gleichheit und Gleichbehandlung gewährleistet wird. Liberaler Individualismus und übervölkischer Universalismus erweisen sich auch hier als die beiden Pole derselben Weltanschauung.”
- “Seit der Erklärung, die der Reichskanzler Adolf Hitler am 20. Februar 1938 im Deutschen Reichstag gegeben hat, besteht auf der Grundlage unseres nationalsozialistischen Volksgedankens ein deutsches Schutzrecht für die deutschen Volksgruppen fremder Staatsangehörigkeit. Damit ist ein echter völkerrechtlicher Grundsatz aufgestellt.”
- “Der deutsch-russische Grenz- und Freundschaftsvertrag vom 28. September 1939 (abgedruckt Zeitschrift für Völkerrecht, Band XXIV, S. 99) verwendet bereits im amtlichen Text den Begriff des Reiches. Er setzt die Grenze der "beiderseitigen Reichsinteressen" im Gebiet des bisherigen polnischen Staates fest. Ausdrücklich wird in Art. 2 des Vertrages jegliche Einmischung dritter Mächte in diese Abmachung abgelehnt und in der Einleitung als Zweck des Vertrages betont, daß den dort lebenden Völkerschaften ein ihrer völkischen Eigenart entsprechendes friedliches Dasein gesichert werden soll. Damit war das Versailler System des sogenannten Minderheitenschutzes für diesen Teil des europäischen Raumes erledigt. Aus den baltischen Ländern ist im Gesamtzusammenhang der politischen Neuordnung im Osten die deutsche Bevölkerung auf das Gebiet des Deutschen Reiches umgesiedelt worden (deutsch-estnisches Protokoll über die Umsiedlung der deutschen Volksgruppen vom 15. Oktober 1939 und deutsch-litauischer Vertrag vom 30. Oktober 1939). Dazu kommt die Rückwanderung der Deutschen aus Wolhynien und Bessarabien. Für den Donauraum hat der Wiener Schiedsspruch des deutschen und des italienischen Außenministers vom 30. August 1940 die neue Gebietsgrenze zwischen Ungarn und Rumänien unter dem Gesichtspunkt einer gerechten Volksordnung gezogen. Gleichzeitig sind zwischen der Reichsregierung und der ungarischen und der rumänischen Regierung Abmachungen zum Schutze der deutschen Volksgruppen in beiden Ländern getroffen worden, so daß auch hier das liberaldemokratische, individualistische Versailler Minderheitensystem überwunden und durch den Gedanken einer Volksgruppenordnung ersetzt ist. Für die Dobrudscha sieht der rumänisch-bulgarische Vertrag vom 7. September 1940 eine Pflichtumsiedlung der beiderseitigen Volksgruppen aus der Nord- und der Süd-dobrudscha vor. In allen diesen Fällen hat sich der Grundsatz der Nichteinmischung raumfremder Mächte als geltendes Prinzip des heutigen Völkerrechts auch hinsichtlich des Volksgruppenrechtes durchgesetzt.”
- “Eine Großraumordnung gehört zum Begriff des Reiches, der hier als eine spezifisch völkerrechtliche Größe in die völkerrechtswissenschaftliche Erörterung eingeführt werden soll. Reiche in diesem Sinne sind die führenden und tragenden Mächte, deren politische Idee in einen bestimmten Großraum ausstrahlt und die für diesen Großraum die Interventionen fremdräumiger Mächte grundsätzlich ausschließen. Der Großraum ist natürlich nicht identisch mit dem Reich in dem Sinne, daß das Reich der von ihm vor Interventionen bewahrte Großraum selber wäre; und nicht jeder Staat oder jedes Volk innerhalb des Großraumes ist selber ein Stück Reich, so wenig jemand bei der Anerkennung der Monroedoktrin daran denkt, Brasilien oder Argentimen zu einem Bestandteil der Vereinigten Staaten von Amerika zu erklären. Wohl aber hat jedes Reich einen Großraum, in den seine politische Idee ausstrahlt und der fremden Interventionen nicht ausgesetzt sein darf. Der Zusammenhang von Reich, Großraum und Nichtinterventionsprinzip ist grundlegend.”
- “Zu einer neuen Ordnung der Erde und damit zu der Fähigkeit, heute Völkerrechtssubjekt ersten Ranges zu sein, gehört ein gewaltiges Maß nicht nur "natürlicher", im Sinne naturhaft ohne weiteres gegebener Eigenschaften, dazu gehört auch bewußte Disziplin, gesteigerte Organisation und die Fähigkeit, den nur mit einem großen Aufgebot menschlicher Verstandeskraft zu bewältigenden Ap- parat eines modernen Gemeinwesens aus eigener Kraft zu schaffen und ihn sicher in der Hand zu haben.”
- “Das eigentümliche Mißverhältnis des jüdischen Volkes zu allem, was Boden, Land und Gebiet angeht, ist in seiner Art politischer Existenz begründet. Die Beziehung eines Volkes zu einem durch eigene Siedlungs- und Kulturarbeit gestalteten Boden und zu den daraus sich ergebenden konkreten Machtformen ist dem Geist des Juden unverständlich. ”



#### Hintergrunddokument: [Hitler-Stalin-Pakt 1939](https://www.1000dokumente.de/index.html?c=dokument_de&dokument=0025_pak&l=de)

##### Nichtangriffsvertrag zwischen Deutschland und der Union der Sozialistischen Sowjetrepubliken
Die Deutsche Reichsregierung und\
die Regierung der Union der Sozialistischen Sowjetrepubliken\
geleitet von dem Wunsche die Sache des Friedens zwischen Deutschland und der UdSSR zu festigen und ausgehend von den grundlegenden Bestimmungen des Neutralitätsvertrages, der im April 1926 zwischen Deutschland und der UdSSR geschlossen wurde, sind zu nachstehender Vereinbarung gelangt:
- Artikel I.\
  Die beiden Vertragschliessenden Teile verpflichten sich, sich jeden Gewaltakts, jeder aggressiven Handlung und jedes Angriffs gegen einander, und zwar sowohl einzeln als auch gemeinsam mit anderen Mächten, zu enthalten.
- Artikel II.\
  Falls einer der Vertragschliessenden Teile Gegenstand kriegerischer Handlungen seitens einer dritten Macht werden sollte, wird der andere Vertragschliessende Teil in keiner Form diese dritte Macht unterstützen.
- Artikel III.\
  Die Regierungen der beiden Vertragschliessenden Teile werden künftig fortlaufend zwecks Konsultation in Fühlung zueinander bleiben, um sich gegenseitig über Fragen zu informieren, die ihre gemeinsamen Interessen berühren.
- Artikel IV.\
  Keiner der beiden Vertragschliessenden Teile wird sich an irgend einer Mächtegruppierung beteiligen, die sich mittelbar oder unmittelbar gegen den anderen Teil richtet.
- Artikel V.\
  Falls Streitigkeiten oder Konflikte zwischen den Vertragschliessenden Teilen über Fragen dieser oder jener Art entstehen sollten, werden beide Teile diese Streitigkeiten oder Konflikte ausschliesslich auf dem Wege freundschaftlichen Meinungsaustausches oder nötigenfalls durch Einsetzung von Schlichtungskommissionen bereinigen.
- Artikel VI.\
  Der gegenwärtige Vertrag wird auf die Dauer von 10 Jahren abgeschlossen mit der Massgabe, dass, soweit nicht einer der Vertragschliessenden Teile ihn ein Jahr vor Ablauf dieser Frist kündigt, die Dauer der Wirksamkeit dieses Vertrages automatisch für weitere fünf Jahre als verlängert gilt.
- Artikel VII.\
  Der gegenwärtige Vertrag soll innerhalb möglichst kurzer Frist ratifiziert werden. Die Ratifikationsurkunden sollen in Berlin ausgetauscht werden. Der Vertrag tritt sofort mit seiner Unterzeichnung in Kraft.

Ausgefertigt in doppelter Urschrift, in deutscher und russischer Sprache,\
Moskau am 23. August 1939.\
Für die deutsche Reichsregierung: *I. Ribbentrop*\
In Vollmacht der Regierung der UdSSR: *W. Molotow*

Hier nach: *Politisches Archiv des Auswärtigen Amtes, f 11/0048-0050. Mikrofilm.*

##### Geheimes Zusatzprotokoll
Aus Anlass der Unterzeichnung des Nichtangriffsvertrages zwischen dem Deutschen Reich und der Union der Sozialistischen Sowjetrepubliken haben die unterzeichneten Bevollmächtigten der beiden Teile in streng vertraulicher Aussprache die Frage der Abgrenzung der beiderseitigen Interessensphären in Osteuropa erörtert. Diese Aussprache hat zu folgendem Ergebnis geführt:
1. Für den Fall einer territorial-politischen Umgestaltung in den zu den baltischen Staaten (Finnland, Estland, Lettland, Litauen) gehörenden Gebieten bildet die nördliche Grenze Litauens zugleich die Grenze der Interessenssphären Deutschlands und der UdSSR. Hierbei wird das Interesse Litauens am Wilnaer Gebiet beiderseits anerkannt.
2. Für den Fall einer territorial-politischen Umgestaltung der zum polnischen Staate gehörenden Gebiete werden die Interessenssphären Deutschlands und der UdSSR ungefähr durch die Linie der Flüsse Narew, Weichsel und San abgegrenzt.Die Frage, ob die beiderseitigen Interessen die Erhaltung eines unabhängigen polnischen Staates erwünscht erscheinen lassen und wie dieser Staat abzugrenzen wäre, kann endgültig erst im Laufe der weiteren politischen Entwicklung geklärt werden.In jedem Falle werden beide Regierung diese Frage im Wege einer freundschaftlichen Verständigung lösen.
3. Hinsichtlich des Südostens Europas wird von sowjetischer Seite das Interesse an Bessarabien betont. Von deutscher Seite wird das völlig politische Desinteresse an diesen Gebieten erklärt.
4. Dieses Protokoll wird von beiden Seiten streng geheim behandelt werden.

Moskau, den 23. August 1939.\
Für die deutsche Reichsregierung: *I. Ribbentrop*\
In Vollmacht der Regierung der UdSSR: *W. Molotow*

Hier nach: *Politisches Archiv des Auswärtigen Amtes, f 19/182-183. Mikrofilm.*

[Das Orginal von „THE ATLANTIC REVOLUTION (1940)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/344.pdf)
