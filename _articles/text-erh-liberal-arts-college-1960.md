---
title: "Eugen Rosenstock-Huessy: Liberal Arts College (1960)"
category: lectures
created: 1960
published: 2023-09-22
---
### NOTE OF THE EDITOR

This is the edited transcription of one lecture March, 1960. Harold Stahmer, a student of Rosenstock-Huessy ́s 1951, is the man who loosens his tongue.

1

The transcription was made by Frances Huessy – my work are the following changes:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.

2

This one lecture – and Rosenstock-Huessy didn ́t like to speak once, because the listener should express what they are willing and able to understand – has the deepest dimensions of a whole lifetime: Judaism and Christianity being the themes of Rosenstock-Huessy ́s life. And now he is alone, left a widower after Margrit ́s death.
The protest against listening as watching and not as obedience to the living word expresses the mourning in a heart-rending way. The three stories:

- *Rosenstock-Huessy and his friends after 1918 I, 4*
- *Rosenstock-Huessy clearing the court I, 1*
- *Rosenstock-Huessy ́s origin being Jewish I, 4*

mark the turning points of his life: birth, conversion and death.

This was March 1960 (just a week before Margrits birthday on the tenth of March – and she would have been 67 years old) – in September Freya von Moltke came to Four Wells (with her second son Konrad von Moltke) which may have meant that his mourning about Margrit ́s death was softened. In April he made his journey to Europe.

If I had to give a title to this piece of spoken language in musical terms, I would say:
Maestoso espressivo.

*Cologne, May 3, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH Liberal Arts College (1960)]( {{ 'assets/downloads/wilkens_erh_Liberal_Arts_College_1960.pdf' | relative_url }})

- [The transcript of „Liberal Arts College (1960)” as PDF](https://www.erhfund.org/wp-content/uploads/647.pdf)
- [The audio recordings of „ERH Liberal Arts College (1960)”](https://www.erhfund.org/lecturealbum/volume-22-liberal-arts-college-1960/)
