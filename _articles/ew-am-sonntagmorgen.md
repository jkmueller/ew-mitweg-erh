---
title: Am Sonntagmorgen
category: vertiefung
created: 2017-12
---


Bei dieser Tagung in Imshausen haben wir auf die gemeinschaftsbildende Kraft des gemeinsamen Zuhörens auf den Text verzichtet – dafür traten die Stimmen der einzelnen Vertreter der Tagungsteile hervor.

Am Sonntagmorgen versammelten wir uns, um über den Neuen Sonntag zu sprechen. Da trat in den Vordergrund der Satz, daß unsre Religion in dem besteht, was für uns im Kalender unabdingbar, wichtig oder nebensächlich ist. Nicht also die Formen, die religiöses Leben geschaffen hat: zur Kirche gehen, Hausandacht, Beichte, Buße und Reue, sondern schärfer gesehen, was nun wirklich wichtig ist: Zukunft, Besitz, Pflicht, Vermögen. Und wie wir uns leicht darüber hinwegtäuschen, daß das – könnte man es in Worte fassen – unsre Religion ist.

Das durch Weihnachten, Ostern, Pfingsten, Geburtstage, Namenstage, weltliche Feste (1. Mai, Helmut-Kohl-Tag), durch Urlaub, Geburt, Konfirmation, Hochzeit, Begräbnis, Höherstufung, Rente, Pension gegliederte Jahr gibt Auskunft, was in diesem Sinne unabdingbar, wichtig, nebensächlich ist.

Der Neue Sonntag nun soll über die Ebene des Jahres, der untersten biographischen Einheit, hinausheben und das Leben der Seele im Zwölfton des Geistes erfahrbar werden lassen. Atem des Geistes könnte uns dazu verhelfen, die siebzig, achtzig, neunzig Jahre unsres Lebens (Victor von Weizsäcker war der Meinung, es sei jedes Leben gleich lang, so daß also nachträglich der Zwölfton des Geistes auf die wirklich gelebte Lebenszeit zu verteilen wäre …) als sinnvolle, atmende Gliederung zu erfahren:

der Neue Sonntag könnte werden die Erneuerung des Protests Israels gegen die Acht, Zehn und Zwölf der Reiche (das chinesische I Ging hat acht Zeichen, das ägyptische Jahr war in zehnmal dreißig Tage eingeteilt, mit dem bekannten Überfluß in den Schalttagen, das Einfügen von Stier/Skorpion und Krebs/Steinbock in die Sternzeichen vervollständigte auf Zwölf),

wenn jeder Arbeitnehmer sechs Jahre arbeitete, dann ein ganzes Jahr freigestellt würde, um die seelische Erneuerung in der Erfahrung neuer Gemeinschaft erfahren zu können (was etwas anderes als Urlaub ist!).

Das bewußte Leben von 14 bis 84 würde sich dann in zehn solche „Jahreswochen” gliedern, faßbar, erinnerbar, instandsetzend für die die Zeit überwindenden Töne des Prophezeiens und Stiftens.

Wie könnte man dafür sorgen, daß also

von 14-21 sich der Name erneuert,\
von 21-28 das Wahrnehmen der Welt deutlich wirklich wird,\
von 28-35 der Dienst am Nächsten in den Vordergrund tritt,\
von 35-42 ein Werk geschaffen wird, das biographisch „stehenbleibt”,\
von 42-49 eine Umwälzung geschieht, bei der der Generationenbruch zur Rede steht,\
von 49-56 man sich kein X mehr für ein U vormachen läßt,\
von 56-63 mit dem Lebenswort vor Kinder und Enkel tritt,\
von 63-70 das Warten auf die Ernte des Lebens lernt,\
von 70-77 eine für die Mitmenschen lebenswichtige Verantwortung übernimmt,\
von 77-84 lehren darf, was das Leben gelehrt hat –

wobei diese Zeiten im konkreten einzelnen Leben variieren mögen, mal kürzer, mal länger. Und die Verschiedenheit der Lebensläufe von Mann und Frau sorgen immer für bunte Fülle von Verwirklichung.

Diese Schau klang nur behende und leicht gestreift an, darf aber doch hier zu denken geben.

aus: [ERHG Mitgliederbrief 2017-12](https://www.rosenstock-huessy.com/erhg-2017-12-mitgliederbrief/)
