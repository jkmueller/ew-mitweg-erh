---
title: Overview of the Lecture-Editions
category: lectures
created: 1000
---
Eckart Wilkens hat im Laufe der letzten Jahre viele der aufgenommenen und transkribierten Vorlesungen Rosenstock-Huessys bearbeitet.
In der Übersicht sind es die fett gedruckten Elemente.

*[Eckart Wilkens edited over the last few years many of the Rosenstock-Huessy's recorded and transcribed lectures. In the overview they are the ones in bold face.]*

***Rosenstock-Huessy 's [Lecture Courses (1949-1968)](https://www.erhfund.org/eugen-rosenstock-huessy-live/)***

| 1949 | VOLUME 1:  | [CIRCULATION OF THOUGHT (1949)](https://www.erhfund.org/lecturealbum/volume-01-circulation-of-thought-1949/)         | **([6 LECTURES]({{ 'text-erh-circulation-of-thoughts-1949' | relative_url }}))**  |
|      | VOLUME 2:  | UNIVERSAL HISTORY (1949)              | (6 LECTURES)      |
| 1951 | VOLUME 3:  | UNIVERSAL HISTORY (1951)              | (1 LECTURE)       |
| 1952 | VOLUME 4:  | POTENTIAL TEACHERS (1952)             | (2 LECTURES)      |
| 1953 | VOLUME 5:  | CROSS OF REALITY (1953)               | (24 LECTURES)     |
|      | VOLUME 6:  | [HINGE OF GENERATIONS (1953)](https://www.erhfund.org/lecturealbum/volume-06-hinge-of-generations-1953/)           | **([14 LECTURES]({{ 'text-erh-hinge-of-generations-1953' | relative_url }}))** |
|      | VOLUME 7:  | MAKE BOLD TO BE ASHAMED (1953)        | (6 LECTURES)      |
| 1954 | VOLUME 8:  | [COMPARATIVE RELIGION (1954)](https://www.erhfund.org/lecturealbum/volume-08-comparative-religion-1954/)           | **(28 LECTURES)** |
|      | VOLUME 9:  | [CIRCULATION OF THOUGHT (1954)](https://www.erhfund.org/lecturealbum/volume-09-circulation-of-thought-1954/)         | **(26 LECTURES)** |
|      | VOLUME 10: | [FOUR DYSANGELISTS (1954)](https://www.erhfund.org/lecturealbum/volume-10-four-dysangelists-1954/)              | **([2 LECTURES]({{ 'text-erh-before-and-after-marx-1954' | relative_url }}))**  |
|      | VOLUME 11: | [HISTORY MUST BE TOLD (DRAFT, 1954)](https://www.erhfund.org/lecturealbum/volume-11-history-must-be-told-draft-1954/)    | **([1 LECTURE]({{ 'text-erh-history-must-be-told-1954' | relative_url }})))**   |
|      | VOLUME 12: | [UNIVERSAL HISTORY (1954)](https://www.erhfund.org/lecturealbum/volume-12-universal-history-1954/)              | **([25 LECTURES]({{ 'text-erh-universal-history-1954' | relative_url }}))** |
| 1955 | VOLUME 13: | HISTORY MUST BE TOLD (1955)           | (1 LECTURE)       |
|      | VOLUME 14: | UNIVERSAL HISTORY (1955)              | (4 LECTURES)      |
| 1956 | VOLUME 15: | CIRCULATION OF THOUGHT (1956)         | (1 LECTURE)       |
|      | VOLUME 16: | [GREEK PHILOSOPHY (1956)](https://www.erhfund.org/lecturealbum/volume-16-greek-philosophy-1956/)               | **([26 LECTURES]({{ 'text-erh-greek-philosophy-1956' | relative_url }}))** |
|      | VOLUME 17: | UNIVERSAL HISTORY (1956)              | (9 LECTURES)      |
| 1957 | VOLUME 18: | UNIVERSAL HISTORY (1957)              | (29 LECTURES)     |
| 1959 | VOLUME 19: | [AMERICAN SOCIAL HISTORY (1959)](https://www.erhfund.org/lecturealbum/volume-19-american-social-history-1959/)        | **(42 LECTURES)** |
|      | VOLUME 20: | [HISTORIOGRAPHY (1959)](https://www.erhfund.org/lecturealbum/volume-20-historiography-1959/)                 | **([13 LECTURES]({{'text-erh-historiography-1959' | relative_url }}))** |
|      | VOLUME 21: | [MAN MUST TEACH (1959)](https://www.erhfund.org/lecturealbum/volume-21-man-must-teach-1959/)                 | **([1 LECTURE]({{'text-erh-man-must-teach-1959' | relative_url }}))**   |
| 1960 | VOLUME 22: | [LIBERAL ARTS COLLEGE (1960)](https://www.erhfund.org/lecturealbum/volume-22-liberal-arts-college-1960/)           | **([1 LECTURE]({{'text-erh-liberal-arts-college-1960' | relative_url }}))**   |
|      | VOLUME 23: | WHAT FUTURE THE PROFESSIONS (1960)    | (4 LECTURES)      |
| 1962 | VOLUME 24: | GRAMMATICAL METHOD (1962)             | (3 LECTURES)      |
|      | VOLUME 25: | [ST. AUGUSTINE BY THE SEA (1962)](https://www.erhfund.org/lecturealbum/volume-25-st-augustine-by-the-sea-1962/)       | **([6 LECTURES]({{'text-erh-bionomics-of-language-1960' | relative_url }}))**  |
| 1965 | VOLUME 26: | [ECONOMY OF TIMES (1965)](https://www.erhfund.org/lecturealbum/volume-26-economy-of-times-1965/)               | **([5 LECTURES]({{'text-erh-economy-of-times-1965' | relative_url }}))**  |
|      | VOLUME 27: | [TALK WITH FRANCISCANS (1965)](https://www.erhfund.org/lecturealbum/volume-27-talk-with-franciscans-1965/)          | **([2 LECTURES]({{'text-erh-talk-with-franciscans-1965' | relative_url }}))**  |
|      | VOLUME 28: | [CROSS OF REALITY (1965)](https://www.erhfund.org/lecturealbum/volume-28-cross-of-reality-1965/)               | **([1 LECTURE]({{'text-erh-the-cross-of-reality-1965' | relative_url }}))**   |
| 1966 | VOLUME 29: | [LINGO OF LINGUISTICS (1966)](https://www.erhfund.org/lecturealbum/volume-29-lingo-of-linguistics-1966/)           | **([3 LECTURES]({{'text-erh-the-cross-of-reality-1965' | relative_url }}))**  |
|      | VOLUME 30: | [PEACE CORPS (1966)](https://www.erhfund.org/lecturealbum/volume-30-peace-corps-1966/)                    | **([3 LECTURES]({{'text-erh-peace-corps-1966' | relative_url }}))**  |
| 1967 | VOLUME 31: | [CRUCIFORM CHARACTER OF HISTORY (1967)](https://www.erhfund.org/lecturealbum/volume-31-cruciform-character-of-history-1967/) | **([5 LECTURES]({{'text-erh-the-cruciform-characterof-history-1967' | relative_url }}))**  |
|      | VOLUME 32: | [UNIVERSAL HISTORY (1967)](https://www.erhfund.org/lecturealbum/volume-32-universal-history-1967/)              | **([20 LECTURES]({{'text-erh-universal-history-1967' | relative_url }}))** |
| 1968 | VOLUME 33: | [FASHIONS OF ATHEISM (1968)](https://www.erhfund.org/lecturealbum/volume-33-fashions-of-atheism-1968/)            | **([1 LECTURE]({{'text-erh-fashions-of-atheism-1968' | relative_url }}))**   |
|      | VOLUME 34: | [THE UNIVERSITY (1968)](https://www.erhfund.org/lecturealbum/volume-34-the-university-1968/)                 | **([1 LECTURE]({{'text-erh-the-university-1968' | relative_url }}))**   |

*available at [www.erhfund.org](https://www.erhfund.org/)*\
**bold face: edited version by Eckart Wilkens**\
Die Bearbeitungen machen die Texte Rosenstock-Huessys leichter zugänglich indem sie Wiederholungen sichtbar machen und den vielen externen Beispielen einen Platz geben. Die Systematik des Textes wird so auch in der räumlicher Gestalt deutlich. Wir bedanken uns dafür bei Eckart recht herzlich.

*[The editions make the texts of Rosenstock-Huessy easier accessible by showing repetitions and the place of examples therein. The systematic of the text becomes clear in the spacial ordering. We thank Eckart for that.]*
>*Jürgen Müller*

aus: [ERHG Mitgliederbrief 2019-09](https://www.rosenstock-huessy.com/erhg-2019-09-mitgliederbrief/)
