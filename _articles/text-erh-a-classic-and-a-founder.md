---
title: "Eugen Rosenstock-Huessy: A Classic and a Founder (1937)"
category: bearbeitung
created: 1937
published: 2024-02-18
---
Eugen Rosenstock-Huessy

### A Classic and a Founder

##### I. The Scientific Grammar of Michael Faraday (1791-1867)
1. The Grammar of his Diary
2. The Three Dimensions of Time

##### II. The Tripartition in the Life of Theophrastus Paracelsus (1493-1541)
1. Humanism versus Natural Science
2. Antecedents
3. Theophrastus becomes Paracelsus
4. After-Life
5. a. 1526 and 1540: Two Portraits
b. The Law of Twofold Beginning
6. The Tripartition of the Good Life
7. Scientific Bad Humor (Bibliographic confessions)

##### III. The Common Denominator for Classic and Founder
1. External Diversity
2. Internal Identity

### NOTE BY THE EDITOR

This is the transcription of the pamphlet from 1937, a copy heavily used by a student.
I articulated the text by chapters with 16 parts each (four times four I-IV) to give the reader occasion to feel and to perform the changes of aspect so characteristic of Rosenstock-Huessy ́s oral style of presentation.

The pamphlet is standing on fire! In the third part Rosenstock-Huessy commenting about the founder Paracelsus stands up for his own mission to found the third form of Western University.

And the poignancy of arguments! He integrates as it were the four parts of Goethe ́s Farbenlehre (Theory of colors): didactic part, historical part, polemical part and the poetical visions of the whole. The chapter II, 7 being the polemical part – is anybody prepared to take this phalanx of arguments if only to be ashamed?

The study of Michael Faradays prose is really a jubilee about the qualities of the English language – a sort of homecoming to America for the writer.

During the Robert Cabot Lectures this pamphlet is given as reading matter – one may add this wonderful piece of brilliant scholarship to these.

*Cologne, 12 January, 2017 \
Eckart Wilkens*

[Eckart's Edition]( {{ 'assets/downloads/wilkens_erh_A_Classic_and_a_Founder.pdf' | relative_url }})

Die deutsche Übersetzung: [Ein Klassiker, ein Stifter]( {{ 'text-erh-a-classic-and-a-founder-dt' | relative_url }})



[The original of „A Classic and a Founder (1937)” as PDF-Scan](https://www.erhfund.org/wp-content/uploads/312.pdf)
