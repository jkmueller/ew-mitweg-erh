---
title: Wer ist Eugen Rosenstock-Huessy
category: hinfuehrung
order: 1
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/erh_jung.jpg' | relative_url }}){:.img-right.img-small}
Geboren am 6. Juli 1888 in Berlin, gestorben am 24. Februar 1973 in Four Wells, Norwich, Vt. USA

Eugen Rosenstock-Huessy hätte wohl von sich sagen können, daß er viele Leben hatte:

* als Kind emanzipierter Eltern jüdischer Herkunft, aufgewachsen mit sechs Schwestern,
* als Student der Rechte, als der jüngste Privatdozent 1914,
* als Kriegsteilnehmer (Offizier vor Verdun),
* als erster Herausgeber der Daimler-Werkzeitung,
* als erster Leiter der Akademie der Arbeit,
* als Professor für Recht in Breslau,
* als Anreger und Inspirator der freiwilligen Arbeitslager für Arbeiter, Bauern und Studenten in Schlesien,
* als Historiker, Theologe, Soziologe vor der Hitler-Zeit,
* als Emigrant 1933,
* als Immigrant in den Vereinigten Staaten,
* als Hochschullehrer in Harvard und am Dartmouth College, Hanover, New Hampshire,
* als Initiator des Camp William James,
* als Stifter der neuen Richtung der Erwachsenenbildung nach dem Ersten Weltkrieg und in demselben Sinne auch nach dem Zweiten Weltkrieg in Deutschland,
* als Autor einer Soziologie, die systematisch Gegenwart und Universalgeschichte umfaßt.

![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_5.jpg' | relative_url }}){:.img-left.img-small}

Eugen Rosenstock-Huessy zeichnete ein feines Gespür für die geschichtlichen Ereignisse aus, die er als Mitlebender wach und mit überraschender Schnelligkeit in größere Zusammenhänge stellte.
Er verstand sich als einen derer, die nach der grundlegenden Erfahrung des Zusammenbruchs nach dem Ersten Weltkrieg neue Wege suchten, um einer sozialen Ordnung nach den Weltkriegen den Weg zu weisen. Zu dieser Schar gehörten namentlich die Autoren der Zeitschrift "Die Kreatur", die von 1926 bis 1930 erschien.[^1] Die Begegnung mit [Franz Rosenzweig](http://www.ersterweltkrieg.eu/rosenzweig/rosenzweigichbleibealsojude.html) erhellte ihm, dass es trotz des Christentums auch das Judentum weiterhin geben müsse.

Die von Eugen Rosenstock-Huessy angeregten freiwilligen Arbeitslager für Arbeiter, Bauern und Studenten in Schlesien legten den Grundstein für die Widerstandsbewegung um Helmut James Graf von Moltke. Walter Hammer (1888-1966) nannte ihn darum den Erzvater des [Kreisauer Kreises](https://de.wikipedia.org/wiki/Kreisauer_Kreis).

Sofort nach Hitlers Usurpation am 31. Januar 1933 erkannte er, wie es um die Menschen jüdischer Herkunft in Deutschland stand, und verließ das Land bereits im gleichen Jahr. Als Hochschullehrer in den Vereinigten Staaten wirkte er prägend auf viele Menschen.
In Deutschland, wo er 1950 erstmals wieder seine Stimme in der Göttinger Universität erheben konnte, kam er dann fast nur denen wieder nahe, die sich unmittelbar von ihm inspirieren ließen.[^2] Seine treuesten europäischen Hörer nach 1945 sind wohl in den Niederlanden zu finden.

![Eugen Rosenstock-Huessy]({{ 'assets/images/110kk.jpg' | relative_url }}){:.img-right.img-small}

[^1]: Walter Benjamin, Nikolaj Berdjajew, Hugo Bergmann, Martin Buber, Edgar Dacqué, Hans Ehrenberg, Rudolf Ehrenberg, Marie Luise Enckendorff, M Gerschenson und W. Iwanow, Eberhard Grisebach, Willy Haas, Hermann Herrigel, Edith Klatt, Fritz Klatt, Georg Koch, Ernst Loewenthal, Ernst Michel, Wilhelm Michel, Albert Mirgeler, Karl Nötzel, Alfons Paquet, Werner Picht, Florens Christian Rang, Eugen Rosenstock-Huessy, Franz Rosenzweig, Heinrich Sachs, Leo Schestow, Justus Schwarz, Ernst Simon, Dolf Sternberger, Eduard Strauss, Ludwig Strauss, Hans Trüb, Viktor von Weizsäcker, Joseph Wittig.

[^2]: Sein Argument, dass Deutschlands geistige und seelische Existenz daran hinge, wie man die Märtyrer unter Hitler würdige, dürfte seiner Wirkung in der Adenauer- und Ulbricht-Ära stark im Wege gestanden haben, wo solche Gedankengänge aus verschiedenen Gründen sehr unwillkommen und unpopulär waren.
