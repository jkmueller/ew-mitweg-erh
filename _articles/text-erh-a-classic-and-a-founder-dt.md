---
title: "Eugen Rosenstock-Huessy: Ein Klassiker, ein Stifter (1937)"
category: bearbeitung
created: 1937
published: 2024-02-18
---
Eugen Rosenstock-Huessy

### Ein Klassiker, ein Stifter

##### I. Die Wissenschaftsgrammatik des Michael Faraday (1791-1867)
1. Die Grammatik seines Tagebuchs
2. Die drei Dimensionen der Zeit

##### II. Die Dreiteilung im Leben des Theophrastus Paracelsus (1493-1541)
1. Humanismus versus Naturwissenschaften
2. Vorgänger
3. Theophrastus wird Paracelsus
4. Nachleben
5. a 1526 und 1540: Zwei Porträts
b. Das Gesetz des doppelten Anfangs
6. Die Dreiteilung des Guten Lebens
7. Verstimmte Wissenschaft (bibliographische Bekenntnisse)

##### III. Der gemeinsame Nenner für Klassiker und Stifter
1. Äußere Verschiedenheit
2. Innere Gleichheit

### NOTIZ DES ÜBERSETZERS

In Lise van der Molens Bibliographie der Schriften Eugen Rosenstock-Huessys stehen für die amerikanischen Jahre von 1933 bis 1950 folgende Buchpublikationen:

- The Multiformity of Man, 1936 und 1949, 39 bzw. 71 Seiten
- A Classic and a Founder, 1937 und 1943, 73 Seiten
- Out of Revolution, 1938, 1939, 1969, 795 Seiten
- The Artist and his Community, 1940, 16 Seiten
- The Christian Future or The Modern Mind Outrun, 1946, 1947, 248 Seiten
- Planetary Man, In Memoriam Oswald Spengler, 1946, 14 Seiten
- The Driving Power of Western Civilization, 1950, 126 Seiten (aus: Out of Revolution)

*A Classic and a Founder* ist das Ankunftsbuch im Dartmouth College, sieben Studenten werden als Helfer genannt, namentlich Mr. Symmons. Die Schrift liegt mir in der gehefteten Form vor, wie sie im Dartmouth College produziert wurde, mit Schreibmaschine geschrieben, offenbar das Exemplar eines Studenten, der heftig Notizen gemacht hat.

Ich habe den Text getreulich übersetzt und dabei, über die von Rosenstock-Huessy getroffene Einteilung in Kapitel noch weiter unterteilt (die Kapitelüberschriften sind von mir), damit der Leser im Lesen nachvollziehen kann, wie sich die Sprache wendet – daß nämlich der Weg von

*Präjekt zu Subjekt zu Trajekt zu Objekt*

auch das Verfahren ist, mit dem Rosenstock-Huessy seine Darstellungen entfaltet.
Das Namensregister nennt alle im Text (also nicht in den Anmerkungen) genannten Namen.

*Köln, 21. Mai 2013 \
Eckart Wilkens*

[Eckarts Übersetzung]( {{ 'assets/downloads/wilkens_erh_Ein_Klassiker_ein_Stifter_v2.pdf' | relative_url }})

Das amerikanische Orginal: [A Classic and a Founder]( {{ 'text-erh-a-classic-and-a-founder' | relative_url }})



[Das Orginal von „A Classic and a Founder (1937)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/312.pdf)
