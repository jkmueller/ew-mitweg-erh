---
title: "Eugen Rosenstock-Huessy: Universal History (1954)"
category: lectures
created: 1954
published: 2023-04-27
---

### NOTE OF THE EDITOR
### 1.

Listening and reading constitute a different behavior to times and spaces:
listening you have to touch the frailty, the liveness of the presence of mind, you discover what is singular, just this moment in yours and the speaker ́s life, you are asked to follow the word as coming from an authority now, to translate it into your own life, forming the meaning by memory – it is, to speak with these lectures, the possibility to know the heartbending story of tribalism,
reading you enter the eternity of all the scriptures through all the ages, you may come back, you may come forward, you may reread what you have forgotten, the book guarantees you a special form of eternal life, unbound to the moment of the living encounter.
How to translate the modus of listening to that of reading?

### 2.

The transcriptions of Rosenstock-Huessy ́s lectures, as delivered by Frances Huessy, are helpful to the listener to the recordings, where a shimmer of eternity – you may repeat the listening as you want – steals something of the original presence of meind away, in so far the modulations of the speaker who listens himself to the listeners with their at least corporal presence, may be vague through technical disturbance.
But to read these transcripitons without listening to the recordings asks from the reader a form of making real the original situation with the same keen mindfulness which is asked then – and this is only partially possible: reading a text broadens the time, makes space bigger, you think to own, to possess, to occupy the text before you, you are prone to speed up.

Therefore the handling of a transcription is liable to the law of technical progress: you speed up time, you broaden space, you miss the social contact.

### 3.

My editing tries to come up with this change. I try to listen to the text, that means that I follow the changes of tone, of situation (chalk for the blackboard, questions), of colloquial scenes – and then to figure these changes in the written text.

The cadence of Rosenstock-Huessy speeking is always, that he\\
*proposes a new theme, aspect, remembrance or scientific mode,*
*tries to make this new tone insistent to the listener, calling for emphatic realizing what the word means in your own life,*\\
*gives a remembrance of what other people or the speaker himself have gathered to the theme, aspect, scientivic mode,*\\
*concludes the sense of this passage in formulating the essence.*

### 4.

This process on more than one level is here annotated:

chapters include four times four passages of this kind,
each lecture consists of twice four chapters (sometimes only three chapters in the second part, sometimes an epilogue as third part),

The chapters being divided in four parts I-IV, each of these divided into four parts 1- 4. Each part made clear by paragraphes whenever the tone is modulated by a change of subject.

### 5.

Similarly two modes of appeal to the listener are marked: whenever Rosenstock- Huessy tells a story I gave a headline and it is marked by coloring (modus 3 remembrance), sentences which sum up a whole context are printed in bold print (modus 4 supplying memory). The reader – as if he is a musician who has to realize a text given by a musical score – is asked to realize these changes of tone and insistance.

### 6.

The four modes of speech are collected in the appendix:
the index of contents opens the possibility to share the plan of the whole as one presence of mind (intended to create a living time over a few months),

the index of names – which, by the way, come off here as if they are old acquaintances – introduces the reader into the social and historical context,

the index of stories – sometimes exhilarating enough – corroborates the credibility of the speaker and the validity of what is said – you are asked to remember similar stories in your own life,

the index of sentences “take it down” may facilitate the long process of minding and realizing of what had come to you very shortly.

### 7.

Eugen Rosenstock-Huessy held these lectures in his sixty-sixth year of his life – you get a sum of his whole active life, just at the fringe of saying goodbye to the need of speaking publicly and within the frame of college life. Therefore these lectures – as I found out when a worked on them, really astonished how it could be, that I am dumbfounded again, although or even because I read Rosenstock-Huessy since more than fifty-five years! – are an especially precious part of his Life, Teaching and Acting.

*Cologne, January 21st 2019 (that means sixty-five years after these lectures were delivered)*\\
*Eckart Wilkens*

[Eckart's Edition]( {{ 'assets/downloads/wilkens_erh_Universal_History_1954.pdf' | relative_url }})

[The transcript of „Universal History 1954” as PDF](https://www.erhfund.org/wp-content/uploads/637.pdf)\
[The audio recordings of „Universal History 1954”](https://www.erhfund.org/lecturealbum/volume-12-universal-history-1954/)

A [note on Eckart's Edition]( {{ 'text-erh-prophecy-american-dictator' | relative_url }})
