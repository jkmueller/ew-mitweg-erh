---
title: "Eugen Rosenstock-Huessy: Die atlantische Revolution (1940)"
category:
created: 1940
published: 2022-07-27
---

vom Autor von „Out of Revolution“

### KAPITEL 1: Ein neues Kapitel der Weltrevolution

Ich nenne die Atlantische Revolution das Kapitel der Weltrevolution, das am 10. Mai 1940 eröffnet wurde.

Die Weltrevolution war mit dem deutsch-russischen Bündnis wieder virulent geworden.

Die Kurve der Revolution verläuft verblüffend parallel zur französischen. Nur, und das ist eine Voraussetzung für ihr Verständnis, vertauscht die Revolution unserer Zeit in der Abfolge der Ereignisse die Rollen von Krieg und Revolution im eigentlichen Sinne.
Die Französische Revolution beginnt mit drei Jahren Revolution, stürzt sich in 23 Jahre Krieg und endet mit einem kurzen revolutionären Epilog. Seit 1914 beginnt die Weltrevolution mit drei Jahren des Großen Krieges, dann beginnt das russische Kapitel der Revolution, dann das italienische, schließlich das deutsche im Jahr 1933, und 1940 kehrt sie zu ihrem Ursprung zurück, in Form eines Krieges.

Es ist jedoch ein Krieg im Gewand der Revolution und eine Revolution im Gewand des Krieges, genau wie die napoleonischen Kriege, und zwar durchgehend.

Die Atlantische Revolution betrifft die alten Mächte des Atlantiks: Holland und Belgien, Frankreich und Spanien, das britische Empire und die Vereinigten Staaten.

Soweit sich diese Mächte auf dem europäischen Festland befinden, weichen sie einfach den Mächten des Inneren. Mitteleuropa, das offiziell von Hitler und Mussolini vertreten wird, wird heute von der Vergangenheit, von Russland, genauso bedrängt, wie dieselben Länder im Jahr 1800 vom Westen bedrängt wurden.

Die USA, die Europa immer über die Atlantikküste gesehen haben, sehen sich plötzlich mit Italien und Deutschland als Treuhänder des gesamten Kontinents konfrontiert. Jeder atlantische Küstenstreifen, den die älteren atlantischen Mächte in Europa behalten, wird ein prekärer Besitz sein, entweder im Schatten Mitteleuropas oder als Vorposten der USA in Europa. Die Atlantische Revolution zwingt die USA, die Alliierten als souveräne Mächte abzulehnen.

Der König von England mag in dem prophetischen Spiel als Kaiser von Amerika überleben; aber es wird keinen Platz in Europa geben, der gegen Hitler gehalten werden kann, außer durch die ständige Wachsamkeit und den Machtwillen der USA.

Die Amerikaner sind so lange als bloße Beobachter und Zuhörer behandelt worden, dass ihnen der Gedanke an eine atlantische Revolution noch ziemlich unklar erscheint. Sie haben sich immer als außerhalb des Rings des Schicksals betrachtet.

Sowohl die Isolationisten als auch die Weltbürger diskutierten bei ihren „Foreign Affairs”-Mittagessen darüber, was man mit diesen widerspenstigen Völkern anderswo tun könnte, sollte oder würde. Aber es schien immer eine Frage des freien Willens, der freien Wahl, der Präferenz und der Vorliebe zu sein.

In dem Begriff "Atlantische Revolution" versuche ich die Vorstellung zu verdichten, dass die Weltrevolution eine überlegene Kraft ist.

Das letzte Jahrhundert kannte nur wenige Kräfte, die der individuellen Freiheit überlegen waren. Da war natürlich der Konjunkturzyklus, vor dem sich jeder freie Amerikaner wie vor einer mächtigen und unbarmherzigen Gottheit verneigte. Die Idee, den Konjunkturzyklus als Halluzination zu bekämpfen, wie die Astrologie des neunzehnten Jahrhunderts, war undenkbar. Die Konjunktur war der Hauptgott für alle.

In den atlantischen Regionen musste eine weitere Übermacht anerkannt werden: die britische Seemacht. Auch sie war gottgegeben. Diese Seemacht ermöglichte den Handel in der ganzen Welt und hielt die Kontinente offen; denn in jeder Mündung eines schiffbaren Flusses gab es einen kleinen Staat, Holland, Portugal, Uruguay, Rumänien usw., über den der Händler das Hinterland erreichen konnte.
All dies war gottgegeben.

Die einzige Kraft, die dem Menschen individuell überlegen war, war sein eigener Nervenzusammenbruch. Dieser, so musste er zugeben, überkam ihn mitten in seinen Plänen und Aktivitäten. Dieser Zusammenbruch war sein persönliches Schicksal. Er hielt ihn sorgfältig von den Zusammenbrüchen anderer Menschen getrennt. Wir versteckten die neurotischen Opfer in immer größerer Zahl in Irrenhäusern, oder wir erhöhten die Zahl der Selbstmorde.

Die atlantische Revolution ist die Herausforderung an die atlantischen Völker: den ägyptischen Zauber des Konjunkturzyklus abzuschütteln und die Energien zu bündeln, die in individueller Angst und Qual, individueller Frustration und Angst, Zusammenbruch und Leid schlummern. Gemeinsame Leiden schaffen, gemeinsame Leiden sind die einzige Grundlage für eine gemeinsame Politik und ein gemeinsames Leben.

Die Leiden von Privatpersonen, ihre privaten Westentaschenreligionen, müssen gebündelt und veröffentlicht werden. Die Konjunktur muss als öffentliche Religion geächtet und geleugnet werden. Er muss - mit all seinen Begleiterscheinungen - als das Idol einer vergangenen Epoche entlarvt werden.

Ich weiß, dass es viele Tempel für dieses Idol gibt, und sie sind sehr schön - wie das Brookings Institute in Washington oder das Department of Economics in Harvard. Es tut mir leid, aber Gott lebt nicht in Gebäuden aus Stein. Und die atlantische Revolution ist die Herausforderung des lebendigen Gottes, gegen die Götter, die in diesen Tempeln angebetet werden, und für die wirklichen Leiden von uns allen, in unseren Nervenzusammenbrüchen während der letzten zwei oder drei Generationen.

Die Weltrevolution ist in ihrer atlantischen Phase ganz anders als jede andere Phase. Da sie eine Weltrevolution ist, ist sie weder russisch noch italienisch noch deutsch. Das sind alles nur Kapitel oder Verse des Ganzen.

### KAPITEL 2: DIE SPALTUNG IST DIE SÜNDE DES MENSCHEN (W. BLAKE)

Es ist offensichtlich, dass die atlantische Revolution wenig von Russland oder von Mitteleuropa zu lernen hat. Ein Vergleich der wenigen zentralen Elemente kann dies zeigen.

Russland hatte 1917 keine moderne Maschinerie, keine alten "Kader", die zur Führung fähig waren, und ein Volk mit einer enormen Geburtenrate. Es hatte auf allen Seiten territoriale Grenzen und fast keine Küstenlinie.

1935 hatte Deutschland die modernste Maschinerie, keine Kader oder anerkannte Führung - die Monarchien waren 1918 verschwunden -, keine vernünftigen Grenzen und eine dicke Bevölkerung.

Im Jahr 1940 sind die Vereinigten Staaten stark unterbesetzt, mit einer Bevölkerungspyramide, die auf dem Kopf steht (die Alten überwiegen die Jungen), mit allen Maschinen, die man sich wünschen kann, mit kontinentalen Grenzen und mit ziemlich intakten sozialen Kadern.

Auf eine kurze Formel gebracht, kann man sagen:

- Russland: *keine Maschinen, keine Kader, keine Ozeane, Ströme von Menschen*
- Deutschland: *keine Kader, keine Ozeane, Maschinen, Menschenströme*
- USA: *Kader, Meere, Maschinen, keine Menschen.*

Die Menschen auf dem Beacon Hill mögen sich erleichtert fühlen, wenn sie diese Formel sehen. Sie scheint jedes Gerede von einer atlantischen Revolution als völlig töricht erscheinen zu lassen.

Die französischen Kader werden zwar von Hitler vernichtet und ausgelöscht, und zwar genau jetzt. Wie in Polen, wo die Kader, die Intellektuellen, der Adel, der Klerus, vernichtet wurden, so werden in Frankreich die Kader der Gesellschaft vernichtet. Frankreich mag in der einen oder anderen Form überleben; Paris ist für immer als Konzentrationspunkt des französischen Denkens verschwunden. Als Aragon seinen Paysan de Paris schrieb, nahm er das Jahr 1940 vorweg.

Die atlantische Revolution kann also die Form eines Krieges annehmen und Kader auslöschen, hinter denen kein Leben und keine Völker mehr stehen. Wir müssen dringend nach einer dritten Form jenseits des Bürgerkriegs und des militärischen Krieges suchen, in die wir die Weltrevolution diesseits des Atlantiks kleiden können.

Ich glaube, dass es eine dritte Form gibt. Aber zuerst diese sehr wichtige Frage: "Um Himmels willen, ihr sprecht immer von einer Weltrevolution. Je n'en vois pas la necessité. Warum sollte es überhaupt eine Weltrevolution geben? Geben wir den Alliierten ein wenig Hilfe, sperren wir ein paar unliebsame Rote und Nazis ein, und schon haben wir uns dieses Schreckgespenst vom Hals geschafft."
Die Weltrevolution ist nichts Erstaunliches. Als nach der Schlacht von Hastings der neue eiserne Ritter zum Sieger Europas wurde, als normannische Könige von Asow in Russland bis Palästina, Griechenland, Sizilien, England und Norwegen regierten, zwang die neue Rüstungsindustrie die Völker Europas in das Zunft- und Handwerkssystem ihrer mittelalterlichen Städte. 185 verschiedene Betriebe waren erforderlich, um einen voll gepanzerten Ritter mit seinen Pferden und seinem eigenen Geschirr usw. herzustellen. Und dieser Produktion musste alles andere untergeordnet werden.

Heute müssen hinter jedem Soldaten zehn oder zwölf Industriearbeiter mehrere Jahre lang arbeiten, um die Maschinen für seinen Kampf herzustellen. Alles andere muss dieser Tatsache untergeordnet werden.

1787 und 1789 wurden die Gesetzestexte geschrieben, ohne dass man die Kräfte und Ressourcen der Natur kannte, die das 19. Jahrhundert sich zu Nutze machen würde. Die Zunahme der der Natur entrissenen Kräfte erfordert immer eine neue Regelung.

Die ganze Weltrevolution besteht in der Umverteilung der natürlichen Kräfte und Ressourcen, vom Öl zur Elektrizität, von der Arbeitskraft zur Pferdestärke.

Selbst Herr Wilkie weiß das sehr gut. Es gibt viele Lösungen für diese Aufgabe. Die Kader der Gesellschaft, die die Forderung in jedem Land auf die einfachste und undoktrinärste Weise anerkennen, erwerben das Recht, dieses Land zu regieren.

Aus diesem Grund haben Ideologien mit dieser Aufgabe wenig zu tun. Der Kampf findet statt zwischen der Blindheit, die versucht, sich dem Problem zu entziehen - Männer wie Hoover oder Taft oder Coolidge oder Hull oder Garner oder Dewey oder Borah -, und dem Eingeständnis, dass Gottes Schöpfung viel größer, viel reicher ist, und dass Männer wie Wilkie oder McNutt den Dogmen von Rousseau oder Woodrow Wilson sehr widersprechen.

Die atlantische Revolution scheint mir in dem Kampf um dieses Eingeständnis zu bestehen. Woodrow Wilson ging mit der typischen Vision eines Mannes aus Princeton nach Europa: Europa müsse aus souveränen Nationen bestehen, im Stil von 1850. Wilson verdammte Europa dazu, in den Kleidern des vorindustriellen Zeitalters zu leben. Der Kreuzzug des Sternenbanners in das Heilige Land Europas zwang Europa zu einer Balkanisierung, die den ländlichen Gemeinden von 1776 vielleicht angemessen gewesen wäre, die aber die Amerikaner in Amerika nicht toleriert hatten, als sie die von der heimischen Regierung um die dreizehn Kolonien errichteten Zäune durchbrachen und nach Westen, in den unendlichen Raum strömten.

Die Endlichkeit der Wilsonschen Vision für den europäischen Raum war seine Pietätlosigkeit. Die Menschen leben nicht in Kisten. Wenn sie dazu verdammt sind, wie es das Völkerbundsprogramm vorsieht, haben sie keine andere Wahl, als diese Schachteln in Pillendosen zu verwandeln.

Der Mensch ist ein wanderndes und bewegliches Wesen, das niemals für immer in irgendeinem Teilchen des Raumes eingeschlossen oder einbalsamiert werden darf.

Dies ist das Credo Amerikas. Warum haben wir vom Rest der Welt verlangt, nach einem anderen Credo zu leben als unserem eigenen?
Das Eingeständnis, das von den Kadern und den Männern und Frauen, die die Kader der USA bilden, verlangt wird, lautet: Du sollst nicht in deinem Haus verschiedene Maße haben, ein großes für Amerika und ein kleines für Europa.

Der durchschnittliche Atlantic Monthly-Amerikaner hat zwei Weltanschauungen, zwei Religionen, zwei Ideologien, eine für sich selbst, eine für Europa. Und das lähmt und macht alle seine Handlungen und Gedanken zunichte. Spaltung ist die Sünde des Menschen, rief William Blake. Er würde es heute noch verzweifelter rufen. Was können wir tun? Die Antwort muss auf den Schrei antworten; sie muss geistig sein.

Wir können es uns nicht leisten, jemanden zu töten, mit der Kontinuität zu brechen, die Einheit abzuschaffen. Der einfache Grund für dieses tugendhafte Verhalten ist, dass wir ohnehin zu wenige Einwohner haben, zu wenig Kontinuität. Das Einssein ist die Frucht der Kontinuität.

Aber wir haben die ungenutzten privaten Leiden all unserer Neurotiker und Menschen in der Mitte des Lebens. Wir haben die Minderheiten, deren schlummernde Energien in den letzten Jahrzehnten nicht durch den Sog des Schmelztiegels angefacht wurden.
Die große Ketzerei der Deutschen besteht darin, nur an das Unendliche zu glauben und den Tod dem Leben vorzuziehen. "Les Allemands n'aiment pas la vie." Sie entstammen dem Unendlichen und lehnen es ab, sich durch endliche Formen jeglicher Art begrenzen zu lassen; sie müssen in den Krieg ziehen, der der gröbste Ausdruck des Unendlichen ist, das sich nicht begrenzen kann.

### KAPITEL DREI: GESPRÄCHE ÜBER DAS CHRISTENTUM SIND UNMÖGLICH

Die große Irrlehre der Neuen Welt besteht jedoch darin, dass sie ihren eigenen Vorstoß in den unendlichen Raum vergessen hat.
Die Seele des Menschen kommt aus dem Unendlichen ins Endliche.

Dem modernen Amerikaner wird von seinen Psychiatern gesagt, dass er das nicht tun soll. Er soll sich anpassen. Oder er wird in ein Irrenhaus gesteckt. Seine Leiden werden lächerlich gemacht: "Mensch, sei glücklich, sei endlich, sei begrenzt wie jedes Unternehmen. Fühle dich nicht verantwortlich für das Unrecht in der Welt."

Der Mensch ist unbegrenzt und grenzenlos. Der Mensch kommt aus dem Unendlichen und Unbegrenzten in diese endliche Welt, um zu leiden und diese Welt durch sein Leiden zu verändern. Die Pioniere haben gelitten und haben die Welt verändert. Die Atlantische Revolution ist der neue Planwagen. Sie lässt das alte Land der Psychologie und Soziologie hinter sich. Sie fordert die Generation des mittleren Alters heraus, sich einzugestehen, dass sie auf der Suche nach einer Seele ist. Sie fordert sie auf, ihre Leiden so ernst zu nehmen, wie es Keats tat, als er diese Erde das Tal der Seelenbildung nannte. (Brief an George und Georgiana Keats, Sonntag, 14. Februar 1819; Anm. d. Red.)

Die Umverteilung der neuen Kräfte der Natur ist die Überschrift der gesamten Weltrevolution. Man muss sich jedoch darüber im Klaren sein, dass die drei bisherigen Lösungen - die russische, die japanische und die deutsche - zur Errichtung hemisphärischer Wirtschaften Probleme lösen, die es in der westlichen Hemisphäre nicht gibt.

Russland hatte keine moderne Maschinerie, Japan befand sich auf einer Insel, Deutschland war der Rumpf jenes seltsamen Kreises von Rändern und Vorgebirgen, der sich Europa nennt.

Daher hungerten die Bolschewiken Russland zugunsten schwerer industrieller Investitionen aus, Japan irrt durch das chinesische Festland, die Nazis hungerten Deutschland für die Eroberung vertretbarer Grenzen aus.

Amerika ist ein Kontinent. Wir haben vernünftige Grenzen. Nur sind sie für das Leben zu vernünftig. Sie sind mit einem Meterstab gezogen, die Striche wie die Straßen von Washington oder die kanadische Grenze. Diese Vernünftigkeit ist ihre Gefahr. Wir sind leer, steril.

An die Stelle der Fruchtbarkeit ist in diesem Land die Ausbeutung getreten.

Wir hören hier Leute sagen, dass die Arbeitslosen zu viel für diesen Kontinent sind, und dass wir keine anderen hereinlassen können. Die Familien haben keine Kinder. Wir sind nicht von außen bedroht, sondern von diesem tief sitzenden Hass gegen die Fruchtbarkeit des Bodens und die Fruchtbarkeit der Familie und die Kreativität der menschlichen Seele.

Da wir die Grenzen und die Maschinen haben, muss die Umverteilung der Macht in deiner und meiner Seele stattfinden. Das "Ich" muss dem "Wir" und dem "Du" Platz machen.

Ich weiß sehr wohl, dass die Kader dieses Landes ihre Mission auf eine anständigere Weise betrachten. Sie sagen, dass sie die Werte der Zivilisation, des Christentums und einiger anderer benachbarter Schatzinseln, in diesem Aufruhr der Massen einer neuen Barbarei retten müssen.

Dies ist sicherlich die von Nietzsche und Solovjeff erwartete Herausforderung für das Christentum. Und die atlantische Revolution kann bedeuten, dass wir die Essenz der Offenbarung, die Frucht des Leidens durch viertausend Jahre Menschheitsgeschichte, für die ganze Menschheit retten müssen.

Davon bin ich überzeugt, in meinem Herzen. Aber ich bin nicht geneigt, es hier unter Ungläubigen zuzugeben, die von der Rettung des Christentums sprechen, nachdem sie es durch ihr Leben und ihre Arbeit und ihre gesamte wissenschaftliche und literarische Praxis der Lächerlichkeit preisgegeben haben.

Was?

Ich habe mehr als dreißig Jahre lang mit diesen akademischen Heiden gekämpft, und ich soll zugeben, dass Harvard das Christentum oder die Offenbarung retten wird?

Ich hoffe, dass sich sogar der Präsident und die Kuratoren von Harvard über eine solche Annahme ärgern würden.

Das zentrale Glaubensbekenntnis des Christentums ist sein Glaube an Tod und Auferstehung. Es glaubt an das Ende der Welt, immer und immer wieder. Daher hängt die Erhaltung des Christentums von der freiwilligen geistigen Bekehrung und Auferstehung der Generation mittleren Alters in diesem Land ab.

Das Christentum ist noch nie durch Wunschdenken gerettet worden. Die Bewahrung des Christentums ist unmöglich, unerwünscht, unnötig. Ein solcher Prozess ist antichristlich. Das Christentum sagt: Wer versucht, seine Seele zu retten, wird sie verlieren.
Und es spuckt seinen "Bewahrer" aus.

Diese Zeit ist nicht an privaten Bekehrungen oder privater Reue interessiert. Gott scheint sich wenig um Trunkenheit oder Verbote, Ausschweifungen oder körperliche Keuschheit zu kümmern. Er verlangt nach einer neuen Keuschheit des Geistes, nach einer amerikanischen Seele, die Hollywood und Broadway den ersten Platz in der Darstellung der Zukunft Amerikas verweigert und die bereit ist, für eine neue Konvergenz der Wissenschaften zu arbeiten.

Das Ende dieser Welt mit ihrer fairen Vermehrung der Neugierde ist in der Tat gekommen.

Alle Sünden sind mental.

Die atlantische Revolution kann immer noch als eine mentale Revolution geführt werden. Das Unglaubliche könnte geschehen, dass die hoffnungslosen Menschen mittleren Alters, die heute ihr Geld zum Psychoanalytiker tragen, es vorziehen, ihren Geist zurück in die Realität zu tragen, von wo aus die Dreieinigkeit von Liebe, Hoffnung und Glaube sie wieder ergreifen und Ordnung in 27 Millionen Atome bringen kann, so dass sie in kosmischer Harmonie schwingen.

Die Bewahrung der Werte kann nur erreicht werden, wenn die gebildeten Klassen des Westens diese Werte zuerst leben.
Dies geschieht nicht dadurch, dass man dem Kommunismus den Wind aus den Segeln nimmt und in einer Stunde der Gefahr plötzlich von der klassenlosen Gesellschaft spricht, nachdem man jahrzehntelang die Existenz von Klassen geleugnet hat.
Niemals zuvor, so wage ich zu behaupten, hing die bloße Bewahrung des Christentums so sehr von einem Akt des Glaubens ab, von der Auferstehung in den Köpfen der Erwachsenen, vom Umdenken der Menschen in der Mitte des Lebens.

Seelisches Leiden wird zu einer heiligen Pflicht.

Die Menschen müssen seelische Qualen erleiden, sonst sind sie dem Untergang geweiht.

Es reicht nicht aus, anderen in Kirchen oder Klassenzimmern die Regeln von vor zweitausend Jahren beizubringen. Die regierenden, lehrenden und predigenden "Kader" müssen die Regeln leben und sie auf ihr eigenes geistiges und intellektuelles Leben anwenden, hier und jetzt. Sie müssen Buße tun für die letzten dreißig Jahre des Verfalls, des Zynismus, der Entwurzelung, der Friedenskampagnen und Verbote. Sie müssen sich mit einem echten pater peccavi hinknien.

Wenn sie ihr Denken nicht verjüngen, wird die Welt unbarmherzig über die vergeblichen Versuche dieser gebildeten Leute lachen, die Zügel der Macht zu behalten.

Die Macht wird umverteilt, meine Freunde, und zwar aus dem einfachen Grund, dass Macht die Pflicht ist, für die ferne Zukunft zu sorgen.

### KAPITEL VIER: WIEDER GLAUBEN, LIEBEN, HOFFEN

Die Politik von heute funktioniert nicht mehr zwischen Wählern und Regierenden, denn der Bauer oder der Städter, der seine Stimme abgibt, ist morgen vielleicht kein Bauer, kein Städter mehr.

#### DIE GESCHICHTE EINES JUNGEN FREUNDES

Ein junger Freund von mir ging mit mir zu einer öffentlichen Anhörung von Landwirten und er rief aus: "Aber das sind doch alles alte Männer. In fünf Jahren wird die Hälfte von ihnen gestorben sein, und ihre Höfe könnten aufgegeben werden. Wie können sie Partner einer Politik für dieses Tal sein, die sich im Jahr 1980 auswirken wird?"

Der moderne Mensch in der Mitte des Lebens hat seine Eigenschaft als Gründer und Vater der Gesellschaft aus den Augen verloren. Er fragt nach Rechten, nach Glück, nach Subventionen oder nach Tarifen, nach Freiheit und Frieden, und zwar im Namen seiner selbst und nicht im Namen seiner Enkelkinder.

Nun gut. Das bedeutet, dass sich jemand anderes um die Enkelkinder kümmern muss. Er wird uninteressant, ein Massenmensch, der mit Hilfe des sechsfachen Hollywoods und des siebenfachen "daily" ruhig gehalten werden soll.

Macht wird verliehen, damit sie als Speerspitze der Gesellschaft wirken kann. Macht verschwindet immer dann, wenn offensichtlich wird, dass sie nicht zu diesem einen und einzigen Zweck ausgeübt wird, zu dem sie verliehen wurde.

Wozu also sollte unser Umdenken uns wieder befähigen? Ich kann die drei Hauptrichtungen nur als Beispiele nennen.
1. Die einzige Möglichkeit, unsere Demokratie zu retten, besteht darin, zu erkennen, dass die Demokratie nichts Religiöses ist, sondern der säkulare Ausdruck, das endliche Mittel, um etwas unendlich Größeres auszudrücken. Wir müssen in der Politik den Unterschied zwischen dem Unendlichen und dem Endlichen wiederherstellen.
Solange die Demokratie als Ersatz für das Christentum selbst, als eine Religion, behandelt wird, werden ihre Missbräuche als selbstverständlich hingenommen.\
Abgeordnete, die sich dem Lobbyismus und dem "Schreiben Sie Ihrem Abgeordneten" beugen, gelten als tugendhaft. Sie sollten hinausgeworfen werden. Das "Zu spät" der Demokratien in auswärtigen Angelegenheiten wird einfach als unvermeidlich hingenommen.
Aber es gibt nichts Unvermeidliches an der Dummheit der französischen und englischen Demokratien zwischen September 1929 und Mai 1940. Sie war nicht unvermeidlich, sondern einfach unentschuldbar.\
Und so starb die Demokratie mit all ihrer "Unvermeidlichkeit".\
Die Demokratie ist nicht unvermeidlich, aber die Zukunft der Vereinigten Staaten ist unvermeidlich. Wir sind genauso ungeschützt und unvorbereitet wie im Jahr 1776. Und diese Tatsache der Ungeschütztheit und Unvorbereitetheit ist die Plattform des Verfassungskonvents, die wir uns jetzt zu Herzen nehmen sollten.
2. Die Produktivität muss über die Vorurteile der Ökonomen im Finanzministerium und in der Lehre siegen.
Durch Nicht-Produktion und Arbeitslosigkeit haben wir das Dreifache der Schulden der Vereinigten Staaten verloren. Denn die Arbeit von zehn Millionen Menschen hätte in sieben Jahren etwa 125.000.0000.000 Dollar eingebracht.\
Aber Jahr für Jahr hat das Brookings Institute zur Zufriedenheit von Herrn Morgenthau bewiesen, dass Deutschland unter seinem Goldmangel zusammenbrechen musste.
3. Die Zivilisation kann nicht ohne geistige Leiden und körperliche Entbehrungen leben.
Das Leben der Friedenskampagne, das einfache Leben, das Leben der Verbote, das Leben der Nicht-Produktion, das Leben der Geburtenkontrolle kann nicht gepredigt werden, ohne das Leben selbst zu zerstören. Das Leben ist weder leicht noch verboten und wird es nie sein.
4. Der Mensch ist nicht bekannt. Der Mensch ist das bergauf gehende Tier der Schöpfung. Der Mensch tut das Unmögliche.
Wenn er es nicht tut, wird er überflüssig, ein Ärgernis auf dieser Erde. Er fällt unter die wilden Tiere, wird zum bloßen Vieh, wenn er seine Rolle als die unbekannte Größe des Universums aufgibt.\
Dann kratzt sich Mutter Erde immer und zerquetscht den Menschen wie eine lästige Fliege in irgendeiner Form der Zerstörung, im Krieg oder in einer Grippewelle.
5. Die eigentliche Gesundheit des Menschen und Potenz hängt davon ab, dass er auf dieser Erde etwas Unerhörtes zu leisten hat, dass er aus dem Unendlichen als Bote ins Endliche kommt.\
Der Mensch erkrankt, wenn er sich auf begrenzte Anstrengungen und auf eine Teilung des Ziels beschränkt. Begrenzte Anstrengungen führen nicht einmal zu begrenzten Ergebnissen.

Nun, ich bin bereit zuzugeben, dass dies wieder die alte christliche Lehre ist. Denn diese fünf Punkte verkünden:
1. Cesar ist nicht Gott, auch wenn Cesar sich als Demokratie verkleidet.
2. Die wahrsten Wirtschaftslehren sind nicht wahr genug, wenn das Leben auf dem Spiel steht.\
Die Wirtschaftswissenschaften befassen sich nicht mit der ewigen Wahrheit, sondern mit flüchtigen Bedingungen.\
Die Ökonomen haben versucht, den Teil unserer Existenz zu reparieren, der nicht repariert werden kann. In diesem Moment müssen die Ressourcen, die Kraft, die Intelligenz und die Arbeit dieses Kontinents ohne Rücksicht auf das Gold zusammengebracht werden.\
In "Out of Revolution" habe ich gezeigt, dass sich zu jedem Zeitpunkt verschiedene Wirtschaftssysteme überlagern müssen und überlagert haben, und dass dieses Land auch heute nicht allein vom Kapitalismus lebt.
3. Alle modernen Sozialplanungen oder Wahlkämpfe in diesem Land lassen das Prinzip der menschlichen Natur außer Acht, dass der Mensch über sich hinauswachsen kann.\
Der Mensch variiert von null bis unendlich, je nach unserem Glauben an den Menschen. Die moderne Hochschule, die moderne Fabrik, der moderne Politiker behandeln den Menschen nach dem IQ, dem Lohn und den Besitzständen. Sie machen den Menschen in allen drei Bereichen des Lebens kaputt.\
Der einzige Bereich, in dem er ernst genommen wird, sind unsere Spiele. Im Sport wird jeder als das rekordverdächtige Tier akzeptiert, das er ist. Anderswo üben wir uns im Unglauben.\
Diesen Irrtümern wollen wir unsere drei Grundsätze entgegensetzen: Wir glauben an die Göttlichkeit des Menschen, an seine Leichtgläubigkeit, an seine Bekehrbarkeit, an alle drei.

Was bedeutet das?

Es bedeutet, dass wir Glauben, Liebe und Hoffnung haben.
1. Unser Glaube besagt, dass der Mensch das Unmögliche vollbringen kann, dass er seine Haut abstreifen, seine Götzen verbrennen, seinen Vorlieben absterben und sich neue aneignen kann, wenn er seine Anbetung der eigenen Klugheit überwindet.
2. Unsere Liebe sagt, dass diese Neue Welt in uns allen stärker ist als alle politischen, wirtschaftlichen oder doktrinären Gewohnheiten, die durch ein geistiges Non possumus vereitelt werden können. Wir glauben, dass die völlige Ohnmacht der gebildeten Klassen in diesem Land nicht endgültig ist, weil wir Amerika lieben und glauben, dass sie es auch lieben.
3. Wir hoffen, dass die innere Leidenschaft in dieser atlantischen Revolution erreichen kann, was die äußere Gewalt anderswo erreichen musste. Die Verjüngung des Denkens, die Einwanderung in ein neues Reich des Denkens mag an die Stelle der früheren Grenze treten. Jeder möge sich jeden Abend fragen: In welchen unbekannten Teil des menschlichen Lebens bin ich heute eingewandert?\
Das ist die Frage an die Menschen im Schaukelstuhl, die ich seit so vielen Jahrzehnten im Atlantic Monthly lese, dass sie die Wellen vergessen haben, aus denen der Atlantik an den Klippen der Normandie besteht.

### KAPITEL FÜNF: TÄGLICHE WIEDEREINWANDERUNG

Marblehead vermittelt ein falsches Bild von der Atlantischen Revolution.

Der neue Normanne besetzt Frankreich, und Gott führt seinen Plan aus, wie die Wasser das Meer bedecken. "Die Kontinente, die sich aus den Wellen erhoben haben, können nichts Besseres tun, als der Lektion des Ozeans zu folgen und den Herrn so einmütig zu preisen wie die Wasser, die das Meer bedecken." (Out of Revolution, Autobiography of Western Man, 1938, S. 332).

Dieses Amerika ist nicht eine der vielen Nationen, die Hitler zerstören kann. Es ist eine neue Welt, eine Hemisphäre, ein Kontinent. Aber in dem vergeblichen Versuch, als bloße Nation zu versauern, hat Amerika den Glauben und die Hoffnung verloren und seine Liebe kastriert.

Die Grundsätze eines positiven Glaubensbekenntnisses für die atlantische Revolution sind also offensichtlich.

1. Wir müssen zu 1776 zurückkehren.

Die Einwanderung als spirituelle Kraft, als die Kraft, die reale Welt wiederzuentdecken und nicht die Welt von Beacon Hill, muss unser Ballettruf werden. Mit diesem Slogan bringen wir unseren Glauben an die Kraft des Menschen zum Ausdruck, sich zu verändern, Schwierigkeiten zu überwinden und ein neues Umfeld zu schaffen.

Die Einwanderung ist der gemeinsame Nenner, der Amerika immun machen kann gegen Hitlers Plan einer Revolution, in der er Sektion gegen Sektion, Rasse gegen Rasse, Klasse gegen Klasse aufstellen würde.

Die Erwachsenen sollten dafür sorgen, dass die Wiedereinwanderung zu einem moralischen und physischen Prozess im Leben ihrer Kinder wird. Das kann es werden, wenn die Kinder sehen, dass ihre Eltern

Wenn die Kinder sehen, dass ihre Eltern wieder stolz auf ihre eingewanderte Qualität sind (die diese Eltern jetzt zu unterdrücken versuchen), und wenn die Eltern von ihren Kindern einen Dienst verlangen, bevor sie die Kinder als Bürger eines Kontinents anerkennen, der von unserem Glauben an die schöpferische Kraft der Einwanderung lebt.

Was verlangen wir heute von unseren Kindern? Prüfungen? Geld verdienen? Ein Auto zu fahren? Ich nehme an, dass wir von ihnen erwarten, dass sie ein Auto fahren. Das ist so ziemlich alles.

Jetzt wollen wir ihnen Gerechtigkeit widerfahren lassen. Sie müssen mindestens ein Jahr im Civil Conservation Corps dienen. Die Römer verlangten fünfundzwanzig Jahre Dienst; ist es nicht lächerlich, dass kein Kind zur "Einwanderungsfähigkeit" dieses Kontinents beitragen muss, außer wenn es unterprivilegiert ist?

Bevor wir die Kräfte der Natur in der Neuen Welt umverteilen können, müssen wir den Nachweis erbringen, dass wir zu ihrer Erhaltung beigetragen haben.

Diese Einwanderung durch den Dienst am Boden wäre unser erstes Bekenntnis zur Weltrevolution und ihrer Forderung nach Umverteilung.
Erst wenn wir uns selbst und unsere Kinder aufgefordert haben, diesen Beitrag an Geist und Körper zu leisten, werden wir reif genug sein, über andere neue Grenzen zu diskutieren. Wir würden dann über zehn Millionen Menschen mit neuen Augen, neuen Hoffnungen, neuen Vorstellungen vom Leben verfügen.

Bevor wir sie haben, sind alle Diskussionen darüber, was wir tun sollten oder müssen, Augenwischerei, bis wir die neuen Menschen haben. Wir sind auf Null reduziert. Unsere elektrische Batterie ist entladen.

Das ist der Grund für die Sinnlosigkeit all unserer politischen Diskussionen. Unter desillusionierten Menschen, die nur an Fakten glauben, sind Worte steril. Das ist die eigentliche Gefahr aller politischen Entscheidungen, die jetzt befürwortet werden. Wir schaffen es nicht, Menschen hervorzubringen, die zu viel mehr fähig sind, als man ihnen zumutet.

Die einzige Disziplin, die uns retten kann, ist das Umdenken eines jeden Menschen. Sie und ich müssen wieder unterscheiden: die höheren Mächte, die niederen Mächte, die gleichen Mächte, oder anders ausgedrückt, Götter, Wirtschaft, demokratische Formen.
Der Mensch lebt unter überlegenen Kräften, er manipuliert die Dinge und Materialien, die weniger integriert sind als er selbst; er beherrscht die niederen Lebensformen (wir dürfen Eier und Fleisch essen, Eisen schürfen, Feuer benutzen usw., denn das sind minderwertige Lebensformen).

Und der Mensch lebt mit seinen Brüdern durch die Zeitalter des Menschen auf dieser Erde.

Der lebendige Gott erschafft die Erde und die Wellen der Elektrizität, des Radiums, des menschlichen Leidens, die unseren Weg erhellen. Und wir entdecken unseren Gehorsam ihm gegenüber wieder, in einer unendlichen Aufgabe.

Materie ist Materie: Wirtschaften ist Wirtschaften und muss gehandhabt werden, mit oder ohne Gold, aber sicherlich ohne jegliche Präferenz oder Dogma für eine bestimmte Form des Wirtschaftens. Wir beten für unser tägliches Brot und nicht für den Goldstandard, denn die Materie ist dem Menschen unterlegen und kann daher je nach den Umständen auf verschiedene Weise behandelt werden.
Der Mensch und seinesgleichen sind durch die Zeitalter hindurch ein einziger Mensch, in der Macht der Rede, der Lehre und der Schrift.
Ihr Eltern, lehrt ihr das, was über eure eigene Lebensspanne hinausgeht? Sprecht ihr über das Glück eurer Kinder hinaus in die Zukunft?
Wenn ihr das tut, bildet ihr die einzige legitime Sprache. Wenn ihr täglich in den Großen Menschen einwandert, den wir Kleinen einst bilden, durch wahre Sprache.

Glaubst du an diese Tatsache des "Filius sumus", "wir sind der Sohn", wie Augustinus es in zwei Worten ausdrückt, die mit der ganzen falschen Psychologie unseres Jahrhunderts aufräumen? Wenn nicht, dann schweigen Sie, um das Christentum und die westliche Zivilisation zu retten. Sie wissen nicht, was es verdient, gerettet zu werden, oder was gerettet werden kann.

Der Mensch kann in Kontinuität und Anstand leben, solange er alle drei Grundsätze dieses Glaubensbekenntnisses tatsächlich praktiziert. Denn dann handelt er als Mensch, als Sohn, und nicht als Mensch, als bloße Anhäufung.

Die Beziehungen zwischen den Menschen ohne gleichzeitige Beziehungen zu den niederen und den höheren Kräften bleiben unfruchtbar.

### KAPITEL SECHS: EINE GRUPPE VON GLÄUBIGEN?
Eine Familie, in der jeder eine Arbeit außerhalb des Hauses hat und in der nicht einmal die Mahlzeiten gemeinsam eingenommen werden, löst sich auf, weil das materielle, minderwertige Leben nicht gemeinsam ist. Eine Familie, die nicht zu den Lehren aufschaut, die die Individuen als kosmische Gesetze unterwerfen, bleibt kinderlos.

In der Gesellschaft sind Streiks, Industriespionage, Lobbyismus unvermeidlich, wenn die Menschen anfangen, darüber zu sprechen: "Ich bin nur ein Mensch!" Das ist nicht sehr viel, denn das Niveau der Menschheit ist zwischen dem höheren und dem niedrigeren angesiedelt. Und der Mensch, der für nichts verantwortlich ist, der keinem Vorgesetzten gegenüber verantwortlich ist, wird verrückt oder dumm.
In der Erziehung führt das Weglassen der drei Ebenen zu albernen Diskussionen zwischen "Gleichen", in denen der Lehrer für nichts verantwortlich ist und der Schüler niemandem gegenüber verantwortlich ist.

Können wir eine Gruppe von Menschen aufbauen, die an die Macht, die Autorität und die Einsicht des Menschen in die Veränderung glauben? Wenn sie, wie ein Mann wieder einwandern ...

Das ist jetzt vorbei.

Aber die Neue Welt kann ihren alten grenzenlosen Mut wiedererlangen, wenn sie die Wiedereinwanderung in das Leben eines jeden Kindes einführt. Dies sollte unsere gemeinsame gegenwärtige Aktivität werden, die aus unserer Vergangenheit erwächst und in unsere Zukunft weist - aus der naiven Zugehörigkeit zu einer Minderheit, aus dem Eingeborenensein in einem Staat, die alle wieder in die Neue Welt einwandern müssen, die jenseits des Nationalismus liegt.

Dieser Prozess muss unser ganzes Denken und Handeln prägen: Es ist unsere Art, an die Seele des Menschen zu glauben, an seine Kraft zu wachsen und sich zu verändern. Wenn wir ihn loslassen, werden sich die Minderheiten um ihre eigenen Interessen scharen und der Schmelztiegel verliert seinen Zauber.

Das amerikanische Zeitalter wird andauern, die Vereinigten Staaten von Amerika werden andauern, durch alle unvermeidlichen Veränderungen in Politik und Wirtschaft hindurch, solange wir die Einwanderung ohne
Ende zum Leitstern in der amerikanischen Flagge machen. Solange eine mutige Wiedereinwanderung Amerika wiederentdeckt, kann sich diese Hemisphäre sicher fühlen.

Aber Sicherheit ist nicht das letzte Wort im Leben. Wir müssen auch wissen, dass wir einen echten Beitrag für Europa und Asien leisten.
Und das können wir auch.

Denn diese beiden Welten haben den Glauben an die Kraft des Menschen aufgegeben, sich persönlich zu wandeln, ein Mensch zu werden, der Verantwortung übernimmt.

Die ersten Christen sind aus dieser Welt ausgewandert, als Märtyrer und Mönche. Rassisten und Nationalisten, natürliche Menschen, die sich auf ihrer ersten Geburt ausruhen, vertuschen ihre Auswanderung und bleiben in einem zufälligen Umfeld und einer bestimmten Nationalität stecken.

Wir sind keine Abtrünnigen von dieser Welt. Die grenzenlose Hoffnung, dass der Mensch weder ein Klassen- noch ein Rassenprodukt ist, dass er nicht der Sklave seiner Umwelt ist, sondern Tag für Tag eine neue schafft, hat die Millionen von Einwanderern an diese Gestade geführt.

Dies ist der Kern unseres Glaubensbekenntnisses, und es ist ein unverzichtbarer Grundsatz im Glaubensbekenntnis der gesamten Menschheit.

In diesem Sinne verteidigen wir tatsächlich die Freiheit der Menschheit.

Die Worte eines jungen College-Absolventen aus dem Jahr 1776 bleiben wahr: "Das Schlimmste, was passieren kann, ist, auf den letzten kahlen Berg Amerikas zu fallen, und wer dort bei der Verteidigung der verletzten Rechte der Menschheit stirbt, ist glücklicher als sein Bezwinger. Es war und muss immer das Privileg von Erziehern und Studenten sein, diesen American Way of Life zu verinnerlichen, als einen mutigen Akt, durch den jeder junge Mann in die große amerikanische Gesellschaft der Zukunft eingeführt wird."

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_The_Atlantic_Revolution.pdf' | relative_url }})

[Das Orginal von „THE ATLANTIC REVOLUTION (1940)” als PDF-Scan](http://www.erhfund.org/wp-content/uploads/344.pdf)

[zum Seitenbeginn](#top)
