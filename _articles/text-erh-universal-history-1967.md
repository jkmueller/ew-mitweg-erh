---
title: "Eugen Rosenstock-Huessy: Universal History (1967)"
category: lectures
created: 1967
published: 2023-09-22
---


- [Eckart's Edition: ERH Universal History (1967)]( {{ 'assets/downloads/wilkens_erh_Universal_History_1967.pdf' | relative_url }})

- [The transcript of „ERH Universal History (1967)” as PDF](https://www.erhfund.org/wp-content/uploads/657.pdf)
- [The audio recordings of „ERH Universal History (1967)”](https://www.erhfund.org/lecturealbum/volume-32-universal-history-1967/)
