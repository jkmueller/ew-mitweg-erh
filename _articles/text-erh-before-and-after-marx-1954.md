---
title: "Eugen Rosenstock-Huessy: Before and after Marx (1954)"
category: lectures
created: 1954
published: 2023-09-16
---
### NOTE BY THE EDITOR

This is the text of a lecture in two parts as transcribed by Frances Huessy with the following measures:

- mere oral interjections as “you see”, “so to speak” are eliminated,
- rarely a word is added to make the sentence completely understandable,
- the text is divided into chapters, parts with Roman ciphers, parts with Arabian ciphers, paragraphs (no sentence is lost),
- with added titles to the chapters and headings for the answers given to questions
- the stories told out of the speaker ́s own experience are titled and colored,
- Added are: Contents, an index of names, an index of stories.

The title of the lectures is mentioned by Rosenstock-Huessy himself as “**Before and after Marx**” – *the four disangelists Marx, Freud, Nietzsche, Darwin* are not the main theme although the main background.

*Cologne, November 25, 2016 \
Eckart Wilkens*

- [Eckart's Edition: ERH Before and after Marx (1954)]( {{ 'assets/downloads/wilkens_erh_Before_and_after_Marx_1954.pdf' | relative_url }})

- [The transcript of „ERH Four Dysangelists 1954” as PDF](https://www.erhfund.org/wp-content/uploads/635.pdf)
- [The audio recordings of „ERH Four Dysangelists 1954”](hhttps://www.erhfund.org/lecturealbum/volume-10-four-dysangelists-1954/)
