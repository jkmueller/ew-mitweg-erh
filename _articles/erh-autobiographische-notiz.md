---
title: "Eugen Rosenstock-Huessy: eine autobiographische Notiz"
category: hinfuehrung
order: 110
---

![Eugen Rosenstock-Huessy, um1960]( {{ 'assets/images/rosenstock_150.png' | relative_url}}){:.img-left.img-small}

*„Der Atheist wünschte, ich sollte in die Theologie verschwinden.\
Die Theologen meinten, ich sei wohl ein Soziologe,\
die Soziologen murmelten: wahrscheinlich ein Historiker.\
Die Historiker waren darob entsetzt und riefen: ein Journalist.\
Aber die Journalisten verdammten mich als Metaphysiker.\
Die Metaphysiker ihrerseits hielten Wache am Tor der Philosophie\
  und fragten bei den Staatswissenschaftlern meinetwegen an.\
Die Juristen sind ja schon im Mittelalter als schlechte Christen bekannt gewesen,\
  und so wünschten sie mich in die Hölle.\
Damit konnte ich mich schließlich einverstanden erklären,\
denn als Mitglied der gegenwärtigen Gesellschaft\
kommt unsereiner aus der Hölle ja nur für Augenblicke heraus.”*
