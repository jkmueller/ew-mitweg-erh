---
title: "Neuübersetzung von Eugen Rosenstock-Huessy: Des Christen Zukunft"
created: 2014
category: bearbeitung
published: 2021-12-28
---

![des Christen Zukunft]({{ 'assets/images/agenda-des-christen-zukunft.jpg' | relative_url }}){:.img-right.img-large}
### Eugen Rosenstock-Huessy
## Des Christen Zukunft
### oder:
### Wir überholen die Moderne
#### neu aus dem Englischen übersetzt von Eckart Wilkens

Unter den vielen Büchern, die Eugen ­Rosenstock-Huessy (1888-1973) nach dem Ersten Weltkrieg geschrieben hat und für die er den Maßstab hatte, sie müßten Mitweg mit den Ereignissen sein, ist Des Christen Zukunft, auf Englisch 1946 erschienen, datiert auf Advent 1945, die Hagebutte, die unansehnliche, aber gediegene Kernlehre, die Frucht, in der die ganze Rosenblüte essentiell gefaßt ist. Die deutsche Ausgabe erschien 1955 in einer Übersetzung, die das Ereignis von 1945 nicht wahrnehmen konnte, weil die Betörung durch die Leiden des Weltkriegs II und der Hitler-Finsternis zu stark war. Durfte einer, der Deutscher war, in Amerika 1945 so etwas schreiben?

[Eugen Rosenstock-Huessy: Des Christen Zukunft, agenda, 2015](https://agenda.de/produkt/eugen-rosenstock-huessy-des-christen-zukunft/)
