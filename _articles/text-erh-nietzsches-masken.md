---
title: "Eugen Rosenstock-Huessy: Nietzsches Masken"
category: bearbeitung
created: 1944
published: 2024-01-10
---
#### NOTIZ DES ÜBERSETZERS

I

In Lise van der Molens Bibliographie steht zu dem Stück „Nietzsches Masken“ unter
dem Jahre 1944 (hundertster Geburtstag Nietzsches am 15. Oktober):

(Sollte in das unveröffentlichte Stück Friedrich Nietzsch´s Function in the Church and the
Crisis of Theology und Philosophy 1942 eingefügt werden.)

Dieser Meinung ist er gefolgt, und so gelangte das Stück zu mir.

Ich bezweifle die Einfügung und meine, es sei doch eine eigene Kundgebung zum
hundertsten Geburtstag Nietzsches, zu dem Tag, an dem das In-sein-Eigen-kommen
sich seltsam verschränkt mit den Zeitläufen, dem Jahr, in dem der Zweite Weltkrieg
durch den deutschen Widerstand, offenbar werdend aller Welt am 20. Juli 1944,
entschieden wurde. Die seltsame Verschränkung der zerstörerischen Gedanken
Nietzsches und der brutalen Täter dieser Zerstörung, der Nazis, forderte dazu
heraus: sie machen, was Nietzsche vorausgelebt hatte, sein Zusammenbruch 1889
war schon der Zusammenbruch von 1918 und eben von dem nicht mehr
abzuwendenden Zusammenbruch 1945 auch.

Nimmt man die Einfügung heraus, dann ist der Anschluß im Text von 1942
erkennbar:

*vor der Einfügung:*\
Aber da das Abendland die Welt des Raumes mit Hilfe des Porphyrius, Senecas,
Ciceros, des Aristoteles, Platos, in dieser Reihenfolge (das heißt in umgekehrter als
der historischen Folge) entdeckt hatte, beugte sich Nietzsche über das Floß des
Christentums auf der Flut der Zeit. Er sah die vor-platonische Welt des Dionysos, Er
fand in ihm eine bessere Analogia entis, er fand, daß der Mensch gar nicht so rational
sein möchte wie Sokrates, sondern vergöttlicht.

*nach der Einfügung:*\
Wird die Kirche angegriffen, wenn der Mensch vergöttlicht wird?
Die Vergöttlichung des Menschen war die erklärte Absicht der Kirche der Heiligen.
Nietzsche kommt nirgendwo mit der historischen Wirklichkeit dieser Kirche der
Erlösten zurecht. Jene Riesen und Söhne Gottes waren einfach unbekannt.

II

Die Übersetzung ist nach der Methode gegliedert, daß der Wechsel der Töne:
präjektiv, subjektiv, trajektiv, objektiv auf drei Ebenen verfolgt wird:
- *den Kapiteln von eins bis vier,*
- *den durch römische Ziffern bezeichneten Abschnitten dieser Kapitel,*
- *den durch arabische Ziffern bezeichneten Teilen dieser Abschnitte,*

Die Absätze folgen nicht unbedingt diesem Muster. Die Sätze sind unverändert
gefaßt.

Wie genau diese Gliederung ist, möge die Progression der Teil III, 3 in den Kapiteln
zeigen:

***Erstes Kapitel (präjektiv):***

Um ein ergreifendes Beispiel zu geben:

Als die sturen Sachsen endlich Christen wurden, bauten ihre Fürsten auch Klöster.
Erst fünfzig Jahre nach der Bekehrung wurde die junge Prinzessin Hathumoda
Äbtissin. Ihr Bruder, der Abt, zwanzig Kilometer weiter, tröstete die Nonnen über
ihren Tod, indem er in folgendem Distichon Hathumoda mit den elf heiligmäßigen
Frauen in der Bibel verglich:

*“Sara, Rebecca, Rachel, Debora, Noemi, Ruth et Anna,\
Holda, Susanne, Judith et simul Hester…”*

Die Seelenschaft ersetzte die Stammesgalerie der Vorfahren.

***Zweites Kapitel (subjektiv):***

Und das ist wahr, daß Nietzsche erst lange nach seinem geistigen und physischen
Tode in sein Eigen kam. Wir brauchen dafür die Prägung eines neuen Begriffs.
Nietzsche ist der Prototyp einer wichtigen Möglichkeit für uns alle, die wir in die
Schule des Christentums gegangen sind.

Wir sind alle *Distemporarier*. Unser Dasein erschöpft sich nicht darin, Zeitgenossen,
laufendes Ereignis zu sein.

***Drittes Kapitel (trajektiv):***

Andrerseits kann er viele nicht aushalten.

Der Mensch kann weder jemand sein, der alle Rollen und sozialen Funktionen
vereint, noch kann er vormachen, er sei ein Held.

Die Psychologie, die von der menschlichen Person meint, ihre Einheit wäre für den
Menschen normal und Schizophrenie eine Krankheit, liegt da ganz falsch. Der
Mensch ist kraft Natur keine Einheit. Das ist er erst, wenn er Leichnam ist. Solange er
lebt, muß er zwischen seinen toten und zukünftigen Elementen unterscheiden.

Dadurch wird er zu drei Dienern seines eigenen Lebens.

*Zu einem Teil muß er sein eigenes Grab schaufeln,*\
*zum Teil der sein, der sein eigenes Leben verheißt.*\
*Dreieinigkeit ist das höchste, was er erreichen kann.*

Das ist so nah wie möglich der Göttlichkeit, wie wir je zu kommen hoffen können.
Unser unzerstörbares Wasserzeichen als Menschenwesen ist dieses Kreuz unsrer
*Distemporanität,*

oder unsre *polychrone Existenz.*

Das ***vierte Kapitel*** erreicht diese Position gerade nicht mehr.

...

*[die komplette Notiz befindet sich am Ende der Übersetzung]*

[Eckarts Übersetzung]( {{ 'assets/downloads/wilkens_erh_Nietzsches_Masken.pdf' | relative_url }})

The original English text: [Nietzsche's masks](https://www.rosenstock-huessy.com/text-erh-nietzsches-masks-1944/)
