---
title: "Eugen Rosenstock-Huessy: Europa ist nicht mehr die nette Mitte der Welt (1957)"
category: bearbeitung
created: 1957
published: 2024-12-14
---

**EUGEN ROSENSTOCK-HUESSY**\
**EUROPA IST NICHT MEHR DIE NETTE MITTE DER WELT**

erschienen in der Zeitung *Die Welt am Sonnabend*, 6. Juli 1957\
Leicht gekürzte Fassung des am 23. Juni 1957 an der Bochumer Evangelischen Akademie gehaltenen Vortrags mit dem Titel:\
*Die künftige Widersacherin der Kirche*

#### NOTIZ

Von Mai bis Anfang August 1957 hielt Eugen Rosenstock-Huessy eine Gastprofessur in Münster inne. Während dieser Zeit Reisen und Besuche, dazu Tagungen und Vorträge in Loccum, Arnoldshain, Bonn, Bethel, Vlotho, Dortmund, Bochum. Einer von diesen gelangte als Geburtstagsgruß am 6. Juli 1957 in die Zeitung Die Welt.

Die Abschrift gliedert den Vortrag, wie er abgedruckt ist (verkürzt, wie in der Vornotiz und von den Pünktchen am Ende vermeldet), in vier Kapitel, die deutlich machen, wie Rosenstock-Huessy im mündlichen Sprechen das von ihm benannte Kreuz der Wirklichkeit im aufmerksamen Hörer errichtet:

ihn nach vorwärts ziehend (erstes Kapitel: Der anbrechende Friede) \
ihn nach innen einladend (zweites Kapitel: Der Abgrund) \
ihn in die gemeinsame Geschichte hineinsprechend (drittes Kapitel: Nietzsche,
Thronfolger-Selbstmord, Erdbeben-Rede von Eduard Sueß)\
ihn mit der konkreten Befindlichkeit konfrontierend (viertes Kapitel: Die Verirrung
der Landeskirchen)

Dem entsprechend ruft das Inhaltsverzeichnis den Leser in die Zukunft, die mit dem Vortrag eröffnet wird.

Das Namensverzeichnis lädt in den Innenraum ein, in dem sich der Sprecher glaubt mit den Hörern gemeinsam zu befinden.

Das Vermerken der Geschichten, die erzählt werden, manchmal in denkbar kürzester Form, ruft den Hörer in die gemeinsame Geschichte.

Die Merksätze heben hervor, was prägnant im Gedächtnis stehen bleiben mag.

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Europa_ist_nicht_mehr_die_nette_Mitte_der_Welt.pdf' | relative_url }})

Der Text ist Teil des Buches: *Friedensbedingungen einer Weltwirtschaft: zur Ökonomie der Zeit*
ed. Rudolf Hermeier  (Frankfurt am Main: Haag + Herchen Verlag, 1988), 339 pp., Arnoldshainer Schriften zur Interdisziplinären Ökonomie, Bd. 14.
