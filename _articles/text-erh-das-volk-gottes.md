---
title: "Eugen Rosenstock-Huessy: Das Volk Gottes in Vergangenheit, Gegenwart, Zukunft (1961)"
category: bearbeitung
created: 1961
---
in: Juden, Christen, Deutsche, hrsg. von Hans Jürgen Schulz, Freiburg i. Br., S. 198-220

in der Abschrift gegliedert, mit Inhalt, Namensregister, Liste der Geschichten und Merksätzen und einer erklärenden Notiz\
von Eckart Wilkens

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Das_Volk_Gottes.pdf' | relative_url }})

[Das Orginal von „Das Volk Gottes in Vergangenheit, Gegenwart, Zukunft (1961)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/539.pdf)
