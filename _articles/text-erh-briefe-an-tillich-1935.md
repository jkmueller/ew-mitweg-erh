---
title: "Eugen Rosenstock-Huessy: Briefe an Paul Tillich (1935)"
category: bearbeitung
created: 1935
published: 2023-09-11
---
Diese drei zusammengehörigen Briefe, veröffentlicht in der Gesamtausgabe der Werke Paul Tillichs, bringen zu Gehör (oder zu Gesicht), wie die Jahre in Amerika von 1933 bis zu seinem Tode vierzig Jahre später für Eugen Rosenstock-Huessy angelegt waren, nachdem er zwei Jahre dort gewesen.

Zu Gehör und zu Gesicht in einer Weise, die mit einer Härte die Klarheit des geteilten Schicksals zum Ausdruck bringt, wie es nur möglich ist, wenn der Angeredete ohne weiter Umstände versteht, wie die Geistesmächte nun walten, dem, wie Paul Tillich, die genannten Namen nicht nur Schall und Rauch sind, sondern Ereignisse im eigenen Leben.

1

Der alles durchziehende Vorwurf, Rosenstock-Huessy schreibe „intuitiv“ bringt auf den Punkt, was ihm – in vielen Variationen – vorgehalten wurde, das war die freundliche Version des Vorwurfs, unverständlich zu sein, zu impulsiv im Wechsel der Aspekte, nur dem Bauchgefühl folgend, was als nächstes dran ist usw.
Meine Version, die den inneren Rhythmus dieser Ansprache lesbar macht, bringt denn auch heraus, in welch vielfältiger Weise dieser Vorwurf widerlegt wird.

2

Die Härte Rosenstock-Huessys – gegen sich selbst – ist nicht zu überbieten. Besonders die Klage, wie lebensgefährlich es war, die Abschaffung des geistigen Eigentums vorwegzunehmen: das ist bis heute nicht gesühnt.
Und dann der Grund für die Vorlesungen in Amerika, die in die Tiefe gehenden Studien, die dann bei der Verwirklichung des Plans der zweibändigen Soziologie 1958 endlich Frucht getragen haben, auf dem Rückweg, bei der Einwanderung des Werkes nach Europa, wie hier die Einwanderung nach Amerika abgeschildert ist.

3

Es ist wohl treuherzig genug, wenn ich hier noch den Brief Paul Tillichs vom 25. März 1944 hersetze, der – mit zwei Briefen von 1935, also im Zusammenhang mit den drei Briefen Rosenstock-Huessys – auch abgedruckt ist, aber doch ja wie die Antwort auf die Briefe von 1935 zu lesen ist, neun Jahre später:

*New York, March 25, 1944*

Lieber Eugen:

Dein Brief macht mir viel Kopfzerbrechen.

1) können weder ich noch Hanna noch meine Sekretärin den geschriebenen Text ganz entziffern, und das bedeutet, daß gewisse Zusammenhänge nicht ganz klar werden.

2) ist mir nicht deutlich geworden, inwieweit Dein Brief eine Kritik des Textes meines Aufsatzes ist.

Den Vorwurf, daß ich noch an Geistesgeschichte glaube, verstehe ich einfach nicht, da ich ja die ganze gedankliche Entwicklung im Zusammenhang mit der Entwicklung der bürgerlichen Gesellschaft verstehe und die einzelnen Typen des existentialen Protestes gegen die Objektivierung der Welt gar nicht auseinander ableite, sondern nur Analogien der Situation zeige.

Den Zusammenhang Deiner Kritik mit unseren biographischen Erlebnissen kann ich auch nicht durchschauen.

Ich habe Kroner zu Hilfe geholt, der meinem Aufsatz nicht sympathisch gegenüber steht, weil er den Versuch der Existential-Philosophen, den Glauben philosophisch zu fassen, überhaupt ablehnt. Er wendet sich immer mehr der theologischen Orthodoxie zu, aber er weiß nicht, ob dies auch der Sinn Deiner Kritik ist.

Überrascht hat mich Deine hohe Wertung von Nietzsche. Ich selbst werte doch wohl von allen, die ich in dem Aufsatz charakterisiert habe, Marx am höchsten, weil er die soziologische Situation direkt begriffen und als solche angegriffen hat. In dem Sinne habe ich die religiösen Sozialisten oder genauer die Kairos-Philosophie in den Rahmen der ganzen Entwicklung hineingestellt.

Daß ich das getan habe, zeigt durch sich selbst, daß ich mich in einer gewissen Distanz dazu weiß.
Was Du über Heidegger und Rosenzweig sagst, gibt mir viel zu denken, aber ich kann nicht sagen, daß ich es verstanden habe.

Ich wünschte, wir könnten Deinen Brief einmal zusammen durchlesen und an der Debatte über ihn unsere gegenseitigen Stellungen klären.

Meinen Aufsatz habe ich trotz aller Interpretation, die ihm zu Grunde liegt, historisch gemeint und würde mich nicht direkt in ihn hineinstellen.

Um Dir zu zeigen, was ich jetzt tue, schicke ich Dir beiliegende Erklärung, die von einer Gruppe von politisch interessierten deutschen Emigranten seit November entworfen und unterschrieben ist. Vielleicht wird sie irgendwann einmal veröffentlicht. In unserer Gruppe sind alle Anti-Nazi-Gruppen von den Katholiken bis zu den Kommunisten eingeschlossen.

Wie stellst Du Dich dazu?

Mit herzlichem Gruß Paulus
*(Prof. Paul Tillich)*

PS. Ich habe im lateinischen Lexikon vergeblich versucht, herauszufinden, was „circumincedendus“ heißt. Soll es bedeuten: „Jemand, der beschnitten werden soll oder muß“?

4

Das Leben Jesu Christi ist – auch das wieder ein Gegensatz zwischen uns – immer
der Ausgangspunkt für all mein Denken gewesen, *niemals seine Lehre!*

ist denn doch wohl der Kern des Mißverstehens: Tillich ging es um die Lehre – obwohl die Zeit des Leviten abgelaufen ist. Und Rosenstock-Huessy, da er nun doch sprach, ist nicht leicht als der barmherzige Samariter des Denkens zu verstehen.

5

Dazu die Verse aus Ja und Nein:

*Von mir wird vorausgesetzt, \
daß ich ruf-entspringe; \
wird dies Rufgebot verletzt, \
bleib ich toter Dinge.*

*Aber solchen Anrufs Kraft \
singt mich in die Zeiten, \
aus des Raums Gefangenschaft \
freilich auszuschreiten.*

*Aus den Rufen wird Beruf, \
und der heißt mich: singe! \
Weil mich nun das Wort erschuf, \
werd ich guter Dinge.*

*Wer sich selbst vorausgesetzt \
wie die Prinzipiellen \
und sich als Beginn verschätzt, \
fährt flink in die Höllen.*

*Seine Mitgift hat solch Fant \
schamlos weggeätzt; \
denn der Seele Mitgliedsstand \
wird vorausgesetzt.*

*Nobis est propositum \
bene auscultare, \
donec verbum optimum \
discimus cantare.*

6

Ist wohl jemals kürzer verkündet worden, was als Schicksal der Universität, des Levitenamts am eigenen Leibe erfahren wurde:

1 (*präjektiv*)

Es hat seine Totengräberfunktion – welche nur die Erneuerung der speziellen zeitlichen Lehrform betraf -, mißverstanden als eine Funktion, durch welche Lehre überflüssig werde.
Diese Forschung frißt sich und die Universität nun auf.

2 (*subjektiv*)

Aus ....!

3 (*trajektiv*)

Nun scheint es also zu heißen: Ablösung vor!

4 (*objektiv*)

Ist die Repräsentation des Samariters in einer Institution nicht unmöglich?

7

Der im dritten Brief angebotene Friedensschluß zwischen Raum- und Zeitdenker ist doch wohl in jeder Hinsicht beispielhaft!

*Köln, 31. Januar 2017\
Eckart Wilkens*

[Eugen Rosenstock-Huessy: Briefe an Tillich]({{ 'assets/downloads/wilkens_erh_Briefe_an_Tillich_Febr_1935.pdf' | relative_url }})
