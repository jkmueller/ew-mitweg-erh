---
title: "Eugen Rosenstock-Huessy: Greek Philosophy (1956)"
category: lectures
created: 1956
published: 2023-09-12
---

Eckart added a \
"Note On Editing The Lecture On Greek Philosophy 1956/57 By Eugen Rosenstock-Huessy" \
at the end of "Greek Philosophy Contents etc" \
where he explains, again, his approach to editing the transcripts.



- [Eckart's Edition: ERH Greek Philosophy 1956 1-12]( {{ 'assets/downloads/wilkens_erh_Greek_philosophy_1_1956.pdf' | relative_url }})
- [Eckart's Edition: ERH Greek Philosophy 1956 13-26]( {{ 'assets/downloads/wilkens_erh_Greek_philosophy_2_1956.pdf' | relative_url }})
- [Eckart's Edition: ERH Greek Philosophy 1956 Contents etc]( {{ 'assets/downloads/wilkens_erh_Greek_philosophy_contents_etc.pdf' | relative_url }})

- [The transcript of „Greek Philosophy 1956” as PDF](https://www.erhfund.org/wp-content/uploads/641.pdf)
- [The audio recordings of „Greek Philosophy 1956”](https://www.erhfund.org/lecturealbum/volume-16-greek-philosophy-1956/)
