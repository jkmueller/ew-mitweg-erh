---
title: "Vorstellung Rosenstock-Huessys: Teil 1 – Leben"
category: hinfuehrung
order: 100
---

## Herkunft

Am 6\. Juli 1888 wurde Eugen Moritz Friedrich Rosenstock als Sohn von Theodor und Paula Rosenstock in der Plantagenstraße 3 in Berlin geboren. Er hatte drei ältere und drei jüngere Schwestern. Er besuchte das Friedrich-Wilhelm-, dann das Joachimsthaler Gymnasium und machte 1906 Abitur. Danach Studium der Rechtswissenschaften in Zürich. Als einer, der, wie er es sagte, „als Christ aufgewachsener Gläubiger” war, ließ er sich in demselben Jahr auch taufen.

## Hochschule

Das Examen rigorosum war am 22\. April 1909\. Er erlangte an der Ruprecht-Karls-Universität zu Heidelberg die Doktorwürde mit der Arbeit „Landfriedensgerichte und Provinzialversammlungen vom 9\. bis 12\. Jahrhundert”. Dem Vaterland diente er als Einjährig-Freiwilliger bei der Artillerie in Kassel. 1912 habilitierte er sich mit der Arbeit „Ostfalens Rechtsliteratur unter Friedrich II.” und warmit 24 Jahren!der jüngste Privatdozent an der juristischen Fakultät in Leipzig für Deutsches Privatrecht, deutsche Rechtsgeschichte, ab 1914 auch für Staatsrecht. Während eines Studienaufenthaltes in Florenz 1913-1914 lernte er seine Frau, die Schweizerin Margrit Huessy, kennen; die Trauung war am 29\. Juni 1914 in Leipzig. Im selben Jahr erschien auch, was er sein Professorenbuch nannte: „Königshaus und Stämme in Deutschland zwischen 911 und 1250”. Er warmit 26 Jahren -, wie man so sagt, ein gemachter Mann.

## Erster Weltkrieg

Während des ganzen Ersten Weltkriegs stand er als Offizier im Felde, unter anderem vor Verdun. In diesen Jahren konzipierte er als Schauempfang, wie der Ruf Gottes an die Propheten in der Bibel heißt sein Werk, das erst mit dem Erscheinen des zweiten Teils der Soziologie, „Die Vollzahl der Zeiten”, 1958, also vierzig Jahre später, zum Vorschein gekommen ist. Franz Rosenzweig, mit dem er per Feldpost einen intensiven und entscheidenden Briefwechsel hatte (1935 mit den Briefen Franz Rosenzweigs in Deutschland veröffentlicht), rief ihn aus der Konzeption hinaus in die geschichtliche Stunde, zum Tun.

Das entscheidende Datum für sein Leben und Wirken wurde der 9. November 1918, der Zusammenbruch des Deutschen Kaiserreichs. Das 1920 in dem mit Hans Ehrenberg und Leo Weismantel gegründeten Patmos-Verlag erschienene Werk „Die Hochzeit des Kriegs und der Revolution” drückt das Geschehen in seiner Sprache aus: Während bei den Epochenbrüchen des Zweiten nachchristlichen Jahrtausends Revolution und dazugehöriger Krieg weit auseinandergelegen haben (1789 Sturm auf die Bastille, dann die Napoleonischen Kriege), trafen sie in den Jahren des Weltkriegs 1917 und 1918 zu einer Hochzeit zusammen. Die Russische Revolution wäre ohne den Ersten Weltkrieg nicht zustandegekommen.

## In der Weimarer Republik

Alles weitere Leben müsse aus dieser entscheidenden Erfahrung hervorgehen, so war seine Entscheidung. Und deshalb lehnte er drei wichtige Angebote ab, die ihn an Institutionen berufen wollten, die es vor dem Weltkrieg schon gab:
* an der Verfassung der Weimarer Republik als Unterstaatssekretär mitzuarbeiten,
* zweitens als Redakteur der katholischen Zeitschrift «Hochland» und
* drittens als Professor an der Universität Leipzig.

Staat, Kirche und Universität galten ihm als veraltete Gefäße.

Vielmehr ging er den neuen Weg, zunächst als Gründer der ersten deutschen Werkzeitung bei Daimler in Stuttgart, dann wurde er 1921 bis 1922 Gründer und erster Leiter der Akademie der Arbeit in Frankfurt am Main. 1921 wurde sein Sohn Hans geboren. Er legte sein Amt als Leiter der Akademie der Arbeit nieder, weil die Dozenten seiner andragogisch begründeten Forderung, dass sie auch voneinander lernen sollten, könnten und dürften, nicht nachkommen wollten. Er promovierte (nach einer Zeit als Arbeitsloser) erneut in Darmstadt und habilitierte sich als Privatdozent für Soziologie.

Die Universität Breslau rief ihn 1923 als Rechtsgelehrten und er nahm diesmal an, weil die Not des Tages ihn zwang, in das alte Gefäß der Universität zurückzukehren. 1924 war er Mitbegründer des «Hohenrodter Bundes»- ein Zeichen des Wunsches, über die Grenzen der Universität hinaus zu wirken. Das tat er mehr noch und sichtbar geworden in den Freiwilligen Arbeitslagern für Arbeiter, Bauern und Studenten, die er nach Anregung Helmuth James von Moltkes in Kreisau (heute Kryzowa) gründete und bis 1933 begleitete.

Während der Jahre in Breslau gab und nahm er die Freundschaft mit Joseph Wittig, den er bei seinem Kampf mit der katholischen Kirche und nach der Exkommunikation und Heirat mit Bianca Geißler unterstützte. Dokument dieser Freundschaft ist das dreibändige Werk «Das Alter der Kirche» von 1927, dessen dritter Band die Geschichte der Exkommunikation Wittigs belegt. 1928 starb der Vater. Die neue wissenschaftliche Position, in der er die Rechtsfragen angeht, die durch die Industrialisierung in allen Lebensbereichen neu gestellt sind, verkündet er in vielen Aufsätzen und Büchern, am deutlichsten vielleicht in der Festgabe für Xaver Gretener «Vom Industrierecht. Rechtssystematische Fragen», 1926.

Die erste umfassende Darstellung seines „Schauempfangs” von 1917 war aber dann das Buch «Die Europäischen Revolutionen. Volkscharaktere und Staatenbildung» von 1931\. Es war unter äußerstem innerem Druck entstanden: Es war für ihn die letzte mögliche Stunde, mit der erschauten geschichtlichen Wirklichkeit des Zweiten Jahrtausends nach Christus in Deutschland Gehör zu finden. Denn allzu vertraut war er mit den politischen und gesellschaftlichen Strömungen der Weimarer Republik, besonders mit der Radikalität, mit der die Jugend Änderung und Erneuerung ohne Rücksicht auf die Alten forderte, in welche Richtung auch immer.

## Emigration und Immigration

Schon 1919 hatte erin prophetischer Sprache für das Deutsche Reich einen „Lügenkaiser” vorhergesagt, und so überraschte ihn die Usurpation des Rechtsum es einmal so zu nennen durch Hitler und die Nationalsozialisten nicht. Geistesgegenwärtig sagte er am 1. Februar 1933 seine weitere Teilnahme an der öffentlichen Lehre an der Universität in Breslau ab, erwirkte die förmliche Beurlaubung in Berlin und gewann Zeit zur genauen Vorbereitung der Emigration.

Am 9. November 1933 verließ er in dem Fährschiff mit Namen *Deutschland* das Deutsche Reich. Kurz zuvor entging er nur knapp der Verhaftung, als er bei Viktor Bausch in Berlin zu Besuch war und Parteischergen den „Juden Rosenstock” verlangten. Frau und Kind folgten ihm bald in die Vereinigten Staaten von Amerika.

1934 bis 1936 hatte er den Kuno-Fischer-Lehrstuhl für German Art and Culture an der Harvard University inne. 1935 war er, um an der Briefausgabe Franz Rosenzweigs mitzuwirken, noch einmal in Deutschland. Mit Schaudern sah er, was im Deutschen Reich zerstört wurde. Die Rückkehr nach Amerika wurde für ihn zur Immigration, zur Einwanderung.

## Lehren und Wirken in den USA

In den Lowell Lectures in Boston fasste er seine Einsichten in die Gesetzmäßigkeiten des Lebens in der Industrie zusammen. Sie erschienen später, nämlich 1955, auf deutsch als «Der unbezahlbare Mensch». Von 1936 bis 1957 lehrte er am Dartmouth College in Hanover, New Hampshire. 1937 bezog er das eigene Haus in Norwich, Vermont, das noch heute wichtige Teile seines Nachlasses beherbergt.

Mit liebevoller Hilfe „von drüben” übersetzte er das Revolutionenbuch ins Amerikanische. Es erschien 1938 als «Out of Revolution Autobiography of Western Man»«Der Revolution entsprungen – Autobiographie des abendländischen Menschen». Im Titel kommt die Methode zum Ausdruck: auf das zu hören, was die Völker selber sagen, und nicht ihnen ein wie auch immer erfundenes System überzustülpen. Das Werk bleibt so für vielfältige Beobachtungen offen, durch die der einzelne Leser es ergänzen kann und soll.

Am 16. November 1938 nahm sich die Mutter Paula Rosenstock in Berlin kurz vor der bevorstehenden Verbringung ins Konzentrationslager und in den sicheren Tod das Leben. Ganz wie in Deutschland wirkte Rosenstock-Huessy in Amerika als Hochschullehrer und als Stifter einer neuen Form sozialen Wirkens und Lernens: die Arbeit in den Freiwilligen Arbeitslagern in Schlesien setzte er im Camp William James 1939-1940 fort. 1944 verfasste er im Auftrag der amerikanischen Regierung die Denkschrift «Mad Economics or Polyglot Peace». Sie erörtert die Frage, was nach dem Krieg in Deutschland passieren soll. Auch hier spricht der Titel für das Ganze: kein einheitliches Wirtschaftssystem, weder aus West noch aus Ost, weder aus England noch aus Frankreich stammend, kann den Frieden stiften, sondern nur das Zulassen der verschiedenen Wirtschaftsformen und das ist die polyglotte, die mehrsprachige Erscheinung der Menschheit auf dem Planeten Erde.

## Während des Waffenstillstandes nach 1945

Wie verheerend der Wahnsinn Hitlers wirkte und wie er denn wohl trotz all des Entsetzlichen nicht den Verstand rauben muss, das Geschehen zu durchdringen, schildert die im April 1945 publizierte Schrift «Hitler and Israel, or: On Prayer».

Aus dem Tiefpunkt der Erschöpfung nach dem Zweiten Weltkrieg entstand das Kernstück seiner Lehre „The Christian Future or The Modern Mind Outrun”, zehn Jahre später in Deutschland erschienen als „Des Christen Zukunft oder: Wie überholen wir die Moderne”. Nicht leicht war die Schwelle zu nehmen, Deutschland wieder zu betreten, wie es die Einladenden, die „dageblieben” waren, vielleicht dachten.

Mit dem Namen, mit den er 1925 zum Schutz vor dem Judenhass angenommen hatte und den er nach 1933 auch in seinen Publikationen gebrauchte (sein Sohn hieß nach der Ankunft in Amerika Hans R. Huessy), nahm er diese Schwelle mit der Rede „Das Geheimnis der Universität” am Vortage seines Geburtstages 1950 in Göttingen. Eine Reise nach Ägypten 1950-1951 führte ihm die Belege für das vor Augen, was er im Studium des Ägyptischen Reiches erarbeitet hatte. Bei den Darmstädter Gesprächen „Mensch und Technik” 1952 fand der die Formulierung des Gesetzes der Technik:
* „Der technische Fortschritt erweitert den Raum, verkürzt die Zeit und zerschlägt menschliche Gruppen.”

1956 und 1958 endlich erschienen die beiden Bände, die ihm die Sorge nahmen, sein „Schauempfang” von 1917 könne ungefasst bleiben, nämlich die Soziologie in zwei Bänden:
* I „Die Übermacht der Räume”,
* II „Die Vollzahl der Zeiten”.

Mündliches Ereignis wurde dieses Erscheinen in den Münsteraner Vorlesungen des Sommersemesters 1958: „Die Gesetze der christlichen Zeitrechnung”. Am 30. Januar 1959, 26 Jahre nach der Usurpation des Rechts durch Hitler, hielt Rosenstock-Huessy in der Paulskirche in Frankfurt am Main die Rede „Friedensbedingungen einer Weltwirtschaft”. Sie hat zur Gründung der „Aktion Sühnezeichen” beigetragen.

1959 starb die Frau Margrit Rosenstock-Huessy. Zur weiteren Bekräftigung der Lehre von den antiken Reichen reiste er nach Guatemala, Honduras und Mexiko. 1960 zog Freya von Moltke, die Witwe des Märtyrers Helmuth James von Moltke, mit ihrem Sohn Konrad zu Rosenstock-Huessy nach Four Wells, „Vierquell”, wie er sein Haus genannte hatte. Schönste Frucht dieses Zusammenkommens war die Verwirklichung eines ihm seit 1902 vorschwebenden Vorhabens, das zweibändige Werk „Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik”, erschienen in Heidelberg 1963 und 1964.

Zu seinem 75. Geburtstag am 6. Juli 1963 wurde in Bethel die seither bestehende Eugen Rosenstock-Huessy Gesellschaft gegründet. Noch einmal, 1965, ließ er sich zum Schreiben rufen, um dem am intensivsten geliebten Wirken zum Entstehen eines planetarischen freiwilligen Dienstes Stimme zu verleihen „Dienst auf dem Planeten, Kurzweil und Langeweile im Dritten Jahrtausend”. Vorlesungen führten ihn in den Jahren 1965 bis 1968 nach Santa Cruz an die University of California. Und der Briefwechsel mit Franz Rosenzweig, ein Herzstück ihrer beider Biographie, erschien 1969 in englischer Übersetzung und mit einer aktuellen Einleitung.

## Eiptaph

Als Epitaph auf sein Leben, Lehren und Wirken schrieb er:

I tried to speak law, prayer, tale and song;\
discovered logic's leak, its bend on wrong.\
I tried to act and suffer and accept\
and to succeed, to rule, and most, to teach;\
but since my heart sincerely laughed and wept,\
all that gives power, stayed outside my reach.\
I tried to love, not always knowing how;\
I never bluffed, and I discovered "thou".

Versucht hab ich zu sprechen Recht, Gebet, Erzählung, Lied;\
entdeckte das Leck der Logik, ihre Fehlerneigung.\
Versucht habe ich zu handeln, zu leiden, anzunehmen\
und Gelingen, zu herrschen und, noch am meisten, zu lehren;\
da aber mein Herz in Lauterkeit lachte und weinte,\
blieb, was Macht verleiht, außerhalb meiner Reichweite.\
Versucht hab ich zu lieben, wußte nicht immer wie;\
hab niemals jemandem was vorgemacht, und: entdeckte das „Du”.

Aber auf dem Grabstein steht bei dem Namen dann nur: Johannes 1, Vers 14 mit den Daten 6. Juli 1888 und 24. Februar 1973.

Eckart Wilkens\
Köln, 23. April 2007

(unter Verwendung des tabellarischen Lebenslaufs, den Lise van der Molen am 27. April 1994 in Winsum geschrieben hat)

[zum Seitenbeginn](#top)
