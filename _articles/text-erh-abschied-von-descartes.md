---
title: "Eugen Rosenstock-Huessy: Abschied von Descartes"
category: bearbeitung
created: 1970
published: 2024-07-07
---

Die ersten beiden Teile dieses Stücks aus dem Band I am an Impure Thinker von 1970 sind ein Kapitel aus dem Hauptwerk Out of Revolution, 1938, konzipiert 1917, auf deutsch in der ersten Ausgabe 1931, in der zweiten 1951 erschienen, wo aber dieses Kapitel nicht zu finden ist.

...

Die Passage:\
Aber sage mir, daß du gewillt bist, dein Leben als Satz in der Autobiographie des Menschengeschlechts zu erfahren, sag mir, wie weit du Verantwortung mit den Missetätern der Vergangenheit teilst und wann du gezeigt hast, wieweit du dich mit der übrigen Menschheit identifizieren kannst - und dann weiß ich, ob dein Wissen Überlebenswissen ist, „Metanomik“ der Gesellschaft als Ganzer - oder ob es doch nur deine private Metaphysik ist.
(sie lautet in der deutschen Übersetzung im Geheimnis der Universität S. 111 und der autoritäre Ton ist da nicht zu überhören, das Überspringen der Forderung an die Leser von 1958, mit den Missetaten der Jahre 1933 bis 1945 umzugehen:

*Sage mir indessen, daß du willens bist, dein Leben zu erfahren als einen Satz in der Autobiographie der Menschheit; sage mir, wie weit du die Verantwortung teilst mit den fehlerhaften Menschen früherer Zeiten! Wenn du mir gezeigt hast, bis zu welchem Grade du dich mit der übrigen Menschheit identifizieren kannst, dann werde ich wissen, ob dein Wissen überdauerndes Wissen, Metanomik der Gesellschaft als Ganzes, oder nur deine Privat-Metaphysik ist.)*

bietet dem Leser einen Maßstab für künftige Lektüre – nicht nur von Essays, sondern aller Formen der Expression, des lebendigen Ausdrucks. Was hält diesem Maßstab stand?

*Köln, 20. November 2019 \
Eckart Wilkens*

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Abschied_von_Descartes.pdf' | relative_url }})

[Der Text ist eine Übersetzung eines Teils aus „I am an impure thinker (1970)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/588.pdf)
