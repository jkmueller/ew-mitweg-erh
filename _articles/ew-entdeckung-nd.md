---
title: Entdeckung über Das Neue Denken
category: methodik
created: 2018-08
---


Ich habe inzwischen Eugen Rosenstock-Huessys Soziologie in zwei Bänden 1956/1958, Franz Rosenzweigs Stern der Erlösung 1920, Rudolf Ehrenbergs Metabiologie 1950 und Ernst Michels Buch Ehe 1948 neu gliedernd nach der grammatischen Methode bearbeitet und dabei ist mir aufgegangen, wie die Lehre vom Kreuz der Wirklichkeit in diesen Sprechern, die alle in der Zeitschrift "Die Kreatur" versammelt waren, bezeugt ist. Dabei läßt sich sehen, daß die Lehre präjektiv von Rosenstock-Huessy, subjektiv von Ernst Michel (sein Ehebuch geht in die Nähe, die alle angeht), trajektiv von Rosenzweig, objektiv von Rudolf Ehrenberg vertreten wird. Ja, man kann sagen, daß das Hauptwerk Rosenstock-Huessys in deutscher Sprache nur erscheinen konnte, weil Ernst Michel und Rudolf Ehrenberg durch die Nazi-Zeit hindurch die Treue gehalten und Eugen Rosenstock-Huessy die Rückkehr nach Deutschland ermöglicht haben. Also nicht nur der Inhalt dieser Werke geht uns an, sondern die Sprechweise, die die Umkehr des Wortes auf den Sprecher zum Motor des Sprechens und Darlegens hat.

Wer auf sein Wort hört, gelangt ins Innere, wer weiterhört, tritt in die Geschichte ein, wer wiederum weiterhört, bringt eine mitteilbare Frucht. Wer nicht auf sein Wort selber hört, für den gilt, was von dem auf den Weg gefallenen Samen gesagt ist: der Böse kommt und raubt, was in sein Herz gesät ist. Wer in dem Inneren steckenbleibt (also auch der gesamten Welt des Reflexivums mit Kunst und Wissenschaft), von dem heißt es, daß das Wort verdorrt, weil es keine Wurzel in der Wirklichkeit hat. Wer nur in der Geschichte lebt und denkt, da sei nun alles erledigt, für den gilt, daß Sorgen und die Blendung des Reichtums das Wort ersticken. Wer diese Prüfungen des Hörens auf das Wort besteht, bringt Frucht. In der Reihenfolge des Erscheinens: Rosenzweig, Michel, Rudolf Ehrenberg, Rosenstock-Huessy von 1920 bis 1958 ist also der Auszug aus der Nationalepoche für uns alle nachzugehen.

aus: [ERHG Mitgliederbrief 2018-08](https://www.rosenstock-huessy.com/erhg-2018-08-mitgliederbrief/)
