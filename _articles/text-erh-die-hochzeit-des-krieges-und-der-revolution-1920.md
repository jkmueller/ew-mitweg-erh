---
title: "Eugen Rosenstock-Huessy: Die Hochzeit des Krieges und der Revolution (1920)"
category: bearbeitung
created: 1920
published: 2024-05-10
---
Eugen Rosenstock-Huessy

### Die Hochzeit des Krieges und der Revolution

Würzburg 1920

*abgeschrieben und neu gegliedert von \
Eckart Wilkens*


[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_die_hochzeit_des_krieges_und_der_revolution.pdf' | relative_url }})

[Das Orginal von „Die Hochzeit des Krieges und der Revolution” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/083.pdf)
