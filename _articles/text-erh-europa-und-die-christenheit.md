---
title: "Eugen Rosenstock-Huessy: Europa und die Christenheit (1919)"
category: bearbeitung
created: 1919
published: 2022-09-27
---

In der Abschrift gegliedert, mit Inhalt, Namensregister, Liste der Merksätzen und einer erklärenden Notiz\
von Eckart Wilkens

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Europa_und_die _Christenheit.pdf' | relative_url }})

[Das Orginal von „Europa und die Christenheit” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/060.pdf)
