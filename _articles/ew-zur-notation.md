---
title: Zur Notation
category: methodik
created: 2023-09
published: 2023-09-11
---
### ZUR NOTATION DER WERKE EUGEN ROSENSTOCK-HUESSYS
oder:\
Warum aus der Differenz zwischen Hören und Lesen der Texte Eugen Rosenstock-Huessys Verwirrung entsteht
geschrieben auf Anforderung des Vorstands der Eugen Rosenstock-Huessy Gesellschaft auf der Vorstandssitzung 31. Mai bis 2. Juni 2013, Andreas Schreck, Jürgen Müller, Thomas Dreessen.

Die Schriften Eugen Rosenstock-Huessys sind vielmals auf das Urteil getroffen, sie wären zu sprunghaft, überforderten den Leser, überschritten die Grenze des für einen geschriebenen Text Ziemlichen, indem plötzlich Persönliches vorkommt, das den Leser zur Stellungnahme zwingt.

Was ist das Geheimnis dieses Stils?

#### Im zweiten Teil
fügt Eckart die Bearbeitung von

***Eugen Rosenstock-Huessy \
Hitler und Israel, oder: Vom Gebet\
1944***

als Beispiel hinzu.

[Eckart Wilkens: Zur Notation]({{ 'assets/downloads/wilkens_erh_Zur_Notation.pdf' | relative_url }})
