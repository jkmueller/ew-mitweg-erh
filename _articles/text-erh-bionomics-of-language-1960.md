---
title: "Eugen Rosenstock-Huessy: The Bionomics of Language (1962)"
category: lectures
created: 1962
published: 2023-09-23
---
### NOTE OF THE EDITOR

This is the edited transcription of six lectures of June 1962 – Rosenstock-Huessy being nearly seventy-four years old – in St. Augustine-by-the-sea. It is the most proud exemplification of living speech coming from the heart – all of which is spoken out of the heart, “no manuscript”.

The transcription was made by Frances Huessy – my work are the following changes:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.

The titles of the lectures (and even more the titles of the chapters) reveal the majestic plan of the whole:
- First lecture: Numbers, Words, Names
- Second lecture: The four paths into reality
- Third lecture: The cross of reality
- Fourth lecture: The blind spot of science
- Fifth lecture: Qualitative distinctions of time
- Sixth lecture: The highest and the medium powers

as if the six days of the creation are transformed into the real meeting of listening and speaking during those ten days in June 1962, in California.

The seriousness is expressed in that the speech makes even these ten days to a whole One, which is the victory of times against spaces.

It should be noted, that this living speech as example for what is told and required perhaps is more important than the books of Rosenstock-Huessy in which many of the issues, the cross of reality is laid down in English or in German.

Take care of this precious and astonishing soul with the whole life-time 1888 to 1973 resounding. 2, Kor. 12, 9.

*Easter 2017, April 17,\
Cologne \
Eckart Wilkens*

- [Eckart's Edition: ERH The Bionomics of Language (1962)]( {{ 'assets/downloads/wilkens_erh_The_Bionomics_of_language_1962.pdf' | relative_url }})

- [The transcript of „ERH The Bionomics of Language (1962)” as PDF](https://www.erhfund.org/wp-content/uploads/650.pdf)
- [The audio recordings of „ERH The Bionomics of Language (1962)”](https://www.erhfund.org/lecturealbum/volume-25-st-augustine-by-the-sea-1962/)
