---
title: Fazit einer Amerika-Reise
category: vertiefung
created: 2019-09
---

Von dem Wiedersehen des Hauses Eugen und Margrit Rosenstock-Huessys in Norwich/ Vermont. „Four Wells” und dem Eindruck von dem Besuch ihrer Gräber auf dem Friedhof, wo auch Freya und Konrad von Moltke sowie Hans Rosenstock Huessy begraben sind – zu- letzt war ich dort im Jahre 1988, als die große Jahrhundertfeier stattfand, wo ich in dem Klassenzimmer, wo Eugen unterrichtet hat, davon gesprochen habe, was die vier Auferstehungsberichte der vier Evangelisten unterscheidet – möchte ich gern die beiden Gedichte mitteilen, die den Eindruck dort gleich aussprachen:

Das Grab Margrit und Eugen Rosenstock-\
Huessys – die Hügelzüge lassen\
die Blicke nach wie vor schweifen –\
sagt: die Auferstehung in wandelnd\
bleibender Erscheinung ist in\
*Palästina, Schlesien und anderswo*\
geschehen, die Gewißheit Jakobus\
1, Vers 25 steht wohl beschattet im Licht.

*auf dem Friedhof in Norwich/Vermont*\
*Joseph Wittig, Leben Jesu in Palästina, Schlesien und anderswo, 1926*\
*Spruch auf Margrits Grab*

Das heißt: es war mir, als spräche eine Stimme: „Nicht hier – sondern er ist auferstanden”, wir können uns nicht darauf verlassen, daß es da nun ist und als Kultort bleibt, sondern sollen das Wort des Paulus beherzigen (Röm. 10, 6-8): **6** Aber die Gerechtigkeit aus dem Glauben spricht so (5.Mose 30,11-14): »Sprich nicht in deinem Herzen: Wer will hinauf gen Himmel fahren?« – nämlich um Christus herabzuholen –, **7** oder: »Wer will hinab in die Tiefe fahren?« – nämlich um Christus von den Toten heraufzuholen –, **8** sondern was sagt sie? »Das Wort ist dir nahe, in deinem Munde und in deinem Herzen.« Dies ist das Wort vom Glauben, das wir predigen.

Dann das zweite Gedicht:

Wo alles geschehen ist, da ist nun\
Stille, die Regungen, von denen\
die Nachbarn keine Ahnung gehabt,\
die Kollegen nicht, fragen ganz\
ohne Aufwand: Hast Du darauf\
geachtet, wie sich Trotz und Liebe\
vermählt haben, lebendig und\
über die Maßen im Wort „herberget gern”?

*bei dem Haus, das Eugen und Margrit Rosenstock-Huessy gebaut haben,
Four Wells, Norwich, Vermont*\
*Römer 12, 13*

Da schimmert also der Dank an Freya von Moltke, deren Gastlichkeit wir, Sigrid und ich mit den Kindern Julia, Caroline, David, Niklas, Hanna zusammen genossen haben, später dann auch Simon (aber da war ich nicht mit dabei). Ich war wirklich ganz frei und die förderlichen Absichten, die Freya gewiß hegte, drängten mich nicht – und auch nicht mehr, als die Besuche aussetzten, weil die Freundschaft mit Konrad und Ulrike von Moltke aussetzte.
Inzwischen weiß ich: Four Wells war zwar das Amerika der Zukunft, aber das Amerika von damals hatte so gut wie keine Ahnung davon. Das wußte ich damals nicht, als ich die Sonne in Vermont als Ausdruck planetarischer Herzlichkeit erlebte.
Auch da also der Verweis auf die eigene Existenz, wo auch immer sie ist.


aus: [ERHG Mitgliederbrief 2019-09](https://www.rosenstock-huessy.com/erhg-2019-09-mitgliederbrief/)
