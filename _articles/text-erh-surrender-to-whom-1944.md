---
title: "Eugen Rosenstock-Huessy: Surrender to Whom? (1944)"
category: bearbeitung
created: 1944
published: 2023-03-15
---
Surrender to Whom?

with a letter of Arnold Wolfers \
January 30th 1945

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Surrender_to_whom_1944.pdf' | relative_url }})

[Das Orginal von „Surrender to Whom? 1944” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/386.pdf)
