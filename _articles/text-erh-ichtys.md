---
title: "Eugen Rosenstock-Huessy: ICHTHYS Leben – Lehre – Wirken (1923)"
category: bearbeitung
created: 1923
published: 2024-12-14
---

**EUGEN ROSENSTOCK-HUESSY**\
**ICHTHYS**\
**LEBEN – LEHRE – WIRKEN**

### NOTIZ

1

Am 9. März 1923, einen Tag vor Margrit Rosenstock-Huessys dreißigstem Geburtstag, schrieb Franz Rosenzweig an Martin Buber:

*Ich sprach einmal vor einem Jahr mit Strauss von Ihnen. Da sagte er: „Er kann das Wort
Religion nicht entbehren.“ Worauf ich: „Damit ist er gestraft.“*

*Hier können Sie es aber entbehren. Man kann es immer entbehren, wenn man einen
„angerufenen“ Namen nennt, was Sie ja in diesem Absatz tun. Wer seine Liebe bei Namen
nennt, der braucht das Wort „Geliebte“ nicht mehr.*

Lise van der Molen merkt zu *Leben, Lehre und Wirken* an: *Eine Auslegung der letzten
beiden Worte des Sterns der Erlösung. 10. März 1923.* Dann 1928 im *Alter der Kirche*
veröffentlicht, weiter in der *Sprache des Menschengeschlechts* 1963 und der
*Umwandlung* 1968.

2

Das Jahr 1922 bedeutete eine jähe Wendung im Leben der drei durch Bekehrungs-
und Liebeserfahrung verbundenen drei: Eugen und Margrit Rosenstock-Huessy und
Franz Rosenzweig. Eugen legte am 22. Februar 1922 das Amt als Leiter der
Akademie der Arbeit nieder, wurde stellungslos. Bei Franz Rosenzweig zeigt sich im
Januar 1922 die Erkrankung an Amyotropher Lateralsklerose, die es ihm bald nicht
mehr möglich machte, das Freie Jüdische Lehrhaus in Frankfurt zu leiten.
Rosenstock-Huessy wurde dann am 28. Juni 1923 an die Universität Breslau berufen
– und damit endete das leichte-schwere Hin-und-her zwischen Margrit Rosenzweig
und Franz Rosenzweig – und in seinen Briefen an „Gritli“ sind die Spuren der
kommenden Entfremdung zu lesen.

Wie wichtig aber die gemeinsame Erfahrung der Zeit davor war, bezeugte Rosenstock-Huessy in dem Gedicht, das er Hertha Vogelstein in ihr Exemplar von *„Dienst auf dem Planeten“* mehr als vierzig Jahre später schrieb:

*„Lehrhaus“ – „Akad´mie der Arbeit“:\
o versucht nicht, sie zu trennen\
Augenblicke – Ewigkeiten\
zueinander zu bekennen\
ist des Wortewesens Sinn.*

*Nimm dies Büchlein freundlich hin.*

*Für Hertha Vogelstein\
21.XI. 1965 Eugen Rosenstock-Huessy*


3

Was bedeutet es da, daß er seiner Frau, die mit der Trennung von Franz Rosenzweigs Nähe umzugehen hat, diese Schrift „Leben, Lehre, Wirken“ schreibt? Will er ihr Eindruck machen, stellungslos, der er ist, geschäftig und umtriebig wie eh und je? Wie nähert er sich ihr? Und von ihr ist in dem ganzen Stück kein Wort. Das heißt dann doch in der Bemerkung: *Eine Auslegung der letzten beiden Worte des Sterns der Erlösung.* Die lauten: *INS LEBEN.*

Angehoben hatte der „Stern der Erlösung“ mit dem wuchtigen Satz:

*Vom Tode, von der Furcht des Todes hebt alles Erkennen des All an.*

Und wie streng Franz Rosenzweig in diesem Buch sein „System“ baute, zeigen die Überschriften über den drei Teilen:

*ERSTER TEIL:\
DIE ELEMENTE oder: DIE IMMERWÄHRENDE VORWELT\
als Zeichen: das aufrechtstehende gleichseitige Dreieck*

*ZWEITER TEIL:\
DIE BAHN oder: DIE ALLZEITERNEUERTE WELT\
als Zeichen: das mit der Spitze nach unten weisende gleichseitige Dreieck*

*DRITTER TEIL:\
DIE GESTALT oder: DIE EWIGE ÜBERWELT\
als Zeichen: der aus den beiden gleichseitigen Dreiecken zusammengesetzte Davidsstern*

Und am Ende des Buches, geschrieben 1917 bis 1918, Margrit Rosenstock-Huessy in
Feldpostbriefen aus Mazedonien mitgeteilt, löst sich das strenge Zeichen der
Überwelt in das Antlitz:

*Gleich wie der Stern in den zwei übereinandergelegten Dreiecken seine Elemente und die Zusammenfassung der Elemente zur einen Bahn spiegelt, so verteilen sich auch die Organe des Antlitzes in zwei Schichten.*

*Denn die Lebenspunkte des Antlitzes sind ja die, wo es mit der Umwelt in Verbindung tritt, seis in empfangende, seis in wirkende.*

*Nach den aufnehmenden Organen ist die Grundschicht geordnet, die Bausteine gewissermaßen, aus denen sich das Gesicht, die Maske, zusammensetzt: Stirn und Wangen. Den Wangen gehören die Ohren, der Stirn die Nase zu. Ohren und Nase sind die Organe des reinen Aufnehmens. Die Nase gehört zur Stirn, sie tritt in der heiligen Sprache gradezu für das Gesicht im ganzen ein. Der Duft der Opfer wendet sich an sie wie das Regen der Lippen an die Ohren.*

*Über dieses erste elementare Dreieck, wie es gebildet wird von dem Mittelpunkt der Stirn als dem beherrschenden Punkt des ganzen Gesichts und den Mittelpunkten der Wangen, legt sich nun ein zweites Dreieck, das sich aus den Organen zusammenfügt, deren Spiel die starre Maske des ersten belebt: Augen und Mund. Die Augen sind unter sich nicht etwa mimisch gleichwertig, sondern während das linke mehr empfänglich und gleichmäßig schaut, blickt das rechte scharf auf einen Punkte eingestellt; nur das rechte „blitzt“, - eine Arbeitsteilung, die ihre Spuren schließlich bei Greisenköpfen häufig auch in die weiche Umgebung der Augenhöhle eingräbt, so daß dann jene ungleichmäßige Gesichtbildung auch von vorn wahrnehmbar wird, die sonst allgemein nur an der bekannten Verschiedenheit der beiden Profile auffällt.*

*Wie von der Stirn der Bau des Gesichts beherrscht wird, so sammelt endlich sein Leben, alles was um die Augen zieht und aus den Augen strahlt, sich im Mund. Der Mund ist der Vollender und Vollbringer allen Ausdrucks, dessen das Antlitz fähig ist, so in der Rede wie zuletzt im Schweigen, hinter dem die Rede zurücksank: im Kuß.*

*Die Augen sinds, in denen das ewige Antlitz dem Menschen leuchtet, der Mund, von dessen Worten der Mensch lebt; aber unserm Lehrer Mose, der das Land der Sehnsucht lebend nur schauen, nicht betreten durfte, versiegelte er dies abgeschlossene Leben mit einem Kusse seines Mundes.*

*So siegelt Gott und so siegelt der Mensch auch.*

*Im innersten Heiligtum der göttlichen Wahrheit, wo ihm seiner Erwartung nach alle Welt und er selber sich zum Gleichnis herabsinken müßte für das, was er dort erblicken wird, erblickt so der Mensch nichts andres als ein Antlitz gleich dem eignen.*

*DER STERN DER ERLÖSUNG IST ANTLITZ WORDEN, DAS AUF MICH BLICKT UND AUS DEM ICH BLICKE.*

Das heißt: Franz Rosenzweig verläßt sein „System“! In der Begegnung mit Margrit
Rosenstock-Huessy hat sich sein ganzes philosophisch geprägtes „Vorleben“
aufgelöst!

4

Und diese Erfahrung, wie könnte es anders sein, ist auch auf dem Antlitz der Frau, die er liebt, zu schauen – wer es nur wagt, und sei es der Ehemann!

Die Geburtstagsgabe am Ende der Lebensepoche, wo ihr dieser Antlitzglanz gewohnte Erfahrung war, schildert also nichts anderes als die Auflösung des „Systems“ in das lebendige Antlitz! Es ist von Franz Rosenzweig die Rede, dessen Leben nun den Dreitakt von Leben (philosophisches Leben, Zeugnis das Buch: *Hegel und der Staat*), Lehre (das Buch *Der Stern der Erlösung*) und Wirken (das *Freie Jüdische Lehrhaus* in Frankfurt und das Wirken in den schier unzähligen Briefen bis zu seinem
Tode am 10. Dezember 1929)!

5

Die möglichst geräuschlos geöffnete Tür, von der Rosenstock-Huessy spricht, ist das
Wörtchen „wieder“ in dem Satz:

*Es ist dies berufene Wirken kein „einfältig wandeln mit Deinem Gotte“[^1], sondern ein wieder einfältig wandeln; es ist nicht mehr die eigene Antwort an Gott allein, sondern zugleich die Verantwortung für Gott vor den Menschen, ein Ding, wovon das bloße Leben in uns nichts weiß noch zu wissen braucht.*

Auch in der Schlußpartie des Sterns der Erlösung wird ja der im jüdischen Leben
grundlegende Vers Micha 6, 8 genannt:

*Und dies Letzte ist nichts Letztes, sondern ein allzeit Nahes, das Nächste; nicht das Letzte also, sondern das Erste. Wie schwer ist solch Erstes! Wie schwer ist aller Anfang! Recht tun und von Herzen gut sein – das sieht noch aus wie Ziel. Vor jedem Ziel kann der Wille noch erst ein wenig verschnaufen zu müssen behaupten.*

*Aber einfältig wandeln mit deinem Gott – das ist kein Ziel mehr, das ist so unbedingt, so frei von jeder Bedingung, von jedem Erst-noch und Übermorgen, so ganz Heute und also ganz ewig wie Leben und Weg. Einfältig wandeln mit deinem Gott – nichts weiter wird da gefordert als ein ganz gegenwärtiges Vertrauen.*

*Aber Vertrauen ist ein großes Wort. Es ist der Same, daraus Glaube, Hoffnung und Liebe wachsen, und die Frucht, die aus ihnen reift. Es wagt jeden Augenblick zur Wahrheit Wahrlich zu sagen.*

*Einfältig wandeln mit deinem Gott – die Worte stehen über dem Tor, dem Tor, das aus dem geheimnisvoll-wunderbaren Leuchten des göttlichen Heiligtums, darin kein Mensch leben bleiben kann, herausführt.*

*Wohinaus aber öffnen sich die Flügel des Tors? Du weißt es nicht? INS LEBEN.*

Das ganze Stück *„Leben, Lehre, Wirken“* ist also geschrieben, um Margrit, die am
Schluß des Sterns der Erlösung Angeredete, das zurechtzurücken:

Franz Rosenzweig ist eben vor dem Nachtgespräch 1912 mit Eugen Rosenstock-Huessy und Rudolf Ehrenberg, das ihn „zum lebendigen Gott“ rief, nicht „einfältig gewandelt mit deinem Gott“!

Und deshalb war die die folgenden Jahre währende „Bekehrung“ keine Rückkehr, sondern das in dem Stück „Leben, Lehre, Wirken“ beschriebene Übertreten in eine neue Lebensordnung, aus Vorleben und Schau in das Wirken.

[^1]: Micha 6, 8

6

Immer wenn wir einen Mitmenschen auf eine Eigenschaft festlegen, von der wir meinen, sie genau zu kennen, tun wir ihm Unrecht. Denn die wesentliche Kraft der Seele offenbart sich doch nur in jenem „einfältig Wandeln“.

Und so tut der Geburtstagsgruß an die Frau, die von Franz Rosenzweig mit Namen genannt wird (*Wer seine Liebe bei Namen nennt, der braucht das Wort „Geliebte“ nicht mehr.*) nichts anderes, als für die Begegnung mit einem anderen Menschen alles freizuräumen, was als Vorurteil wirken muß (und Vorurteile, so verkehrt sie sein mögen, gehören zur Ökonomie des menschlichen Lebens: wir haben einfach nicht die Kraft, den vielen, vielen Menschen, die uns begegnen, mit jener Offenheit das
Antlitz leuchten zu lassen).

Und – das ist nun das Überraschende, was in dem Brief an Martin Buber erscheint – dazu gehört auch das Vorurteil: *jüdisch, christlich, deutsch, konservativ* usw.! Sondern Franz Rosenzweig öffnet die Aufmerksamkeit für die Begegnung mit dem lebendigen Antlitz!

Und diese setzt alle Zugehörigkeiten, die natürlich bestehen, weil sie uns mitgeteilt sind und Halt geben, an zweite Stelle (Buber als damit *gestraft*, immer das Wort *Religion* im Sinn haben zu müssen).

Die Begegnung mit dem Antlitz ist die Überwindung jeder Form des Hasses, ob nun Juden-, Christen-, Kommunistenhaß, Haß auf die Ungläubigen jeglicher Form!

7

Für die Christen leistet Eugen Rosenstock-Huessy den Zwillingsdienst – zu dem Franz Rosenzweigs, daß er den Davidsstern, mit all dem, das er bedeuten mag, zu der lebendigen Erfahrung liebenden Anblickens erlöst -, daß er den Anblick des Kreuzes und des Gekreuzigten übersetzt hat in die Erfahrungsformen des Kreuzes der Wirklichkeit.

Und auch diese Übersetzung ist in keiner Weise auf eine Gruppe des Menschengeschlechts zu beschränken!

*Vom Anfang, von der Mitte, vom Ende her* zu erleben – das sind Erfahrungsformen, die jedem Menschen beschieden sind.

8

So ist dieser Geburtstagsgruß an Margrit eine Urkunde jenes „wieder“, das jedem offensteht, der dem Fakten-Zwang naturwissenschaftlicher Prägung entkommen will und muß. Beieinander stehen sie, diese beiden unerschöpflichen Leistungen:

*Franz Rosenzweigs Erlösung des liebenden Anblickens,*\
*Eugen Rosenstock-Huessys Erlösung von dem Bilderzwang des Zweiten Jahrtausends nach Christus.*

9

Die Gliederung des Textes folgt dem Gehör, nicht dem lesenden Auge: wenn der Ton wechselt, ist das zu sehen wie bei einer Partitur eines Musikstücks.

Inhaltsverzeichnis, Namensverzeichnis (daß es so wenige Namen sind, zeugt von der Intimität des Textes), die Geschichten von Albert Schweitzer 1913 und Karl Barth 1918, die ausgewählten Merksprüche geben dem Leser Anhalt, in welchem Rahmen sich diese Schrift bewegt, die für wert befunden wurde, in der letzten Publikation *Die Umwandlung des Wortes Gottes in die Sprache des Menschengeschlechts*, Heidelbergn1968 an zweiter, also der Stelle der Offenbarung zu stehen:

*Im Kreuz der Wirklichkeit* (Soziologie II, S. 759-760, am Schluß erweitert)\
*Ichthys* (Die Sprache des Menschengeschlechts I, S. 119-142)\
*Die Frucht der Lippen* (Die Sprache des Menschengeschlechts II, S. 797-903).

*Köln, 9. Mai 2020*\
*Eckart Wilkens*


[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Ichthys.pdf' | relative_url }})

["ICHTHYS Leben – Lehre – Wirken" als Teil des PDF-Scan](https://www.erhfund.org/wp-content/uploads/198-1.pdf)
