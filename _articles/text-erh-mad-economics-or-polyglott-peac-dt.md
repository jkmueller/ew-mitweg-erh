---
title: "Eugen Rosenstock-Huessy: Mad Economics or Polyglott Peace (1944)"
category: bearbeitung
created: 1944
published: 2024-02-17
---
EUGEN ROSENSTOCK-HUESSY

### VERRÜCKT GEWORDENE ÖKONOMIE ODER: MEHRSPRACHIGER FRIEDE

*deutsch von Eckart Wilkens*

---

#### NOTIZ VOM 24. FEBRUAR 2003

Inzwischen, seit dem Oktober 1984, sind mehr als fünfzehn Jahre vergangen: vierzehn
Jahre später wurde endlich die Regierung Helmut Kohl abgelöst, die von den Ideen Ernst
Jüngers durchdrungen war, ohne wahrscheinlich den Ursprung noch groß zu wissen.

Die Irak-Krise hat ernstlich die Frage, wie Deutschland sich zu der eigenen
Kriegserfahrung verhält, auf den Plan gebracht, und also auch den Ärger, eine Sonderrolle
spielen zu wollen, reichlich erregt. Andererseits: die ökonomischen Probleme sind in dem
Teil Bundesrepublik West plus Neue Bundesländer fast nicht zu lösen ohne die wirkliche
Beteiligung der anderen Mächte, England, Frankreich, Rußland, Amerika.

Und statt daß Deutschland die Probleme der Kultur, der Tradition wirklich frei angehen
könnte, sind diese auf die Länder verlagert. Die ökonomische Krise findet statt vor Ort, in
den Kommunen, die die sozialen Lasten zu tragen haben. Und die Regierung hat für das
Ankurbeln der Wirtschaft zwar Möglichkeiten – aber keine Befehlsgewalt.

Ob die europäischen Lösungen, wie sie jetzt in Vorbereitung sind, die Bedürfnisse
übereinanderbringen werden, die militärisch-geographischen, die politisch-moralischen,
die ökonomisch-sozialen?

Auf jeden Fall gelangt man mit dem Memorandum auf die Höhe der Zeit, weil gestaltbar
erscheint, was später als Fatum herniedersaust auf die Erstklässler (bei mir Ostern 1949).

Es folgt der Text meiner Übersetzung von 1984.

---

[Eckarts Kommentar und Übersetzung]( {{ 'assets/downloads/wilkens_zu_erh_mad_economics_or_polyglott_peace.pdf' | relative_url }})\
Die Übersetzung von Mad Economics ist auf den Seiten 33 - 75 zu finden.

---

#### NOTIZ VOM 27. FEBRUAR 2003

Selbst beim ersten Wiederlesen traute ich meiner Übersetzung nicht – und dann erwies es
sich, daß das Amerikanische wirklich ins Deutsche übersetzt ist so, daß es immer noch
Amerikanisch ist. Und man dieses Amerikanische annehmen kann – wenn eine gewisse
Verlangsamung des Lesens stattfindet.

Deshalb habe ich jetzt auch die durch die Ziffern gekennzeichneten Absätze Rosenstock-
Huessys noch unterteilt, nämlich immer dann, wenn der Gedankenzug die Richtung
wechselt.

Der Ernst des Gesagten wird in diesen Tagen durch die Irak-Krise erneut aufgezwungen.
Und selbst die Zerstörung der nach 1950, 1965, 1975, 1980, 1991 aufgebauten Strukturen
der Volkshochschule Köln lese ich nun in diesem Lichte: Alles was selber in sich die
Begriffe von Macht und Nation noch umtreibt, kann sich nicht öffnen zu dem, was die
Arbeitenden sagen, empfinden, möchten.

Daß jetzt der Musikmarkt Schwierigkeiten hat, zeigt dasselbe Phänomen, daß die Ost-
West-Spannung nachgelassen hat und the American Way of Life nicht mehr einfach
kopiert werden kann: er birgt die Extreme nicht mehr.


[Das Orginal von „Mad Economics or Polyglott Peace (1944)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/384.pdf)
