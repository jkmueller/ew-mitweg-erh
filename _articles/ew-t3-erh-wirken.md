---
title: "Vorstellung Rosenstock-Huessys: Teil 3 – Wirken"
category: hinfuehrung
order: 102
---

## Der Name

Der Name Eugen Rosenstock-Huessy erfreut sich nur in wenigen Kreisen einer Bekanntheit. Und doch hat er ein Wirken gehabt, das Gedächtnis gestiftet hat.

Einige kennen und nennen ihn als Rosenstock, Insider in Amerika sprechen von ihm als Eugen.

Es gibt seit 44 Jahren die Eugen Rosenstock-Huessy Gesellschaft, gegründet in Bethel. In Haarlem gibt es das Eugen Rosenstock-Huessy Huis. Ein sich zu seiner Lehre scharender Verein in den Niederlanden nennt sich Respondeo (nach seinem Motto für die dritte Form der abendländischen Hochschule: Respondeo utsi mutabor – Ich antworte um den Preis der Verwandlung). Als Verkörperer seiner Lehre sind Bas Leenman, Ko Vos und Konrad von Moltke schon nicht mehr unter uns Lebenden.

Als Freund und Korrespondent Franz Rosenzweigs taucht er auf.

Einige seiner Werke sind auch nach seinem Tode wieder gedruckt oder ins Englische, Niederländische oder Russische übersetzt worden. Gerade wird Des Christen Zukunft ins Rumänische übersetzt.

## Wirkungsfelder in Vergangenheit und Zukunft

Und trotzdem: Jeder, der von ihm spricht, sieht sich genötigt, ihn wieder vorzustellen, und zwar auch und gerade in den Wirkungsfeldern, die er selber mit erschaffen hat:
* in der Industrie, wo er mit der Idee einer Werkzeitung bei Daimler das Verhältnis zwischen Arbeitgeber und Arbeitnehmer auf ausgesprochene Gegenseitigkeit zu bringen gehofft,
* bei der Akademie der Arbeit und gewerkschaftlichen Veranstaltungen,
* in der ökumenischen Arbeit, die in dem mit Joseph Wittig zusammen gestalteten Werk „Das Alter der Kirche” 1927/1928 (neu 1998) zum Ausdruck kommt,
* bei den internationalen Friedensdiensten, denen erin den schlesischen Freiwilligen Arbeitslagern für Arbeiter, Bauern und Studenten 1927-1933 und im Camp William James in Vermont 1939-1940 Pate gestanden hat,
* bei denen, die dem Wirken des deutschen Widerstands gegen Hitler nachgehen: Walter Hammer hat ihn den Erzvater des Kreisauer Kreises genannt,
* in der Europäischen Union, deren Heraufkunft ohne seine Verheißung in dem Buch über die Europäischen Revolutionen (1931, 1938 und 1951) gar nicht denkbar geworden wäre,
* bei der amerikanischen Besatzungsmacht, wo er durch Freunde die Möglichkeit hatte, die Weltkriegserfahrung aus dem Ersten Weltkrieg in die Urteilsbildung über das Deutsche Reich einzubringen,
* in der Erwachsenenbildung in West und Ost, die auf den Formen fußt, die er und seine Freunde vor der Hitler-Zeit geschaffen hatten,
* in den Evangelischen Akademien, die durch seine feurigen Beiträge Kredit gewonnen hatten,
* bei den Verlagen u.a. Kohlhammer, Käthe Vogt, Lambert Schneider (Lothar Stiehm), die wohl meinten, ihm einen Dienst zu tun, aber in Wirklichkeit war es umgekehrt,
* bei den Rechtsgelehrten, die noch immer auf seinen Forschungen zur Rechtsgeschichte aufbauen oder für die seine Arbeiten zum Industrierecht Zukunftwink sind,
* bei den Historikern, die an seinen von genialer Schau getragenen Werken vorbeisehen, weil sie nicht „wissenschaftlich” genug seien,
* bei den Theologen, die es einem Laien verdenken, dass er ihnen so wichtige Ergebnisse vorsetzt, die doch nur einer vom Fach gefunden haben kann,
* bei den Soziologen, die seine Methode als „unwissenschaftlich” brandmarken, ihn aber doch irgendwie interessant finden,
* bei den Juden, die in ihm auf der Spur Heinrich Heines und Felix Mendelssohn-Bartholdy den abgefallenen Glaubensgenossen meiden,
* bei den Christen, weil er zu wenig kirchlich war,
* bei den Heiden (Atheisten), weil er zu christlich ist

Zwar wurde ihm 1929 das Amt des Vorsitzenden der World Association of Adult Education zuteil, aber auch da erging es ihm, wie er in dem Epitaph schreibt:
* but since my heart sincerely laughed and wept, all that gives power, stayed outside my reach.

Überall, wo er auf Anerkennung hoffen konnte, gab es etwas, was hinderlich im Wege standund steht.

## Die Sprachbarriere

Was ist aber das größte Hindernis? Wenn man das wüsste, würden die anderen vielleicht beiseite rücken?

Ich will versuchen, dem mit den Titeln zu leibe zu rücken, die im dritten Teil seiner Sprache des Menschengeschlechts unter der Überschrift "_Wenn eine Ewigkeit verstummt – Erinnerungen eines Ent-ewigten_"

versammelt sind (S. 15-197). Wer will schon davon etwas hören, von Verstummen und Entewigt-Werden? Ein Existenz-bedrohender Schmerz ist da angesagt.
1. „Kriegsteilnehmer aller Länder vereinigt euch” 1915 das ist doch der glatte Verrat der Loyalität gegen Gott, Kaiser und Vaterland. Klingt außerdem arg nah zu: Proletarier aller Länder vereinigt euch. Die Gemeinsamkeit der Kriegsteilnehmer hat aber den Frieden zwischen den kriegführenden Parteien im Zweiten Weltkrieg 1989 möglich gemacht.
2. „The Letters of Franz Rosenzweig and Eugen Rosenstock-Huessy, by Dorothy M. Emmet”in der neuen Ausgabe der Briefe Franz Rosenzweigs sind Eugen Rosenstocks Briefe chronologisch einsortiert in die übrigen Briefe, dadurch wird die Gestalt des Briefwechsels als einem Ereignis, an dem zwei Anteil haben, verdunkelt. Aber diese Gemeinsamkeit ist es, auf der alles Gespräch zwischen Christen und Juden nach der Schoah aufruht.
3. „Der Selbstmord Europas”so übersetzte Eugen Rosenstock 1919 Oswald Spenglers Titel „Der Untergang des Abendlands” in die Sprache des Mitverantwortlichen. Von Suizid wird nicht gerne geredet, schon gar nicht in Form der Selbstanklage. Aber nur wer den Zusammenhang mit der falschen Richtung nach dem Ersten Weltkrieg und Hitlers und Eva Brauns Selbstmord 1945 begreift, kommt zu Atem.
4. „Ehrlos – Heimatlos” 1919 gegen diese beiden Worte ist ja gerade das Geschimpf und Geheul der Nazis losgebrochen, die die verletzten Gefühle der deutschen Soldaten, die aus dem Ersten Weltkrieg heimgekehrt waren, für ihre Zwecke ausnutzten. Und auch die anderen Völker fragen erstaunt: Damit also wollt ihr euch abfinden müssen: ehrlos und heimatlos zu sein das tun wir nicht. Das Ertragen der beiden Wörter, das Auf-sich-sitzen-Lassen, das Eugen Rosenstock-Huessy als Voraussetzung für einen Frieden nach dem Weltkrieg verkündete wie der Prophet Jeremia einst in Jerusalem, ist nichts für Konservative, Sozialisten, Grüne und Liberale. Und doch ist es die Grundlage für alle Verständigung zwischen den Parteien.
5. „Lehrer oder Führer?” 1926dieser Aufsatz aus der von Martin Buber, Joseph Wittig und Viktor von Weizsäcker herausgegebenen Zeitschrift „Die Kreatur” berührt die wundeste Stelle des deutschen Gewissens.
  * Die Jungen sprechen sich in ihre Erfahrungen hinein; die Alten sprechen aus ihren Erfahrungen heraus!
  * Dieses Minimum an Polyphonie ist in den Institutionen der Bundesrepublik Deutschland zumindest eine äußerste Seltenheit. Jede Diskussion über die Ursachen und Folgen des Nationalsozialismus wird an diesen Aufsatz anknüpfen müssen, um und das wäre das wenigste dieselben Fehler nicht noch einmal zu machen.
6. Die rückwärts gelebte Zeit 1929 das kann sich keiner so recht vorstellen, es gibt zwar in der Musik den Krebsgang, aber in der geschichtlichen Zeit? Und doch hat es das vielleicht in der Bundesrepublik Deutschland und in der DDR von 1949 bis 1989 auch gegeben: Jeder konnte sich nur aufmachen zurück zu seiner persönlichen Aufbruchstelle von 1914 (also entsprechend von 1939 oder 1933) und den dort abgerissenen Faden so radikal und entschlossen wie möglich abspulen, nunmehr auf das schon erlebte Ende zu. Man holte nach, man sühnte, man ergänzte Versäumtes. Das Verstehen der Vorgänge nach 1945 und nach 1990 erfährt eine überraschende Vereinfachung, wenn die schon einmal gemachte Erfahrung zu Rate gezogen wird.

Eugen Rosenstock-Huessys Wirken fällt mithin mitten in die Leere im Gespräch der Generationen nach dem Zweiten Weltkrieg, nach der Hitler-Zeit, nach der Schoah.

Insofern gehört er und wusste das auch nicht in das 20. Jahrhundert, sondern in den Epochenrhythmus von einem Jahrtausend in das nächste.

## Freunde

Sein Wirken an den Freunden, die er in den autobiographischen Fragmenten von 1968 nennt: Werner Picht, Franz Rosenzweig, Leo Weismantel, Eugen May, Joseph Wittig, Hans Ehrenberg, Rudolf Ehrenberg, dann in den Bekanntenkreisen der Zeitschrift „Die Kreatur”, des Hohenrodter Bundes und der World Association of Adult Education, an den Universitäten Leipzig, Breslau, an der Akademie der Arbeit in Frankfurt am Main, am Dartmouth College in Hanover, New Hampshire, an den Evangelischen Akademien ist bei allen, die ihm begegneten, unvergesslich geblieben. Ja, manche Einsicht hat er so plausibel dargestellt, dass man meinte, sie ohne Nennung der Quelle weiterverbreiten zu dürfen. Es ist das mehr oder weniger deutliche Plagiat eine Begleiterscheinung seines Wirkens gewesen.

## Epilog

So harrt sein Werk, um sein Wirken zu erhellen, der Wiederentdeckung im Dritten Jahrtausend, global, das heißt: Universales und Lokales auf neue Weise verbindend.

Eckart Wilkens\
Köln, 24. April 2007

[zum Seitenbeginn](#top)
