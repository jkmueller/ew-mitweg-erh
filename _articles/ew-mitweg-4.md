---
title: "Mitweg Band 4: Zurück ins Wagnis der Sprache"
category: mitweg
created: 2003-03
published: 2023-09-11
---
### INHALT

1.
„ZURÜCK IN DAS WAGNIS DER SPRACHE“ – \
EUGEN ROSENSTOCK-HUESSY 1950 IN GÖTTINGEN \
Am 6. Oktober 1984 (10. Tishri 5745) in der Evangelischen Akademie Berlin, Wannsee \
auf der Tagung der Evangelischen Akademie Berlin (West) gemeinsam mit der Eugen Rosenstock-Huessy Gesellschaft: \
Sprachwandlungen. Entscheidende Augenblicke im Leben Eugen Rosenstock-Huessys, \
5.-7. Oktober 1984

2.
DIE DEUTSCHE UNIVERSITÄT ZUR FRIEDENSFRAGE: ROSENSTOCK-HUESSY 1944 \
Dienstag, 23. Oktober 1984, VHS-Forum am Neumarkt

3.
WIE ÜBERSETZEN – \
NACH DEN WELTKRIEGEN \
am Beispiel Eugen Rosenstock-Huessys, „patmologisch“ \
für die Julitagung 1985 in Berlin

4.
DER EXODUS BEI EUGEN ROSENSTOCK-HUESSY UND IMMANUEL VELIKOVSKY \
Dienstag, 12. März 1985

5.
DIE NAMEN GOTTES: AUS ROSENSTOCK-HUESSYS SPRACHBUCH 1962 \
Dienstag, 22. Oktober 1985, 20 Uhr

6.
SHAKESPEARE ́S SONETTE \
AN ELIZABETH DE VERE, DEUTSCH \
Donnerstag, 31. Oktober 1985, 20 Uhr, VHS-Forum


[Eckart Wilkens: Mitweg Band 4: Zurück ins Wagnis der Sprache]({{ 'assets/downloads/wilkens_mitweg_erh_4_zurueck_ins_wagnis_der_sprache.pdf' | relative_url }})
