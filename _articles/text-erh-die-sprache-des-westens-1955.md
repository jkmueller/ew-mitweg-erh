---
title: "Eugen Rosenstock-Huessy: Die Sprache des Westens (1955)"
category: bearbeitung
created: 1955
published: 2024-02-24
---
Eugen Rosenstock-Huessy

### DIE SPRACHE DES WESTENS

in: *Geschichte in Wissenschaft und Unterricht 6, Zeitschrift des Verbandes der Geschichtslehrer Deutschlands 1955*

*neu gegliedert von\
Eckart Wilkens*


### NOTIZ ZUR GLIEDERUNG

1

Wie da mit so leichtem Mut – oder so scheint es – die deutsche Sprache wieder aufgenommen ist! Der Dank gilt hier Robert Grosche aus Köln, so alt wie Rosenstock-Huessy, 1920-30 Studentenpfarrer in Köln, 1943 Stadtdechant von Köln, von 1945 an Pfarrer in St. Gereon. In Gottfried Hofmanns Chronik ist er nicht erwähnt.

2

Und wie die drei nachchristlichen Jahrtausende heranrücken, indem Mönch, Individuum (Genie) und sakramentale Gruppe vor uns treten! Die Freiwilligkeit und die Liebe gehören zu Ehe, Freundschaft und sakramentaler Gruppe – und damit auch das Zulassen des Vorübergehenden als nicht minderwertig.

3

Wenigstens ein Zitat aus Hans Ehrenbergs „Östlichem Christentum“, Band I, S. 173, aus dem Stück: Einige Worte eines orthodoxen Christen über die abendländischen Glaubensbekenntnisse, geschrieben infolge einer Broschüre des Herrn Laurentie im Jahre 1853:

*Gewiß hat das Christentum auch seinen logischen Ausdruck, der im Symbol enthalten ist; aber dieser logische Ausdruck kann von den übrigen Manifestationen nicht losgetrennt werden. Es wird auch wie eine Wissenschaft unter dem Namen Theologie gelehrt, aber diese ist nur ein Zweig der allgemeinen Lehre. Wer sie davon lostrennt, wer also die Lehre (im engeren Sinn des Vortrags und der Auslegung) von ihren anderen Gestalten abtrennt, der begeht einen schweren Irrtum, wer aus ihr ein Vorrecht macht, begeht eine Torheit, wer sie für eine himmlische Gabe erkläft, die an gewisse Ämter gebunden sei, fällt einer Ketzerei anheim, denn er setzt damit ein neues Sakrament: das Sakrament des Rationalismus (oder des logischen Wissens) ein.* ***Die Kirche erkennt keine andere lehrende Kirche an, als sich selbst in ihrer Gesamtheit.***

4

Die Verzeichnisse dienen der Klärung des Gehörs: das Inhaltsverzeichnis richtet die Aufmerksamkeit auf das Ganze in seinen Gliederungen; das Namensverzeichnis öffnet die Türen für unendlich weitergehende Forschung; die angemerkten Geschichten (im Text mit Überschrift und gefärbt) verbinden mit dem Erleben Rosenstock-Huessys der Geschichte des Menschengeschlechts; die Merksprüche fordern die Aufmerksamkeit auch jenseits des Lesevorgangs.

*Köln, 13. November 2019\
Eckart Wilkens*

[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Die_Sprache_des_Westens_1955.pdf' | relative_url }})

[Das Orginal von „Die Sprache des Westens (1955)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/480.pdf)
