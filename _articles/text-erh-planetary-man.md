---
title: "Eugen Rosenstock-Huessy: Der Mensch Auf Planet Erde (1946)"
category: bearbeitung
created: 1946
published: 2023-09-15
---
DER MENSCH AUF PLANET ERDE

In memoriam Oswald Spengler

*übersetzt und gegliedert \
von \
Eckart Wilkens*


NOTIZ ZUR ÜBERSETZUNG

Text: die Seiten aus den New English Weekly vom 30. Mai, 5. und 13. Juni 1946, London 1946, mit einer handschriftlichen Korrektur S. 4 (us) und zwei handschriftlichen Zusätzen, die in der Übersetzung gekennzeichnet sind.

Der Text ist in meiner Übersetzung reicher gegliedert und – für den ersten Teil und dessen Kapitel – mit Überschriften versehen. So ist dem Weg der Rede leichter zu folgen, wie sie sich durch das Gehör schlängelt (denn beim Hören entsteht ja das Gefühl des rasch wechselnden, nicht zu überblickenden Gedankengangs nicht, das – bei unseren Lesegewohnheiten – sonst leicht entsteht).

Zehn Jahre nach Oswald Spenglers Tod am 8. Mai 1936 in München spannt diese an die Gegner im Zweiten Weltkrieg geschriebene Rede den Bogen zur Offenbarung der Begegnung Rosenstock-Huessys mit Spenglers Werk in der Kritik aus der „Hochzeit des Kriegs und der Revolution“ 1920: Der Selbstmord Europas, wo Spenglers Irrtum benannt ist.

Das In Memoriam Oswald Spengler zeigt Rosenstock-Huessys Kraft, angesichts der Gegner zu sprechen (vergleiche die Lebensdaten der genannten Personen: Klages, Löpelmann, Bernanos als Mitverantwortliche).

Die Schau auf Europa als die entfaltete Grenze zwischen Rußland und Amerika und wie die Amerikaner damit umgehen sollten, wirft doch auf das deutsche Gejammer über den verlorenen Krieg und die deutsche Teilung ein anderes Licht.

*Köln, 13. Juni 2013 \
Eckart Wilkens*

[Eckarts Übersetzung und Bearbeitung]( {{ 'assets/downloads/wilkens_erh_Planetary_Man.pdf' | relative_url }})

[Das Orginal von „Planetary Man” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/402.pdf)
