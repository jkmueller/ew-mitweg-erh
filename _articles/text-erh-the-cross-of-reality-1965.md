---
title: "Eugen Rosenstock-Huessy: The Cross of Reality (1965)"
category: lectures
created: 1965
published: 2023-09-22
---
### NOTE BY THE EDITOR

This is the edited transcription of one lecture July 1965.

1

The transcription was made by Frances Huessy – my work are the following changes:
1. Commonplace phrases as “you see”, “so to speak” are eliminated. Where the speaker corrects himself within the same sentence, only the corrected version is kept.
2. Additions: \
   paragraphs, \
   chapters with titles scooped from the text, \
   Roman numbers for the four parts of a chapter, \
   Arabian numbers for the four parts of the parts of a chapter, \
   titles for the stories – which are marked by color – which communicate either a personal or historical event, \
   sentences are marked in bold print, which are as a sum of thought and to be kept as taken for themselves, \
   indices of contents, names, stories, sentences.
2

This is a single lecture – you feel that the ascertaining of the listeners is missing. And therefore this warning not to use the cross of reality as a spatial concept on the blackboard.

It is useful for all purposes of teaching the cross of reality, never to forget that even the smallest process of understanding takes time, a real part of your lifetime!

It was Franz Rosenzweig who warned his friend Eugen in his last letter to him not to give his teaching in an apparently simple form of a formula too early in the process of teaching, but as a final statement – as happened here.

I corrected the second Latin formula according to the aforegoing translation: One: I can be measured in terms of quantity; hence I am somebody, something. Which has to be the passive form: The things may say, the masses may claim mensurari possum, quia sum."

*Cologne, May 13, 2017 \
Eckart Wilkens*

- [Eckart's Edition: ERH The Cross of Reality (1965)]( {{ 'assets/downloads/wilkens_erh_The_cross_of_reality_1965.pdf' | relative_url }})

- [The transcript of „ERH The Cross of Reality (1965)” as PDF](https://www.erhfund.org/wp-content/uploads/653.pdf)
- [The audio recordings of „ERH The Cross of Reality (1965)”](https://www.erhfund.org/lecturealbum/volume-28-cross-of-reality-1965/)
