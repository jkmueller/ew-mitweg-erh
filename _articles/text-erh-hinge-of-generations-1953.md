---
title: "Eugen Rosenstock-Huessy: Hinge of Generations (1953)"
category: lectures
created: 1953
published: 2023-09-23
---


- [Eckart's Edition: ERH Hinge of Generations (1953))]( {{ 'assets/downloads/wilkens_erh_Hinge_of_Generations_1953.pdf' | relative_url }})

- [The transcript of „ERH Hinge of Generations (1953))” as PDF](https://www.erhfund.org/wp-content/uploads/631.pdf)
- [The audio recordings of „ERH Hinge of Generations (1953)”](https://www.erhfund.org/lecturealbum/volume-06-hinge-of-generations-1953/)
