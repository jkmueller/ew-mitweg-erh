---
title: "Eugen Rosenstock-Huessy: Friedrich Nietzsches Funktion in der Kirche und die Krise von Theologie und Philosophie"
category: bearbeitung
created: 1942
published: 2024-01-10
---
#### NOTIZ DES ÜBERSETZERS

Die Übersetzung folgt der Abschrift, die Lise van der Molen gefertigt hat, er weist noch auf einen Brief von Stanley Pargellis vom 18. März 1943 hin, wo vielleicht Näheres zu erfahren ist.

1

Die Einteilung in sieben Teile ist demnach von Rosenstock-Huessy. In Lise van der Molens Abschrift ist die 1944 verfaßte Nietzsche-Schrift: Nietzsches Masken eingeschoben. Ich habe diese getrennt übersetzt und halte sie für selbständig, obwohl Verknüpfungen da sind, was ja nicht weiter verwunderlich ist. Besonders wichtig der Hinweis auf das Buch von E. O ́Brien, Son of the Morning, 1923, wo auf die Theseus-Ariadne-Geschichte von Nietzsche-Hans von Bülow-Richard Wagner hingewiesen wird. Vielleicht ist es die fehlende Fußnote in dem zweiten Stück Nietzsches Masken zu der Cosima-Ariadne-Geschichte.

2

Die Einteilung des Textes, teilweise noch detaillierter als in Lise van der Molens Abschnitt, versucht, den Wechsel der Töne präjektiv-subjektiv-trajektiv-objektiv (jeweils von I-IV und kleinteiliger von 1-4) für den Leser darzustellen, damit er, wie es heißt, „auf der Höhe“ bleibt, auf der Höhe nämlich, von der her Rosenstock-Huessy spricht.

3

Was werden die Hörer jener Versammlung der im Geburtsjahr Rosenstock-Huessys gegründeten American Association of Church History und die der American Historical Association im Dezember 1942 gesagt haben? Ein Jahr nach Japans Angriff auf Pearl Habour. So konzentriert das Papier zu der Konferenz auch ist, das „Kreuz der Wirklichkeit“, die Distemporaneität postulierend – wieviel von Rosenstock- Huessys Bewährung dieser Postulate wird hier vorgetragen, bitter bezahlt?

4

Die Übersetzung überraschte mich, indem viele Wendungen im Englischen so klar gemeißelt sind, deren „sprechende“ Übersetzung zu finden dann gar nicht leicht ist.

Das mag auch daran liegen, daß das Jahr 1942 (am 26. April wurde Hitler auch Oberbefehlshaber der Wehrmacht und damit schied jede Aussicht auf Recht in deutschen Landen dahin) die größte Verlassenheit Eugen Rosenstock-Huessys von seiner Herkunft bezeichnet. Daß diese nun in diesem Stück über Nietzsche verlautet wird, zeigt seine Treue.

Meines Wissens bin ich der erste Übersetzer ins Deutsche – ich war bei jener Konferenz ein halbes Jahr alt und faßte das dort gelesene Wort nun im 75. Lebensjahr.
Köln, 4. August 2016 Eckart Wilkens


[Eckarts Übersetzung]( {{ 'assets/downloads/wilkens_erh_Nietzsches_Funktion_in_der_Kirche.pdf' | relative_url }})

[The English text: „Friedrich Nietzsche’s Function in the Church and the Crisis of Theology and Philosophy”](https://www.rosenstock-huessy.com/text-erh-nietzsches-function-1942/)

[The original „Nietzsches functional role in the Christian world and in the Chaos of Theology and Philosophy” as PDF-Scan](https://www.erhfund.org/wp-content/uploads/369.pdf)
