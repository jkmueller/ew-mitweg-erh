---
title: "Eugen Rosenstock-Huessy: Die Worte des Credo (1955)"
category: bearbeitung
created: 1955
published: 2024-02-18
---
Eugen Rosenstock-Huessy

### Die Worte des Credo

1955

geschrieben für die Magna Carta Latina für Ford Lewis Battles

neu übersetzt und gegliedert von Eckart Wilkens

### NOTIZ DES ÜBERSETZERS

1.

Im *Geheimnis der Universität*, 1958 steht das Stück: *Die Worte des Glaubensbekenntnisses*. In der Bibliographie schreibt Lise van der Molen: *fehlerhafte Übersetzung ohne den Einschub* von zwei Seiten auf S. 17. Ursprünglich geschrieben für die *Magna Carta Latina*, die er mit Ford Lewis Battles, dem calvinistischen Freund und Theologen herausgab, datiert auf den 12. September 1955.

2.

Die Übersetzung erscheint in dem Buch zum siebzigsten Geburtstag undatiert und nicht adressiert. Aber gerade das Gezeitigtwerden durch Antlitzen ist doch der Unterschied zwischen griechischer Argumentation und heilsgeschichtlicher Wahrheit. War der Freund Georg Müller in Bielefeld doch so beschattet von der Sprachlosigkeit der Hitler-Jahre, daß er in den vielen Übersetzungen, die im *Geheimnis* und in der *Sprache des Menschengeschlechts* 1963/64 erschienen, die Tatsache der Immigration nach Amerika ausblenden mußte?

3.

Jedenfalls Anlaß genug, das Stück neu zu übersetzen. Und eben nicht wie in der Schule Satz für Satz, wie er da vokabelmäßig und grammatisch strukturiert steht, sondern – hoffentlich – mit dem Verständnis des Ganzen, um das es geht und mit dem Bedenken, wie der Tonfall des englischen Satzes, wie die Eigenheiten des Englischen ohne Konjugation und Deklination, wie der Gebrauch von Artikel und Pronomina in den Tonfall des deutschen Satzes usw. übersetzt werden können, so daß der Übersetzer immerhin sagen kann: auch ich habe den Text vom Ganzen her verstanden und dieses Verständnis den einzelnen Sätzen auch angetragen.

4.

Die Epochenbildung „von Athanasius bis Calvin“ – Athanasius starb am 2. Mai 373 in Alexandria, Calvin am 27. Mai 1564 in Genf – mutet dem Leser Ford Lewis Battles zu, das Danach anzuerkennen, also die Epoche nach 1564, das heißt aber auch: sich des Schutzes der reformierten Theologie, ob nun lutherscher oder calvinistischer Prägung, zu begeben und das Verständnis des Credo, des Ecksteins der christlichen Kirche seit dem Konzil von Nicäa im Jahre 325, einberufen von Konstantin d. Gr., zu hinterfragen, nicht im logischen, sondern im historischen Sinne: nämlich mit dem Verstehen, daß die Lage der Apostel und der von ihnen ins Leben gerufenen Kirche in den Jahren von Jesu Tod bis zum Konzil von Nicäa eine andere war, als es die gesichertere Stellung danach ermöglichte.

5.

Kann man verstehen, warum diese Herausforderung an die Überlieferung der Ewigkeitsvorstellungen so unerwidert geblieben ist? Welche Unruhe daher kommen muß, wenn die lebende Generation so zur existentiellen Erfahrung gerufen wird?

Ich gestehe, daß mir diese Dringlichkeit bekannt war, seit ich das *„Geheimnis“* gelesen habe – aber doch nicht deutlich genug, daß es zum Weitersagen gereicht hätte. (Und ich bin auch keinem begegnet, der mit Rosenstock-Huessys Werk vertraut ist, dem diese Dringlichkeit anzumerken gewesen wäre.)

Ein weiterer Beweis für das Zeitigen der Wahrheit!

6

Die Übersetzung nimmt sich die Freiheit, den Text als Gehörtes wiederzugeben. Das heißt, daß der Wechsel der Beteiligung, der vom Hörer oder Leser gefordert wird, im Schriftbild angezeigt wird.

Das war zu Zeiten, als die gedruckten Bücher noch verlautet wurden, sogar beim lautlosen Lesen, nicht so notwendig. Papier und Einband waren knapp, Rosenstock- Huessy hat die papierlose Zeit des Ersten Weltkriegs erlebt (unser Griechischlehrer Ernst Weiß benutzte Zeitungsränder für Notizen), ihm wäre nicht beigekommen, seine Texte so optisch zu vermitteln.

7

Der Wechsel der Töne folgt den grammatischen Gesetzen, die Rosenstock-Huessys A und O sind, also jeweils. das heißt auf verschiedenen Ebenen, der Passage von *präjektiv, subjektiv, trajektiv, objektiv*.

Das Ganze gliedert sich in vier Teile, die Teile in Kapitel (Teil 2 und 4 haben nur zwei Kapitel), die Kapitel in römisch gezählte Abschnitte, die Abschnitte wieder in arabisch gezählte Unterglieder.

Wer nur liest, muß diesen Wechsel, wenn er nicht eigens dargestellt wird (man hört ja dann nichts), nur verwirrend finden – und so ist ja auch die Reaktion vieler Leser Rosenstock-Huessyscher Sachen, daß sie diese Verwirrung nicht bestehen und aufgeben, die Gestalt des Ganzen zu erreichen und bei der Einen Gestalt, die das Ganze ist, auch zu bleiben, das heißt: es weiterzusagen, es zu tun.

Die optische Gliederung hat andrerseits die Gefahr, daß man die einzelnen Sätze zu sehr isoliert und den Zusammenhang auch so verliert. Die Feinheit der Argumentation ist bestechend – es gehört einige Wendigkeit dazu, sie mitzumachen.

Man spürt diesem Stück an, daß es an einen theologisch gewieften Leser gerichtet ist und dessen Wissen großzügig in Anspruch nimmt.

8

Im Jahre 1955 – Rosenstock-Huessy wurde am 6. Juli 67 Jahre alt – erschien in deutscher Sprache *Des Christen Zukunft*, übersetzt von Christoph von dem Bussche und Konrad Thomas, das Buch, das einschlug, so daß es 1965 als Siebenstern Taschenbuch erschien. Aber das Hauptwerk, das seit 1917 unterwegs war, die *Soziologie in zwei Bänden* erschien erst 1956 und 1958. So ist das Stück *Die Worte des Credo* doch noch in der Schwebe der Erwartung auf das Aufatmen: *Ich habs gesagt*. Die Intimität mit dem calvinistischen Freund hebt diese Schwebe liebevoll auf. Ich hoffe, daß bei der Übersetzung davon etwas übrig geblieben ist.

9

Die vier hinzugefügten Verzeichnisse beleuchten die Vierfalt der Aspekte:

das **Inhaltsverzeichnis** ebnet den Weg vom Ganzen zu den Teilen (präjektiv, vom Leser aus gesehen),

das **Namensverzeichnis** zeigt, welche Personen als unmittelbar nennbar präsent sind (das mag für den Leser nach mehr als sechzig Jahren und in anderen Umständen nicht immer zutreffen, aber die Namen geben ja Gelegenheit, dieser Gesellschaft durch genaueres Nachforschen beizutreten) (subjektiv),

die Heraushebung der **Geschichten von** ... verdeutlicht, welche Anknüpfungspunkte in der Geschichte des Menschengeschlechts ad hoc angesprochen werden (trajektiv),

die **Merksprüche**, von mir ausgewählt, nicht vom Autor, stellen Sätze heraus, die auch außerhalb des Kontexts Anstoß, Bestätigung, Erneuerung, Nachdenken erzeugen mögen (objektiv).

10

Wie weit jenseits der Spaltung in Ost- und Westkirche, in römisch-katholisch und protestantisch, in lutherisch und reformiert versetzt dieses Stück, um die von der Offenbarung gefundene existentielle Erlebnisweise zu retten! (Franz Rosenzweigs *Stern der Erlösung* glitzert mit seinen drei Teilen *Schöpfung – Offenbarung – Erlösung* immer wieder auf).

11

Gern schicke ich diese Übersetzung als Advents- und Weihnachtsgruß den Freunden von der Eugen Rosenstock-Huessy Gesellschaft mit den Zeilen, die meinen Eindruck von der Übersetzungsarbeit reflektieren.

Musik im Palast der Seele, den Freundschaften,\
legt die Tannenzweige auf den Tisch,\
steckt die Lichter an, traut dem Wort,\
daß es zur rechten Zeit kommt, wieder geht,\
wenn nichts mehr im Wege steht, es zu tun,\
die duftende Atmosphäre beschenkt den Atem:\
wir wissen noch nicht, daß der Gesang\
längst im Herzen Wohnung genommen hat,\
ehe die klare Vernunft dahinter gekommen ist.

Es ist leichter, die Sterbehemden anzuziehen\
als die Hochzeitskleider, deshalb gebt acht,\
daß die Taufe nicht in Vergessenheit gerät:\
Gottes Kinder laufen im Freimut herum,\
Jesus Christus aus Nazareth hat uns gesetzt\
in die Advents- und Weihnachtsdecke,\
die leicht und zart leuchtet, sogar die Sterne\
haben sich einflechten lassen,\
hör auf das Licht, sprich den Namen.

*Köln, 5. Dezember 2019\
Eckart Wilkens*

[Eckarts Übersetzung]( {{ 'assets/downloads/wilkens_erh_Die_Worte_des_Credo.pdf' | relative_url }})

[Das Orginal von „The Terms of the Creed (1955)” als PDF-Scan](https://www.erhfund.org/wp-content/uploads/488.pdf)
