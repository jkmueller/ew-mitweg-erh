---
title: "Eugen Rosenstock-Huessy: Angewandte Seelenkunde (1923)"
category: bearbeitung
created: 1923
published: 2024-05-10
---
Eugen Rosenstock-Huessy

### Angewandte Seelenkunde

1923

*mit anderer Gliederung von \
Eckart Wilkens*


NOTIZ

1

Vorstehendes ist eine genaue Abschrift (mit ein paar Korrekturen, besonders aber der Richtigstellung der Tabelle mit den Sätzen und Fragen zur ersten, zweiten und dritten Person) des letzten Kapitels des ersten Bandes der *Sprache des Menschengeschlechts* S. 739-810, Heidelberg 1963, allerdings ohne die Fußnote auf S. 739:

*Nunmehr ist die erste Hälfte dieser leibhaftigen Grammatik abgeschlossen. Freilich sind ihre beiden Themen: „Wer spricht?“ und „Wie wird gesprochen?“ unendlicher Entfaltung fähig. Aber einem willigen Lesersollte der Weg aus der bisherigen alexandrinischen Grammatik in eine inkarnierende Sprachlehre geöffnet sein. Die folgende Zusammenfassung legt ihn darauf fest. Sie ging 1916 als „Sprachbrief“ an Franz Rosenzweig zur Abwehr aller Sprachphilosophie, und so ist sie die älteste Urkunde eines Sprachdenkens, in dem die Epoche von Parmenides bis Hegel ausgeschieden ist.*

Franz Rosenzweig bestätigte das in seinem Aufsatz Das Neue Denken, geschrieben im Februar 1925, erschienen in der Oktobernummer des „Morgen“ 1925, zum *Stern er Erlösung*:

*doch verdanke ich diese für das Zustandekommen des Buches entscheidende Beeinflussung nicht ihnen (nämlich den Stellen bei Hermann Cohen), sondern Eugen Rosenstock, dessen jetzt gedruckte „Angewandte Seelenkunde“ mir, als ich zu schreiben begann, schon anderthalb Jahre im ersten Entwurf vorlag.*

Er begann mit dem Stern der Erlösung aber Ende des Jahres 1917.

Nun ist aber die „Angewandte Seelenkunde“ ein Buch von 1924: *Angewandte Seelenkunde, eine programmatische Übersetzung*, Bücher der deutschen Wirklichkeit, herausgegeben von Wilhelm Henrich, Darmstadt, Roether Verlag. Und Anstoß dazu mag – neben anderem – die Buchbesprechung gewesen sein, die Eugen Rosenstock-Huessy zu dem 1921 in Gotha erschienenen Werk von Karl Haase mit demselben Titel ge- schrieben hat, erschienen in der *Arbeitsgemeinschaft*, 4. Jahrgang 1923, S. 129-146, also 17 Seiten stark.

Das sorgte bei mir als dem Leser des Sprachbuches – vor 40 Jahren - für einige Verwirrung. Was war nun der Text des Sprachbriefes von 1916?

Und diese Verwirrung ist es, die mich nun veranlaßt hat, den Text abzuschreiben und dabei drei Gliederungsebenen einzuziehen:

2

 Der Text hat nun 6 Teile, die vielleicht die verschiedenen Schichten erkennen lassen:

Erster Teil: Die Unterscheidung\ Zweiter Teil: Die neue Lehre (1)\ Dritter Teil: Erste Anwendung der neuen Lehre auf die vorher getroffene Unterscheidung\ Vierter Teil: Die neue Lehre (2)\ Fünfter Teil: Die Katastrophe des 9. November 1918 \
Sechster Teil: Proklamation

Das heißt: die Teile 2 und 4 mögen etwa enthalten, was aus dem Sprachbrief von 1916 stammt.

3

Die zweite Ebene sind die 12 Abschnitte, die Rosenstock-Huessy selber gemacht hat:

*I (ohne Überschrift)* \
*II Die Wissenschaft der Psychologie \
III Die Psyche \
 IV Die okkulten Wissenschaften*\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zusammen 13 1⁄2 Seiten im Sprachbuch,\
*V Die Grammatik der Seele*\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16 Seiten im Sprachbuch, \
*VI Das Schicksal der Seele \
VII Die Kräfte der Seele \
VIII Gemeinschaft*\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zusammen 11 1⁄2 Seiten im Sprachbuch,\
*IX Die Sprache der Gemeinschaft*\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;13 Seiten im Sprachbuch\
*X Unser Volk* \
 *XI Geist, Seele, Leib* \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;zusammen fast 9 Seiten im Sprachbuch,\
*XII Die grammatische Methode* \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;etwas über 8 Seiten im Sprachbuch.

Das heißt meine Teile 2 und 4 (Die neue Lehre) machen 29 Seiten des ganzen Textes aus, die anderen Teile zusammen aber 42 Seiten. Man halte dazu, daß die Besprechung des Karl Haase Buches etwa 17 Seiten stark war.

4

Ein wichtiger Teil der Schrift ist die Datierung auf den 9. November 1918, die Eugen Rosenstock-Huessy als das entscheidende Datum für die *Hochzeit des Kriegs und der Revolution* genommen hat (so hieß sein Buch von 1920).

Für das Verständnis ist es wichtig, daß die Entdeckung der grammatischen Methode schon vor dem 9. November 1918 geschah, mitten im Weltkrieg – aber doch dessen Ausgang schon ahnend. So entstand in Rosenstock-Huessy auch nicht das Gefühl, mit dem Sprachbrief von 1916 etwa schon veraltet zu sein: er war damit den Ereignissen voraus, oder vielmehr: im Herzen der Ereignisse. Mit Franz Rosenzweig hat er diese Datierung geteilt.

Dafür steht als majestätischer Beleg das Zitat: *Die Seele ist kein Ding.* Mit diesem Satz nämlich retteten sich die beiden aus dem Dilemma, daß Franz Rosenzweig und Margrit Rosenstock-Huessy nach dem Sprachbrief einander begegnet sind und diese Begegnung die Sprachwirklichkeit des *Sterns der Erlösung* ermöglicht hat!

Es steht in dem Kommentar zu dem Gedicht Seele von Jehuda Halevi von Franz Rosenzweig, und der erschien erstmals mit den *60 Hymnen und Gedichten des Jehuda Halevi deutsch* im Frühjahr 1924 in Konstanz, also gleichzeitig mit der *Angewandten Seelenkunde*. Für alle, die zu der Freundschaft Rosenstocks wie Rosenzweigs gehörten, war dieses Zitat das klare Bekenntnis zu dem Bestand der seit 1914 währenden Freundschaft zwischen den beiden.

Für Rosenstock-Huessy war es Zeit, mit der Entdeckung der grammatischen Methode herauszurücken. 1922 legte er die Leitung der Akademie der Arbeit nieder, war bis Sommer 1923 arbeitslos, er promovierte 1923 nochmals mit seinem Professorenbuch Königshaus und Stämme , vertrat Willy Hellpach in Karlruhe, habilitierte sich als Privatdozent für Soziologie, wurde an die Universität Breslau berufen. 1924 war er dann Mitgründer des *Hohenrodter Bundes*. Vor der *Soziologie I, Die Kräfte der Gemeinschaft* von 1925 erschien also die programmatische Schrift *Angewandte Seelenkunde*.

Wie wichtig sie ihm war, geht aus dem Satz hervor: *Die ausführliche Darstellung wird auch hier wie in so viel anderen Punkten von dem Schicksal dieser Schrift abhängen.*

5

Bei vielen Gelegenheiten habe ich bemerkt, daß Eugen Rosenstock-Huessy so schreibt, daß die Leser verleitet werden, zu schnell zu lesen. Und dann überstürzen sich die Gedanken, ja, der Vorwurf, er halte nicht genug Disziplin bei seinen Schriften und Gedankenwegen, mag daher rühren. Bei der Abschrift einiger Briefe von ihm habe ich aber festgestellt, daß die Unruhe und Nervosität, die man empfinden mag, ganz verschwindet, wenn man der Gliederung des Textes selber folgt und eine Gliederung einzieht, also ein Innehalten, wenn der Gedanke eine andere Richtung nimmt.

Dieser Wechsel der Gedankenrichtung ist aber für die Darstellung dessen, worum es geht, gerade konstitutiv, also nicht Mangel, sondern entscheidendes Merkmal. Nur darf der Leser darauf hoffen, daß er diesen Wechsel angezeigt bekommt.

Dem dienen nun meine Unterscheidungen in numerierte Abschnitte und das Einziehen von Absätzen im Text, wo sie in der Vorlage nicht stehen. Im Inhaltsverzeichnis habe ich versucht, die Richtung der Abschnitte etwa zu bestimmen.

### ERSTER TEIL: DIE UNTERSCHEIDUNG

I

1  Fremdwortübersetzung \
2  Echo (Soziologie) \
3  Psychologie \
4  „Angewandte Seelenkunde“ \
5  William James

Zuerst wird bestimmt, was jetzt Mode ist – und diese Mode steht in Gefahr, die wirkliche Antwort auf das Ereignis Weltkrieg zu verfehlen. Die Soziologie, die von vornherein als Trägerin des neuen Wissens angekündigt wird, ist aber noch zu früh, um das Verständnis zu wecken, wo es nötig ist. Deshalb gilt es, die Psychologie (von der ausdrücklich gesagt wird, daß die Frauen der Salons in Frankreich ihr Interesse und Gehör geschenkt haben) zu klären. Die Übersetzung von *praktischer Psychologie* in *angewandte Seelenkunde* zeitigt nun verblüffende Widersprüche und Gedankengänge. Wie sehr, darauf weist die Tatsache hin, daß der berühmte Psychologe William James ohne das Wort Seele auszukommen glaubte.

II DIE WISSENSCHAFT DER PSYCHOLOGIE \
 1 Konflikt Seelenkunde – Seele \
 2 Sinnesreaktionen, Intelligenz \
 3 „Geist und Körper, Seele und Leib“ (nach 1900) \
4 Theodor Erisman

Der Konflikt, der durch die verschiedenen Worthintergründe von Psyche und Seele entsteht, ist Ausgangspunkt der Schrift, wie sie nun – 1924 – vorgestellt wird. Die Unterscheidung dessen, was die Psychologie tut und was nicht, wie sehr das Wort und die Wirklichkeit der Seele dabei verfehlt wird, wird dargelegt, letzter Zeuge unter vielen ist dann Theodor Erisman.

III DIE PSYCHE \
 1 Seele \
 2 Umsatzstelle für Physisches, Empfangsapparat für Spirituelles \
3 Die okkulten Wissenschaften (Max Dessoir)

Was ist nun aber die Seele? Wenn man sie beschränkt auf die beiden Funktionen, Umsatzstelle für Physisches und Empfangsapparat für Spirituelles zu sein, kommt auch nicht weiter. Das von den Psychologen gemiedene Gebiet, das Max Dessoir distanziert referiert, die okkulten Wissenschaften, die in den zwanziger Jahren erneut Aufwind bei der Publikumsaufmerksamkeit erhielten, können nicht weiter unbeachtet bleiben.

IV DIE OKKULTEN WISSENSCHAFTEN \
1 Magie, Telepathie, Spiritismus, Hypnose \
2 Prophetie \ 
3 Jesus \
4 Religionspsychologie (James, Wobbermin) \
5 Das natürliche Untergeschoß des Glaubens \
6 Himmel und Hölle in Bewegung setzen \
 7 Die Glasglocke\
8 Die Fehler \
 9 Die gegeißelte Psyche (C. F. Meyer)

Ausführlich wird dargelegt, was die okkulten Wissenschaften tun und leisten, was nicht. Verblüffend sicher für viele das Zu-bedenken-geben der Prophetie als wirklicher Menschheitskraft. Die religiösen Kräfte wird man auch nicht zähmen, wenn man sie in Religionspsychologie behandelt. Eine stabile Definition: die okkulten Wissenschaften bilden das Untergeschoß des Glaubens – sie sind nicht der Glaube, aber sie bilden sein Untergeschoß! Ein erstaunliches Zugeständnis. Das aber zugleich sich der Kritik stellen muß, daß beide, Psychologie und okkulte Wissenschaften, denselben Fehler machen. Ein Vers von Conrad Ferdinand Meyer – eines Schweizers, also doch in der Nähe der Schweizerin Margrit Rosenstock-Huessy – formuliert eine sattsam gemachte Erfahrung.

### ZWEITER TEIL: DIE NEUE LEHRE (1)

V DIE GRAMMATIK DER SEELE \
 1 Parallele zur Naturwissenschaft vor der Befreiung von der Logik \
 2 Primat des Einzel-Ichs? (Spengler) \
 3 Die namentliche Anrede als Voraufgehendes, Du, Imperativ, die anderen Personen \
4 Die Modi, Urleistung der Sprache, der Menschenmensch, Voluntativ \
 5 Der Indikativ \
6 Freiheit \
 7 Das Du in der Liebesverwandlung des Imperativs, die bloßen Samariter des Denkens (Hermann Cohen) \
 8 Die Tempora \
 9 Abgrenzung von den Philosophen und Okkultisten \
 10 Ursprünglich-Sprechen (Goethe) \
 11 Wiederursprung (Nein und Trotz) \
 12 Stilgattungen \
 13 Volk = Verwandlungskraft \
 14 Die Du-Ich-Reihenfolge der Seelenverfassung \
 15 Cogito ergo sum (Descartes) Man gibt mir einen Namen, darum bin ich (Claudius) \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DIE SEELE SOLL DIE ANTWORT DES MENSCHEN AN GOTT SEIN \
16 Die Lehre vom Gestaltenwandel

Mit einem läutenden: Wir aber beginnt die Darstellung der neuen Lehre. Ich meine, daß etwa ab meinem Abschnitt 3 der Sprachbrief von 1916 beginnen könnte. Die wichtige Entdeckung, daß das Du immer dem Ich vorausgeht, ist die grundstürzende Tatsache unseres sozialen und seelischen Lebens – und zwar während des ganzen Lebens. Das wird nun in den Modi entfaltet, den Tempora, den Stilgattungen – eine ganze Fülle von Beobachtungen, die nun plötzlich zu einem Sinn zusammenschie-ßen, erwartet den Leser. Die für den *Stern der Erlösung* grundlegende Unterscheidung von *Gott Mensch Welt*, *Schöpfung Offenbarung Erlösung* ist deutlich zu finden. Rosenstock-Huessy hat es dankbar vermerkt, daß Rosenzweig sich zu seiner Vorgängerschaft offen bekannt hat. Sogar das spätere Motto für die dritte Form der Hochschule: Respondeo etsi mutabor ist schon vorgebildet: Man gibt mir einen Namen, darum bin ich. Und das entscheidende Merkmal bildet den Schluß dieser fulminanten Darlegung: die Lehre vom Gestaltenwandel.

### DRITTER TEIL: ERSTE ANWENDUNG DER NEUEN LEHRE AUF DIE VORHER GETROFFENE UNTERSCHEIDUNG

VI DAS SCHICKSAL DER SEELE \
 1 Träger eines einzigartigen Schicksals \
 2 Wo zwei Verschiedenes tun, da kann es das Gleiche bedeuten! \
3 Schöpfungsgleichnis, Lebensgeschichte, Weltverwandelnd \
 4 Fragestellungen

Nun gilt es, die zuvor getroffenen Unterscheidungen an dem Dargelegten zu bewähren. Das einzigartige Schicksal der Seele, das ist es, was die okkulten Wissenschaften bewußt halten. Und dieses ist nun aussprechbar geworden – in unserer Sprache. Man gelangt zu wuchtigen Formulierungen, die ein Leben lang halten sollen und können, was die Seele ist. Und endlich werden die Fragen formuliert, die von der neuen Lehre beantwortet werden können, ohne dem Odium der okkulten Wissenschaften zu verfallen.

VII DIE KRÄFTE DER SEELE \
 1 Mut und Furcht \
 2 Die seelische Krisis, die Katastrophe als Kernereignis des Seelischen \
 3 Die Seele ist ein Widerstand gegen Geist und Leib, der sich zu behaupten sucht \
 4 Die seelischen Aufgaben der einzelnen Lebensstufen, die Biographie vom Tode her \
5 Durchseelung, Scheinursachen von Krankheiten

Die gestellten Fragen werden beantwortet. Ein ganzes Programm von vorliegender Arbeit wird entworfen, zum Teil hat Rosenstock-Huessy sie in seinem weiteren Leben selber bearbeitet. Die Biographie vom Tode her ist Grundlage der Biologie Rudolf Ehrenbergs. Viktor von Weizsäcker hat daran gearbeitet. Hans Ehrenberg und natürlich Franz Rosenzweig in besonderer Weise.

VIII GEMEINSCHAFT \
 1 Entlastungsvorgänge, Vertrauen \
2 Die Scham (Carlyle)

Nicht immer ist die Seele zur Krise fähig, sie bedarf der Scham als der Kraft, die die richtige Zeit für Tun und Lassen findet.

### VIERTER TEIL: DIE NEUE LEHRE (2)

IX DIE SPRACHE DER GEMEINSCHAFT \
 1 Ursprung der Sprache \
 2 Dual und Plural, der Schrecken \
 3 Das Leiden des Ich, der Schreck des Du, das Wunder des Es, Ursprachen des Pluralis \
4 Werden, Sein, Anwendung \
 5 Sätze der ersten, zweiten und dritten Person, Obersatz: Wandel \
 6 Vorlagen im Schweigen \
 7 Ursachensatz, Urhebersatz, Geheißsatz \
 8 Die Fälle \
 9 Politische Anwendbarkeit – Lüge, Irrtum, Sünde, Ungerechtigkeit \
10 Deutschland zur Zeit \
 11 Wachstum \
 12 9.11.1918

Nun wird der zweite Teil der Lehre ausgebreitet, der das dem Gebiet der Soziologie ́, wie sie Rosenstock-Huessy später geschrieben hat, beschäftigt, dem Plural. Es sieht so aus, daß auch dieser Teil zum Sprachbrief von 1916 gehörte. Er mag bis zum achten Abschnitt reichen, wie ich ihn gemacht habe. Denn danach wird eindeutig auf die Lage nach dem Ersten Weltkrieg, nach dem 9.11.1918 Bezug genommen. In der Aufzählung der politischen Übel zeigt sich die Vierzahl des Kreuzes der Wirklichkeit: Lüge, Irrtum, Sünde, Ungerechtigkeit als Alltag.

### FÜNFTER TEIL: DIE KATASTROPHE DES 9. NOVEMBER 1918

X UNSER VOLK \
 1 Gemeinde \
 2 Grammatische Inventur \
 3 Uralphabet (Goethe, Hölderlin) \
4 Geschichtsschreibung

Wenn die Katastrophe das Ereignis ist, an dem sich alles Wissen vom Leben der Seele klärt, dann gilt das in besonderem Maße für die geschichtliche Katastrophe, die Eugen Rosenstock-Huessy als genauer Zeitgenosse und geistesgegenwärtig erlebt hat, den Zusammenbruch des deutschen Kaiserreichs. Zum Teil deutet er 1924 schon an, wie er mit der anstehenden Arbeit der grammatischen Inventur begonnen hat: er hat die Daimler Werkzeitung begleitet, er hat die *Werkstattaussiedlung* veröffentlicht, er hat – 1926 - ein neues *Industrierecht* entworfen. Und die Aufgabe, für das zweite Jahrtausend nach Christus die Geschichtsschreibung lebendig wirken zu lassen, wie er das dann in dem Buch *Die Europäischen Revolutionen* ausgeführt hat, ist schon skizziert.

XI GEIST, SEELE, LEIB \
 1 Die Seele ein Gesamtvorgang \
2 Lebensgeschichte (Lasalle) \
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DER GEIST IST MENSCHHEITSKRAFT, DIE SEELE KRAFT DES MENSCHEN,\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DER LEIB NATURKRAFT IM MENSCHEN \
 3 Kirchenlehre, Derivate (primärer, sekundärer, tertiärer, quartärer Geist), Die erste Forderung

Und noch immer nicht ist der Atem erschöpft! Es gilt auch noch zu klären, warum die Lehre von der Dreiteilung für den einzelnen von der Kirche niemals akzeptiert worden ist. Gleichzeitig wird der deutsche Nationalismus als Derivat des französischen Nationalgeistes von 1789 entlarvt.

### SECHSTER TEIL: PROKLAMATION

XII DIE GRAMMATISCHE METHODE \
1 Scholastik, Akademik \
 2 Zeithergabe der Zeitgenossen \
 3 Arbeitsdienstjahr\
4 Mens sana in corpore sano? \
 5 Der Leib des Geistes ist die Sprache \
6 Übersetzung \
 7 Das Rüstzeug zu einer Übersetzung \
8 Mitweg der Ereignisse

Zum Schluß aber der Entwurf einer Aufgabe nicht nur für das Jahrhundert, nein, für das Dritte Jahrtausend nach Christus. Auch die Idee der freiwilligen Arbeitsdienste wird berührt, wie er sie in den Arbeitslagern von 1928 bis 1930 und im Camp William James dann in den Vereinigten Staaten verwirklicht hat, andeutungsweise, mit dem Wunsch, für viele, viele andere Unternehmungen solcher Art ansteckend zu wirken. Die eigentümliche Hellhörigkeit auf das, was die Sprache selber sagt, kommt auch in dem Übersetzen zum Ausdruck, und wenn es der Styx ist, über den da gerudert werden muß. Mitweg der Ereignisse könnte man geradezu als Motto über das Leben Eugen Rosenstock-Huessys und seiner Freunde schreiben.

6

Es hat einige Zeit gebraucht, ehe ich den Mut nahm, an einem Text überhaupt irgend etwas zu verändern. Der Leser möge selber entscheiden, ob ihm durch die kleinteiligen Absätze Mut gemacht wird, mit Freude und ohne Hast den Gedankengängen zu folgen. Es sind inzwischen über 80 Jahre hingegangen – und dennoch ist die skizzierte Aufgabe noch immer riesengroß vor uns und aktuell.

Im *Stimmstein* der Eugen Rosenstock-Huessy Gesellschaft von 2006 steht der Satz auf S. 73, der Aufhorchen verdient, daß 1989 beide *de facto* zuendegingen: BRD und DDR – und für wen ist das bedenklicher, für die, die es offen und ohne Verhüllung erlebten, oder für die, die gesagt kriegten, alles bliebe beim Alten? Und diese Veränderung ist nichts gegen das, was am 9. November 1918 an Abschied und Erneuerung gefordert war, ja, von der damals erlebten Energie müssen wir die Kraft schöpfen, das Ende von 1989/1990 zu erfassen und das Neue zu gestalten.

*Köln, 15. Dezember 2006 \
Eckart Wilkens*



[Eckarts Bearbeitung]( {{ 'assets/downloads/wilkens_erh_angewandte_seelenkunde.pdf' | relative_url }})

[Das Orginal von „Angewandte Seelenkunde” ist des PDF-Scan](https://www.erhfund.org/wp-content/uploads/554.pdf)

siehe auch: [Rosenstock-Huessy: Nachträgliche Erwägungen Eugen Rosenstock-Huessys zur „Angewandten Seelenkunde” aus dem Erscheinungsjahr 1924](https://www.rosenstock-huessy.com/text-erh-nachtraegliche-erwaegungen-angewandte-seelenkunde-1924/)
