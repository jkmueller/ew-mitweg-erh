---
title: "Eugen Rosenstock-Huessy: Circulation of Thought (1949)"
category: lectures
created: 1949
published: 2023-09-23
---
### NOTE

1

This is an edited text of the transcription from the tapes made by Mark Huessy. It is purged from all mistakes of oral speech which don ́t belong to a text which should be read by a reader who wants to get the sense of which was spoken – not the ability of the speaker to find, in the heat of speech in the presence of more or less attentive listeners – the right words. Example:

**Transcription:**\
Now we -- then we came back to Paris, and I said that at the end of his life, he did teach in these schools on the Left Bank of the river, in a -- as a free- lance teacher. And out of this {comes} this clash: the faculty of Paris arose, as a true university faculty. And we said that in a higher school of learning can only exist where the appointments are not made from the outside, but where the group that is already in the situation can co-opt, co-elect, you see, and include into its membership the newcomers, because only then can the standards be developed {from} actual teaching.

**Edited text:**\
*Then we came back to Paris, and I said that at the end of his life, he did teach in these schools on the Left Bank of the river, as a free-lance teacher. And out of this comes this clash: the faculty of Paris arose, as a true university faculty.*

*And we said that in a higher school of learning can only exist where the appointments are not made from the outside, but where the group that is already in the situation can co-opt, co-elect and include into its membership the newcomers, because only then can the standards be developed from actual teaching.*

2

The text is divided into parts which note the articulation of speech in turning forwards (I), inwards (II), backwards (III) and outwards, each again divided into four parts in the same sense but finer articulated – that is: sixteen steps form a chapter. There are as much chapters as beginning of such articulation, a rather formal manner of dividing the text, of course, based on listening.

3

All chapters bear – as well as the six lectures as a whole – headings which introduce the reader to the important point according to the speaker ́s intention. The table of contents gives the fruit of this listening to the interesting points – it may be surprising how clear the stream of speech becomes in this manner. Of course, any other listener would possibly choose other captions – try it yourself.

4

The index of names (I-VI lectures, 1-7 chapters) may enrich the reading. Especially the notation of personal evidence.

*Köln, December 21st, 2016 \
Eckart Wilkens*

- [Eckart's Edition: ERH Circulation of Thought (1949)]( {{ 'assets/downloads/wilkens_erh_Circulation_of_thought_1949.pdf' | relative_url }})

- [The transcript of „ERH Circulation of Thought (1949)” as PDF](https://www.erhfund.org/wp-content/uploads/626.pdf)
- [The audio recordings of „ERH Circulation of Thought (1949)”](https://www.erhfund.org/lecturealbum/volume-01-circulation-of-thought-1949/)
