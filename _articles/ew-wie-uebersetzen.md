---
title: Wie übersetzen?
category: methodik
created: 2019-12
---

### Zu Bedeutung und Übersetzung von „Speech and Reality”
*Lehre beginnt erst da, wo der Stoff aufhört, Stoff zu sein, und sich in Kraft verwandelt – in Kraft, die nun selber den Stoff, und sei es um das bescheidenste Wort vermehrt, und so aus jener behaupteten Unendlichkeit des Stoffes erst eine Wahrheit macht.*
>*Franz Rosenzweig, Die Bauleute*

Als ich nach dem Erscheinen der „Sprache des Menschengeschlechts” die aus dem Englischen übersetzten Stücke las, hielt ich den Text für die Version, die Eugen Rosenstock-Huessy selber so formuliert hatte. Im Vorwort steht ja:
*Die letzten zwölf Jahre brachten mir die Freundeshilfe des Direktors der Bodelschwingh-Schule in Bethel bei Bielefeld, Dr. Georg Müller. Die englischen Manuskripte wanderten zu ihm. Er wandte die große Mühe einer ersten Übersetzung auf, um sie immer wieder in die öffentliche Diskussion hineinzuziehen. Er hat mir damit den unersetzlichen Dienst geleistet, in einer über die Junggrammatiker der siebziger Jahre kaum hinausgeschritten Welt – das Münchener Symposion der Sprache von 1960 ist absolut vor-nietzisch – auf die neue Fragestellung unermüdlich hinzuweisen. Er und ich hoffen, daß damit das Klima für die verlangsamte Publikation dieses Werkes besser vorbereitet ist.*
Als ich aber damit anfing, die Stücke aus dem 1970, also sieben Jahre später erschienenen Band „Speech and Reality” zu übersetzen, merkte ich, daß die deutschen Übersetzungen im Sprachbuch mangelhaft sind – vor allem berücksichtigen sie nicht die verschiedene Satzmelodie im Englischen und Deutschen, trotten vokabelmäßig daher wie eine Cäsar-Übersetzung in Untersekunda, und vor allem: Entstehungsgrund und Datum erscheinen so verborgen, daß das wirkliche Leben Rosenstock-Huessys von 1933 bis 1950 in Amerika, als des Kriegsgegners Hitlers, gar nicht merklich werden kann.
Bis auf das erste Stück „In Defense of the Grammatical Method” von 1939 und 1955) erscheinen sämtliche Stücke von „Speech and Reality” im zweiten Teil des Sprachbuchs: Wie wird gesprochen:
- *Articulated Speech 1937*
- *The Uni-versity of Logic, Language and Literature 1935*
- *Grammar as Social Science 1945*
- *How Language Establishes Relations 1945*
- *The Listener's Tract 1944*
- *The Individual's Right to Speak 1945*
- *Vom Artikulieren 1937 S. 312*
- *Die Einsinnigkeit von Logik, Linguistik und Literatur 1935, S. 525*
- *In Beziehung Treten 1945, S. 419*
- *Schizo-Somatik oder Wie die Grammatik die Sozialleiber spaltet, S. 432*
- *Hörer und Sprecher – Aufhören und Lossagen 1944, S. 339*
- *Des Individuums Recht auf Sprache 1962, S. 704*

Bei der Übersetzung konnte ich die Stellen einfügen, die der Autor in die deutsche Übersetzung eingefügt hat – was natürlich den Eindruck verstärkt, er hätte den ganzen Text so gestaltet.
Es ist ein Musterbeispiel für die Lehre des Zeitigens: dem deutschen Leser von 1963/64 wird schonend erspart, den Ernst der Existenz von Rosenstock-Huessy in Amerika wahrzunehmen. Die Wirksamkeit aber gerade des richtigen Zeitpunkts des Sprechens – daß das Sprachbuch eben kein Buch ist, sondern die Versammlung solcher zur rechten Zeit gesprochenen Worte mit bestimmten Hörernwird so matt, daß ich mich endlich nicht mehr wundere, daß die gehörige Resonanz auf dieses Alterswerk ausgeblieben ist.
Nach dem Englischunterricht zu urteilen, den ich in den ersten Gymnasialjahren Mitte der fünfziger Jahre des vorigen Jahrhunderts genoß, von Lehrern, die nicht einmal wirklich Englisch reden konnten, ist diese Vertaubung durch Übersetzung nicht erstaunlich. Und doch muß dem aufgeholfen werden, mehr als fünfzig Jahre nach dem Erscheinen der „Sprache des Menschengeschlechts”.

aus: [ERHG Mitgliederbrief 2019-12](https://www.rosenstock-huessy.com/erhg-2019-12-mitgliederbrief/)
